#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int k = -1;
static int S[16] = {};
static int L[8] = {};

static void print_l(void) {
	printf("%d %d %d %d %d %d\n", L[0], L[1], L[2], L[3], L[4], L[5]);
}

static void iterate(size_t position_s, size_t position_l) {
	if (position_l == 6) {
		print_l();
		return;
	}
	if (((size_t) k - position_s) + position_l < 6) {
		return;
	}
	L[position_l] = S[position_s];
	iterate(position_s + 1, position_l + 1);
	iterate(position_s + 1, position_l);
}

int main() {
	if (scanf("%d", &k) != 1 || k == 0) {
		return 0;
	}
	for (;;) {
		if (k <= 6 || k >= 13) {
			return 1;
		}
		for (int i = 0; i < k; ++i) {
			if (scanf("%d", &S[i]) != 1) {
				return 1;
			}
		}
		iterate(0, 0);
		if (scanf("%d", &k) != 1 || k == 0) {
			break;
		}
		puts("");
	}
	return 0;
}
