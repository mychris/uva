#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

int main() {
	for (string line; getline(cin, line);) {
		if (line.empty()) {
			continue;
		}
		bool in_word = false;
		int word_cnt = 0;

		for (char c : line) {
			if (in_word) {
				if (!isalpha(c)) {
					in_word = false;
				}
			} else {
				if (isalpha(c)) {
					in_word = true;
					++word_cnt;
				}
			}
		}
		printf("%d\n", word_cnt);
	}
	return 0;
}
