#include <cstdio>

using namespace std;

static const char *day_to_string[] = {
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
};

static int days[12][31];

static int days_in_month[] = {
    31, // Jan
    28, // Feb
    31, // March
    30, // April
    31, // May
    30, // June
    31, // July
    31, // Aug
    30, // Sept
    31, // Oct
    30, // Nov
    31, // Dec
};

static void init(void) {
	int cur_day = 5; // 01/01/2011 is a Saturday.
	for (int month = 0; month < 12; ++month) {
		for (int day = 0; day < days_in_month[month]; ++day) {
			days[month][day] = cur_day;
			cur_day = (cur_day + 1) % 7;
		}
	}
}

int main() {
	init();
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int month, day;
		scanf("%d %d\n", &month, &day);
		printf("%s\n", day_to_string[days[month - 1][day - 1]]);
	}
	return 0;
}
