#include <cstdio>

using namespace std;

int main() {
	long long players;
	while (scanf("%lld\n", &players) == 1 && players >= 0LL) {
		printf("%lld\n", (players == 0) ? 0LL : players - 1LL);
	}
	return 0;
}
