#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

#include <map>
#include <vector>

using namespace std;

char a_buff[512];
char b_buff[512];

map<vector<unsigned>, unsigned> cache;

static unsigned in_tbl[] = {
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    0,   1,   2,   3,   4,   5,   6,   7,   8,   9,   255, 255, 255, 255, 255, 255,
    255, 10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,
    25,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255};
static vector<unsigned> my_strtoll(const char *s, const unsigned base) {
	vector<unsigned> result;
	result.push_back(0);
	while (*s) {
		char c = *s;
		unsigned c_base = in_tbl[static_cast<size_t>(c)];
		// assert(c_base != 255);
		if (c_base >= base) {
			result.clear();
			break;
		}
		unsigned &b = result.back();
		unsigned n = b * base + c_base;
		if (n < b) {
			b = static_cast<unsigned>(-1);
			result.push_back(n);
		} else
			b = n;
		++s;
	}
	return result;
}

int main() {
	while (scanf("%500s %500s", a_buff, b_buff) == 2) {
		cache.clear();
		unsigned a_base = 0;
		unsigned b_base = 0;
		for (unsigned base = 2; base <= 36; ++base) {
			vector<unsigned> r = my_strtoll(a_buff, base);
			if (!r.empty() && cache.count(r) == 0)
				cache.insert(make_pair(r, base));
		}
		for (unsigned base = 2; !a_base && base <= 36; ++base) {
			vector<unsigned> r = my_strtoll(b_buff, base);
			if (!r.empty() && cache.count(r) == 1) {
				a_base = cache[r];
				b_base = base;
			}
		}
		if (a_base) {
			printf("%s (base %u) = %s (base %u)\n", a_buff, a_base, b_buff, b_base);
		} else {
			printf("%s is not equal to %s in any base 2..36\n", a_buff, b_buff);
		}
	}
	return 0;
}
