#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstdlib>

using namespace std;

static inline bool is_leap_year(const int year) {
	return ((year & 3) == 0 && ((year % 25) != 0 || (year & 15) == 0));
}

static int days_in_year(const int year) {
	return (is_leap_year(year)) ? 366 : 365;
}

static int days_in_month(int month, int year) {
	switch (month) {
	case 1:
		return 31;
	case 2:
		return (is_leap_year(year)) ? 29 : 28;
	case 3:
		return 31;
	case 4:
		return 30;
	case 5:
		return 31;
	case 6:
		return 30;
	case 7:
		return 31;
	case 8:
		return 31;
	case 9:
		return 30;
	case 10:
		return 31;
	case 11:
		return 30;
	case 12:
		return 31;
	default:
		fprintf(stderr, "invalid month %d\n", month);
		exit(127);
	}
}

int main() {
	int to_add, day, month, year;
	while (scanf("%d %d %d %d\n", &to_add, &day, &month, &year) == 4) {
		if (to_add <= 0 && day <= 0 && month <= 0 && year <= 0) {
			break;
		}
		day += to_add;
		if (day > days_in_year(year)) {
			while (month > 2) {
				day -= days_in_month(month, year);
				++month;
				if (month > 12) {
					month = 1;
					++year;
				}
			}
		}
		while (day > days_in_year(year)) {
			day -= days_in_year(year);
			++year;
		}
		while (day > days_in_month(month, year)) {
			day -= days_in_month(month, year);
			++month;
			if (month > 12) {
				month = 1;
				++year;
			}
		}
		printf("%d %d %d\n", day, month, year);
	}
	return 0;
}
