#include <cstdio>
#include <cstring>
#if !defined(ONLINE_JUDGE)
#include <cassert>
#define ASSERT(x) assert(x)
#else
#define ASSERT(x)
#endif

using namespace std;

int mem[200];
/* clang-format off */
int cache[200] = {
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
	31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
	51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
	61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
	71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
	81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
	91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
	101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
	111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
	121, 122, 123, 124, 125, 126, 127, 128, 129, 130,
	131, 132, 133, 134, 135, 136, 137, 138, 139, 140,
	141, 142, 143, 144, 145, 146, 147, 148, 149, 150,
	151, 152, 153, 154, 155, 156, 157, 158, 159, 160,
	161, 162, 163, 164, 165, 166, 167, 168, 169, 170,
	171, 172, 173, 174, 175, 176, 177, 178, 179, 180,
	181, 182, 183, 184, 185, 186, 187, 188, 189, 190,
	191, 192, 193, 194, 195, 196, 197, 198, 199, 200,
};
/* clang-format on */
unsigned int solutions[200][200];

#define KILL(x) (mem[(x)] = 0)
#define NEXT()                   \
	do {                         \
		size_t cntr_ = 1;        \
		while (cntr_ != k) {     \
			ptr = (ptr + 1) % n; \
			if (mem[ptr]) {      \
				++cntr_;         \
			}                    \
		}                        \
	} while (0)

bool test(unsigned int n, unsigned int k, unsigned int i) {
	ASSERT(n <= 100 && k <= 100 && i <= n);
	memcpy(&mem[0], &cache[0], sizeof(int) * 200);
	unsigned int alive = n;
	size_t ptr = i - 1;
	while (alive > 1) {
		NEXT();
		size_t kill_index = ptr;
		if (mem[kill_index] == 1) {
			return false;
		}
		mem[kill_index] = 0;
		while (!mem[ptr]) {
			ptr = (ptr + 1) % n;
		}
		NEXT();
		size_t bury_index = ptr;
		mem[kill_index] = mem[bury_index];
		mem[bury_index] = 0;
		ptr = (kill_index + 1) % n;
		while (!mem[ptr]) {
			ptr = (ptr + 1) % n;
		}
		alive--;
	}
	return true;
}

unsigned int solve(unsigned int n, unsigned int k) {
	ASSERT(n <= 100 && k <= 100);
	if (solutions[n][k]) {
		return solutions[n][k];
	}
	unsigned int i = 1;
	while (!test(n, k, i)) {
		++i;
	}
	ASSERT(i > 0);
	ASSERT(i <= n);
	solutions[n][k] = i;
	return i;
}

int main() {
	unsigned int n, k;
	while (scanf("%u %u\n", &n, &k) == 2 && n > 0 && k > 0 && n <= 100) {
		printf("%u\n", solve(n, k));
	}
	return 0;
}
