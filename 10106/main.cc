#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char lhs[512];
static char rhs[512];
static int result[1024];

int main() {
	while (scanf("%256s\n%256s\n", &lhs[0], &rhs[0]) == 2) {
		const size_t len_lhs = strlen(lhs);
		const size_t len_rhs = strlen(rhs);
		size_t k = 0, l = 0;
		memset(result, 0, sizeof(result));
		for (ssize_t i = (ssize_t) len_rhs - 1; i >= 0; --i) {
			k += 1;
			l = k - 1;
			int carry = 0;
			for (ssize_t j = (ssize_t) len_lhs - 1; j >= 0; --j) {
				l += 1;
				int temp = (rhs[i] - '0') * (lhs[j] - '0') + carry + result[l];
				result[l] = temp % 10;
				carry = temp / 10;
			}
			if (carry > 0) {
				l += 1;
				result[l] = carry;
			}
		}
		if (strcmp(lhs, "0") == 0 || strcmp(rhs, "0") == 0) {
			puts("0");
		} else {
			for (size_t i = (size_t) l; i >= 1; --i) {
				printf("%d", result[i]);
			}
			puts("");
		}
	}
	return 0;
}
