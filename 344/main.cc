#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

/* clang-format off */
static const string roman[] = {
    "c", "xc", "l", "xl", "x", "ix", "v", "iv", "i",
};
static const int arab[] = {
    100, 90, 50, 40, 10, 9, 5, 4, 1, 0,
};
/* clang-format on */

static string to_roman(int n) {
	string result;
	for (int i = 0; n > 0 && arab[i]; ++i) {
		while (n - arab[i] >= 0) {
			n -= arab[i];
			result.append(roman[i]);
		}
	}
	return result;
}

int cnt(string str, char c) {
	int cnt = 0;
	for (char cur : str) {
		if (cur == c) {
			++cnt;
		}
	}
	return cnt;
}

static int i[101];
static int v[101];
static int x[101];
static int l[101];
static int c[101];

void init(void) {
	for (int cur = 1; cur <= 100; ++cur) {
		string rom = to_roman(cur);
		i[cur] = i[cur - 1] + cnt(rom, 'i');
		v[cur] = v[cur - 1] + cnt(rom, 'v');
		x[cur] = x[cur - 1] + cnt(rom, 'x');
		l[cur] = l[cur - 1] + cnt(rom, 'l');
		c[cur] = c[cur - 1] + cnt(rom, 'c');
	}
}

int main() {
	init();
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		printf("%d: %d i, %d v, %d x, %d l, %d c\n", n, i[n], v[n], x[n], l[n], c[n]);
	}
}
