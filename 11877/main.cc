#include <cstdio>

using namespace std;

int main() {
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		/*
		int d = 0;
		int full = 0;
		int empty = n;
		int counter = 7;
		while (empty > 0 && counter > 0) {
		        full = empty / 3;
		        empty %= 3;
		        d += full;
		        empty += full;
		        full = 0;
		        counter--;
		        if (empty == 2)
		                empty++;
		}
		*/
		printf("%d\n", n / 2);
	}
	return 0;
}
