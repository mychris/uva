#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

#include <stack>

using namespace std;

static size_t length;
static size_t check[1024];
static stack<size_t> work;

static bool is_it_possible() {
	size_t j = 0;
	while (!work.empty()) {
		work.pop();
	}
	for (size_t i = 0; i < length; ++i) {
		while (j < length && j != check[i]) {
			if (!work.empty() && work.top() == check[i]) {
				break;
			}
			++j;
			work.push(j);
		}
		if (work.top() == check[i]) {
			work.pop();
		}
	}
	return work.empty();
}

int main() {
	while (scanf("%zu", &length) == 1 && length != 0) {
		if (length > 1000) {
			return 1;
		}
		while (scanf("%zu", &check[0]) == 1 && check[0] > 0) {
			for (size_t i = 1; i < length; ++i) {
				scanf("%zu", &check[i]);
			}
			puts(is_it_possible() ? "Yes" : "No");
		}
		puts("");
	}
	return 0;
}
