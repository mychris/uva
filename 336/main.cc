#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cstdio>
#include <map>
#include <queue>
#include <vector>

using namespace std;

void calcForStartingNode(map<int, vector<int>> &network,
                         map<int, int> &result,
                         int start) {
	assert(result.empty());
	for (const auto &pair : network) {
		result[pair.first] = INT_MAX;
	}
	queue<pair<int, int>> q;
	q.push({start, 0});
	while (!q.empty()) {
		auto head = q.front();
		q.pop();
		int node = head.first;
		int dist = head.second;
		if (dist < result[node]) {
			result[node] = dist;
			for (const int &neighbour : network[node]) {
				q.push({neighbour, dist + 1});
			}
		}
	}
}

int main() {
	int CASE = 0;
	int NC;
	while (1 == scanf("%d", &NC) && 0 < NC) {
		map<int, vector<int>> network;
		map<int, map<int, int>> distances;
		while (NC--) {
			int from, to;
			scanf("%d %d", &from, &to);
			network[from].push_back(to);
			network[to].push_back(from);
		}
		int start, ttl;
		while (2 == scanf("%d %d", &start, &ttl) && (0 < start || 0 < ttl)) {
			CASE++;
			if (distances[start].empty()) {
				calcForStartingNode(network, distances[start], start);
			}
			int cnt = 0;
			for (const auto &entry : distances[start]) {
				cnt += static_cast<int>(entry.second > ttl);
			}
			printf("Case %d: %d nodes not reachable from node %d with TTL = %d.\n",
			       CASE,
			       cnt,
			       start,
			       ttl);
		}
	}
	return 0;
}
