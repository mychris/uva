#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cstdio>
#include <iostream>
#include <map>

using namespace std;

int main() {
	unsigned int pw_length;
	while (scanf("%u", &pw_length) == 1) {
		string str;
		cin >> str;
		map<string, int> m;

		for (size_t i = 0; i < str.length() - pw_length + 1; i++) {
			string subs = str.substr(i, pw_length);
			if (m.find(subs) != m.end()) {
				m[subs] += 1;
			} else {
				m[subs] = 1;
			}
		}

		string ret;
		int max = INT_MIN;
		for (auto k_v : m) {
			if (k_v.second > max) {
				max = k_v.second;
				ret = k_v.first;
			}
		}

		cout << ret << endl;
	}
	return 0;
}
