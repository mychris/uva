#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

static inline string &trim(string &s) {
	// ltrim
	s.erase(s.begin(),
	        find_if(s.begin(), s.end(), [](int x) { return !std::isspace(x); }));
	// ritrim
	s.erase(
	    find_if(s.rbegin(), s.rend(), [](int x) { return !std::isspace(x); }).base(),
	    s.end());
	return s;
}

int main() {
	int the_case = 1;
	for (string line; getline(cin, line);) {
		trim(line);
		if (line == "#") {
			break;
		}
		if (line == "HELLO") {
			printf("Case %d: ENGLISH\n", the_case);
		} else if (line == "HOLA") {
			printf("Case %d: SPANISH\n", the_case);
		} else if (line == "HALLO") {
			printf("Case %d: GERMAN\n", the_case);
		} else if (line == "BONJOUR") {
			printf("Case %d: FRENCH\n", the_case);
		} else if (line == "CIAO") {
			printf("Case %d: ITALIAN\n", the_case);
		} else if (line == "ZDRAVSTVUJTE") {
			printf("Case %d: RUSSIAN\n", the_case);
		} else {
			printf("Case %d: UNKNOWN\n", the_case);
		}
		++the_case;
	}
	return 0;
}
