#include <cstdio>

using namespace std;

int main() {
	int h1, m1, h2, m2;
	while (scanf("%d %d %d %d\n", &h1, &m1, &h2, &m2) == 4) {
		if (h1 == 0 && m1 == 0 && h2 == 0 && m2 == 0) {
			break;
		}
		int cur = h1 * 60 + m1;
		int alarm = h2 * 60 + m2;
		if (alarm < cur) {
			alarm += 24 * 60;
		}
		int sleep = alarm - cur;
		printf("%d\n", sleep);
	}
	return 0;
}
