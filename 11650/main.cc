#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int h, m;
		scanf("%d:%d\n", &h, &m);
		h = 11 - h + ((m == 0) ? 1 : 0);
		if (h <= 0) {
			h += 12;
		}
		if (m > 0) {
			m = 60 - m;
		}
		printf("%02d:%02d\n", h, m);
	}
	return 0;
}
