#include <cstdio>
#include <map>

using namespace std;

static int get_powers(int n, int p) {
	long long result = 0;
	for (long long power = p; power <= n; power *= p) {
		result += n / power;
	}
	return (int) result;
}

static map<int, int> prime_factors(int n) {
	map<int, int> facts;
	int e = 0;
	while (n % 2 == 0) {
		e++;
		n /= 2;
	}
	if (e != 0) {
		facts[2] = e;
	}

	e = 0;
	while (n % 3 == 0) {
		e++;
		n /= 3;
	}
	if (e != 0) {
		facts[3] = e;
	}

	long long t = 5;
	int diff = 2;
	while (t * t <= n) {
		e = 0;
		while (n % t == 0) {
			e++;
			n /= (int) t;
		}
		if (e != 0) {
			facts[(int) t] = e;
		}
		t += diff;
		diff = 6 - diff;
	}

	if (n > 1) {
		facts[n] = 1;
	}

	return facts;
}

static bool test(int n, int m) {
	if (!m) {
		return false;
	}
	if (m <= n) {
		return true;
	}
	map<int, int> facts = prime_factors(m);
	for (auto key_val : facts) {
		if (get_powers(n, key_val.first) < key_val.second) {
			return false;
		}
	}
	return true;
}

int main() {
	int n, m;
	while (scanf("%d %d\n", &n, &m) == 2) {
		if (test(n, m)) {
			printf("%d divides %d!\n", m, n);
		} else {
			printf("%d does not divide %d!\n", m, n);
		}
	}
	return 0;
}
