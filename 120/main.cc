#include <algorithm>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

static int stack[31];
static int pancakes;
static int stack_sorted[31];

static void print_stack(void) {
	for (int i = 0; i < pancakes - 1; ++i) {
		printf("%d ", stack[i]);
	}
	printf("%d\n", stack[pancakes - 1]);
}

static void flip(int n) {
	if (n <= 0 || n > pancakes) {
		fprintf(stderr, "invalid flip %d for pancakes %d\n", n, pancakes);
		exit(EXIT_FAILURE);
		return;
	}
	reverse(&stack[0], &stack[pancakes - n + 1]);
}

static void work(void) {
	print_stack();
	int to_flip = 1;
	while (to_flip <= pancakes) {
		int highest = stack_sorted[pancakes - to_flip];
		for (int i = 0; i < pancakes - to_flip; ++i) {
			if (stack[i] == highest) {
				printf("%d ", pancakes - i);
				flip(pancakes - i);
				printf("%d ", to_flip);
				flip(to_flip);
				break;
			}
		}
		++to_flip;
	}
	printf("0\n");
}

int main() {
	for (string line; getline(cin, line);) {
		pancakes = sscanf(line.c_str(),
		                  "%d %d %d %d %d %d %d %d %d %d"
		                  "%d %d %d %d %d %d %d %d %d %d"
		                  "%d %d %d %d %d %d %d %d %d %d",
		                  &stack[0],
		                  &stack[1],
		                  &stack[2],
		                  &stack[3],
		                  &stack[4],
		                  &stack[5],
		                  &stack[6],
		                  &stack[7],
		                  &stack[8],
		                  &stack[9],
		                  &stack[10],
		                  &stack[11],
		                  &stack[12],
		                  &stack[13],
		                  &stack[14],
		                  &stack[15],
		                  &stack[16],
		                  &stack[17],
		                  &stack[18],
		                  &stack[19],
		                  &stack[20],
		                  &stack[21],
		                  &stack[22],
		                  &stack[23],
		                  &stack[14],
		                  &stack[25],
		                  &stack[26],
		                  &stack[27],
		                  &stack[28],
		                  &stack[29]);
		if (pancakes <= 0) {
			break;
		}
		for (int i = 0; i < pancakes; ++i) {
			stack_sorted[i] = stack[i];
		}
		sort(stack_sorted, stack_sorted + pancakes);
		work();
	}
	return 0;
}
