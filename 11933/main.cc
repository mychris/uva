#include <cstdio>

using namespace std;

int main() {
	unsigned int x;
	while (scanf("%u\n", &x) == 2 && x > 0) {
		unsigned int ab[2] = {0, 0};
		unsigned int c = 0;

		unsigned int i;
		for (i = 1; i != 0; i <<= 1) {
			if (x & i) {
				ab[c] |= i;
				c = (c + 1) % 2;
			}
		}

		printf("%u %u\n", ab[0], ab[1]);
	}
	return 0;
}
