#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		int highest = 0;
		int current = 0;
		while (n--) {
			int x;
			scanf("%d", &x);
			current += x;
			if (current > highest) {
				highest = current;
			} else if (current < 0) {
				current = 0;
			}
		}
		if (current > highest) {
			highest = current;
		}
		if (highest) {
			printf("The maximum winning streak is %d.\n", highest);
		} else {
			puts("Losing streak.");
		}
	}
	return 0;
}
