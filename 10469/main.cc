#include <cinttypes>
#include <cstdio>

using namespace std;

uint32_t do_it(uint32_t a, uint32_t b) {
	uint32_t result = 0;
	uint32_t mask = 1;
	while (mask) {
		result |= (a & mask) ^ (b & mask);
		mask <<= 1;
	}
	return result;
}

int main() {
	uint32_t a, b;
	while (scanf("%" SCNu32 " %" SCNu32 "\n", &a, &b) == 2) {
		printf("%" PRIu32 "\n", do_it(a, b));
	}
	return 0;
}
