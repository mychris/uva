#include <cstdio>
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main() {
	int _C;
	scanf("%d", &_C);
	string line;
	getline(cin, line);
	getline(cin, line);
	while (_C--) {
		map<string, int> map;
		int cnt = 0;
		while (getline(cin, line) && !line.empty()) {
			++cnt;
			auto search = map.find(line);
			if (search != map.end()) {
				search->second += 1;
			} else {
				map.insert({line, 1});
			}
		}
		for (auto it : map) {
			printf("%s %.4lf\n", it.first.c_str(), it.second / (double) cnt * 100);
		}
		if (_C > 0) {
			printf("\n");
		}
	}
	return 0;
}
