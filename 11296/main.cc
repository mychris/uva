#include <cstdio>

using namespace std;

int main() {
	long long upper;
	while (scanf("%lld\n", &upper) == 1) {
		upper = upper / 2 + 1;
		upper = upper * (upper + 1) / 2;
		printf("%lld\n", upper);
	}
	return 0;
}
