#include <cstdio>

using namespace std;

static char input[1024];

static int sum_input_digits(void) {
	int sum = 0;
	char *c = &input[0];
	while (*c) {
		sum += *c - '0';
		++c;
	}
	return sum;
}

static int sum_digits(int n) {
	int sum = 0;
	while (n) {
		sum += n % 10;
		n /= 10;
	}
	return sum;
}

static void do_it(int sum_of_digits, int depth) {
	if (sum_of_digits <= 9) {
		if (sum_of_digits == 9) {
			printf(" is a multiple of 9 and has 9-degree %d.\n", depth);
		} else {
			puts(" is not a multiple of 9.");
		}
		return;
	}
	do_it(sum_digits(sum_of_digits), depth + 1);
}

int main() {
	while (scanf("%s", input) == 1) {
		if (input[0] == '0' && input[1] == '\0') {
			break;
		}
		fputs(input, stdout);
		do_it(sum_input_digits(), 1);
	}
	return 0;
}
