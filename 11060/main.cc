#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>
#include <map>
#include <string>

using namespace std;

struct beverage {
	char name[64];
	size_t before[200];
	size_t before_end;
	size_t before_deleted;
	bool done;
};

static beverage beverages[100];
static map<string, size_t> beverage_mapping;
static char name_a[64];
static char name_b[64];

int main() {
	size_t tc = 1;
	size_t N, M;
	while (scanf("%zu\n", &N) == 1) {
		printf("Case #%zu: Dilbert should drink beverages in this order:", tc++);
		beverage_mapping.clear();
		for (size_t i = 0; i < N; ++i) {
			scanf("%s\n", &beverages[i].name[0]);
			beverages[i].before_end = 0;
			beverages[i].before_deleted = 0;
			beverages[i].done = false;
			beverage_mapping[string(&beverages[i].name[0])] = i;
		}
		if (scanf("%zu\n", &M) != 1) {
			return 2;
		}
		for (size_t i = 0; i < M; ++i) {
			scanf("%s %s\n", &name_a[0], &name_b[0]);
			size_t a_idx = beverage_mapping[string(name_a)];
			size_t b_idx = beverage_mapping[string(name_b)];
			beverages[b_idx].before[beverages[b_idx].before_end++] = a_idx;
		}
		size_t beverages_done = 0;
		while (beverages_done != N) {
			for (size_t i = 0; i < N; ++i) {
				if (beverages[i].done) {
					assert(beverages[i].before_deleted == beverages[i].before_end);
					continue;
				}
				if (beverages[i].before_deleted == beverages[i].before_end) {
					beverages_done++;
					beverages[i].done = true;
					printf(" %s", beverages[i].name);
#if !defined(NDBEUG)
					fflush(stdout);
#endif
					for (size_t j = 0; j < N; ++j) {
						for (size_t k = 0;
						     !beverages[j].done && k < beverages[j].before_end;
						     ++k) {
							if (beverages[j].before[k] == i) {
								assert(beverages[j].before_deleted <
								       beverages[j].before_end);
								beverages[j].before_deleted++;
							}
						}
					}
					break; // restart, otherwise order might be wrong!
				}
			}
		}
		puts(".\n");
	}
	return 0;
}
