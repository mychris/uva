#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

int main() {
	int n_req, n_prop, rfp = 1;
	for (string line; getline(cin, line);) {
		int best = 0;
		double best_val = 0.0;
		string result;
		if (sscanf(line.c_str(), "%d %d", &n_req, &n_prop) != 2) {
			break;
		}
		if (n_req == 0 && n_prop == 0) {
			break;
		}
		if (rfp > 1) {
			cout << endl;
		}
		for (int i = 0; i < n_req; ++i) {
			getline(cin, line);
		}
		for (int i = 0; i < n_prop; ++i) {
			string proposal;
			getline(cin, proposal);
			getline(cin, line);
			double val;
			int met;
			sscanf(line.c_str(), "%lf %d", &val, &met);
			if (met > best) {
				best = met;
				best_val = val;
				result = proposal;
			}
			if (met == best && val < best_val) {
				best = met;
				best_val = val;
				result = proposal;
			}
			for (int j = 0; j < met; ++j) {
				getline(cin, line);
			}
		}
		printf("RFP #%d\n%s\n", rfp++, result.c_str());
	}
	return 0;
}
