#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

size_t n_rectangles = 0;
double rectangles[32][4] = {};

int main() {
	for (;;) {
		char type = ' ';
		while (type != '*' && type != 'r') {
			type = (char) getchar();
		}
		if (type == '*') {
			break;
		}
		scanf("%lf %lf %lf %lf",
		      &rectangles[n_rectangles][0],
		      &rectangles[n_rectangles][1],
		      &rectangles[n_rectangles][2],
		      &rectangles[n_rectangles][3]);
		++n_rectangles;
	}
	double x, y;
	size_t point_cntr = 1;
	while (scanf("%lf %lf", &x, &y) == 2 && x < 9999.9 && y < 9999.9) {
		bool contained = false;
		for (size_t i = 0; i < n_rectangles; ++i) {
			if (x > rectangles[i][0] && x < rectangles[i][2] && y < rectangles[i][1] &&
			    y > rectangles[i][3]) {
				printf("Point %zu is contained in figure %zu\n", point_cntr, i + 1);
				contained = true;
			}
		}
		if (!contained) {
			printf("Point %zu is not contained in any figure\n", point_cntr);
		}
		++point_cntr;
	}
	return 0;
}
