#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int N, last;
		int high_cnt = 0, low_cnt = 0;
		scanf("%d %d", &N, &last);
		while (--N) {
			int cur;
			scanf("%d", &cur);
			if (cur < last) {
				++low_cnt;
			} else if (cur > last) {
				++high_cnt;
			}
			last = cur;
		}
		printf("Case %d: %d %d\n", the_case, high_cnt, low_cnt);
	}
	return 0;
}
