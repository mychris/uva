#include <algorithm>
#include <cstdio>

using namespace std;

static int n[5];

static bool test(int depth, int cur) {
	if (depth >= 5) {
		return cur == 23;
	}
	int ndepth = depth + 1;
	if (test(ndepth, cur + n[depth])) {
		return true;
	}
	if (test(ndepth, cur - n[depth])) {
		return true;
	}
	return cur < 250 && test(ndepth, cur * n[depth]);
}

int main() {
	while (scanf("%d %d %d %d %d\n", &n[0], &n[1], &n[2], &n[3], &n[4]) == 5) {
		if (n[0] == 0 && n[1] == 0 && n[2] == 0 && n[3] == 0 && n[4] == 0) {
			break;
		}
		bool possible = false;
		sort(n, n + 5);
		do {
			if (test(1, n[0])) {
				possible = true;
				break;
			}
		} while (next_permutation(n, n + 5));
		puts((possible) ? "Possible" : "Impossible");
	}
	return 0;
}
