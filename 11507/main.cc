#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstring>

using namespace std;

enum Axis {
	PX = 0,
	NX = 1,
	PY = 2,
	NY = 3,
	PZ = 4,
	NZ = 5,
};

static Axis const bend_table[6][6] = {
    {PX, PX, PY, NY, PZ, NZ},
    {PX, PX, NY, PY, NZ, PZ},
    {PX, PX, NX, PX, PY, PY},
    {PX, PX, PX, NX, NY, NY},
    {PX, PX, PZ, PZ, NX, PX},
    {PX, PX, NZ, NZ, PX, NX},
};

static Axis parse_axis(char const *input) {
	assert(input != NULL);
	assert(input[0] != '\0');
	assert(input[1] != '\0');
	assert(input[2] == '\0');

	switch (input[0]) {
	case '+':
		switch (input[1]) {
		case 'x':
			return PX;
		case 'y':
			return PY;
		case 'z':
			return PZ;
		default:
			assert(0);
		}
	case '-':
		switch (input[1]) {
		case 'x':
			return NX;
		case 'y':
			return NY;
		case 'z':
			return NZ;
		default:
			assert(0);
		}
	default:
		assert(0);
	}
}

static char const *axis_to_string(Axis const a) {
	switch (a) {
	case PX:
		return "+x";
	case NX:
		return "-x";
	case PY:
		return "+y";
	case NY:
		return "-y";
	case PZ:
		return "+z";
	case NZ:
		return "-z";
	}
	assert(0);
}

int main() {
	for (;;) {
		int length;
		Axis current_direction = PX;
		if (scanf("%d\n", &length) != 1) {
			fprintf(stderr, "can not scan next length\n");
			return 1;
		}
		if (length <= 0) {
			break;
		}
		--length;
		while (length--) {
			char next[4];
			if (scanf("%s", &next[0]) != 1) {
				fprintf(stderr, "can not scan next move\n");
				return 1;
			}
			if (strcmp("No", &next[0]) == 0) {
				continue;
			}
			Axis next_direction = parse_axis(&next[0]);
			if (next_direction == PX || next_direction == NX) {
				fprintf(stderr, "invalud input: %s", axis_to_string(next_direction));
				return 2;
			}
			current_direction = bend_table[current_direction][next_direction];
		}
		puts(axis_to_string(current_direction));
	}
	return 0;
}
