#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	puts("The 1500'th ugly number is 859963392.");
	return 0;
}
