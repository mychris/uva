#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cstdio>
#include <stack>
#include <vector>

using namespace std;

struct edge {
	int from;
	int to;
	int time;
};

struct work_item {
	int system;
	int time;
};

vector<edge> edges_from_system[1024];
int systems[1024];
int n_systems;
int n_wormholes;

int main() {
	int N;
	scanf("%d", &N);
	while (N--) {
		scanf("%d %d", &n_systems, &n_wormholes);
		for (int x = 0; x < n_systems; ++x) {
			edges_from_system[x].clear();
			systems[x] = INT_MAX;
		}
		for (int x = 0; x < n_wormholes; ++x) {
			edge e;
			scanf("%d %d %d", &e.from, &e.to, &e.time);
			edges_from_system[e.from].push_back(e);
		}
		int possible = 0;
		stack<work_item> work;
		work.push({0, 0});
		while (!work.empty()) {
			work_item cur = work.top();
			work.pop();
			if (systems[cur.system] != INT_MAX && cur.time + systems[cur.system] < 0) {
				possible = 1;
				break;
			} else if (systems[cur.system] == INT_MAX ||
			           cur.time < systems[cur.system]) {
				systems[cur.system] = cur.time;
				for (auto e : edges_from_system[cur.system]) {
					work.push({e.to, cur.time + e.time});
				}
			}
		}
		puts(possible ? "possible" : "not possible");
	}
	return 0;
}
