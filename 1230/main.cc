#include <cstdio>

using namespace std;

int powermod(int base, int exp, int mod) {
	if (base < 1 || exp < 0 || mod < 1) {
		return -1;
	}
	int result = 1;
	while (exp) {
		if (exp & 1) {
			result = (result * base) % mod;
		}
		base = (base * base) % mod;
		exp = exp >> 1;
	}
	return result;
}

int main() {
	int c;
	while (scanf("%d\n", &c) == 1 && c > 0) {
		while (c--) {
			int x, y, n;
			scanf("%d %d %d\n", &x, &y, &n);
			printf("%d\n", powermod(x, y, n));
		}
	}
	return 0;
}
