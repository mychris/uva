#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int sequence[32];

bool rec(int sum, int idx, int target, int end) {
	if (sum > target) {
		return false;
	}
	if (sum == target) {
		return true;
	}
	if (idx == end) {
		return false;
	}
	return rec(sum + sequence[idx], idx + 1, target, end) ||
	       rec(sum, idx + 1, target, end);
}

bool check(int d) {
	for (int i = 1; i < d; ++i) {
		if (rec(0, 0, sequence[i], i)) {
			return false;
		}
	}
	return true;
}

int main() {
	size_t tc;
	for (tc = 1;; ++tc) {
		int d;
		bool result = true;
		if (scanf("%d", &d) != 1) {
			break;
		}
		printf("Case #%zu:", tc);
		for (int i = 0; i < d; ++i) {
			scanf("%d", &sequence[i]);
			printf(" %d", sequence[i]);
			if (i > 0 && sequence[i] <= sequence[i - 1]) {
				result = false;
			}
		}
		if (result && check(d)) {
			puts("\nThis is an A-sequence.");
		} else {
			puts("\nThis is not an A-sequence.");
		}
	}
	return 0;
}
