#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static char result[1000001];

static int next(int n) {
	int ret = n;
	while (n) {
		ret += n % 10;
		n /= 10;
	}
	return ret;
}

int main() {
	for (int i = 1; i <= 1000000; ++i) {
		int n = i;
		if (result[n]) {
			continue;
		}
		n = next(n);
		while (n <= 1000000 && !result[n]) {
			result[n] = 1;
			n = next(n);
		}
		if (!result[i]) {
			printf("%d\n", i);
		}
	}
	return 0;
}
