#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

static int counts[128];
static char out_buff[5 * 2000000];
static char cur_buff[1024];

int main() {
	int n;
	while (scanf("%d", &n) != EOF && n > 0) {
		memset(counts, 0, sizeof(counts));
		for (int i = 0; i < n; ++i) {
			int x;
			scanf("%d", &x);
			if (x >= 0 && x < 128) {
				++counts[x];
			}
		}
		char *out = out_buff;
		bool first = true;
		for (int i = 0; i < 128; ++i) {
			if (counts[i]) {
				int j = counts[i];
				if (first) {
					out += sprintf(out, "%d", i);
					first = false;
					--j;
				}
				int s = sprintf(cur_buff, " %d", i);
				while (j--) {
					memcpy(out, cur_buff, (size_t) s);
					out += s;
				}
			}
		}
		*out = '\0';
		puts(out_buff);
	}
}
