#include <cstdio>
#include <cstring>

using namespace std;

static int orig_dice[4][3] = {
    {0, 2, 0},
    {4, 1, 3},
    {0, 5, 0},
    {0, 6, 0},
};

int main() {
	int commands;
	while (scanf("%d\n", &commands) == 1 && commands > 0) {
		int dice[4][3];
		memcpy(dice, orig_dice, sizeof(orig_dice));
		while (commands--) {
			char command[20];
			scanf("%s\n", command);
			if (strcmp(command, "north") == 0) {
				int tmp = dice[0][1];
				dice[0][1] = dice[1][1];
				dice[1][1] = dice[2][1];
				dice[2][1] = dice[3][1];
				dice[3][1] = tmp;
			} else if (strcmp(command, "south") == 0) {
				int tmp = dice[0][1];
				dice[0][1] = dice[3][1];
				dice[3][1] = dice[2][1];
				dice[2][1] = dice[1][1];
				dice[1][1] = tmp;
			} else if (strcmp(command, "east") == 0) {
				int tmp = dice[1][0];
				dice[1][0] = dice[1][1];
				dice[1][1] = dice[1][2];
				dice[1][2] = dice[3][1];
				dice[3][1] = tmp;
			} else if (strcmp(command, "west") == 0) {
				int tmp = dice[1][0];
				dice[1][0] = dice[3][1];
				dice[3][1] = dice[1][2];
				dice[1][2] = dice[1][1];
				dice[1][1] = tmp;
			} else {
				printf("unkown %s\n", command);
			}
		}
		printf("%d\n", dice[1][1]);
	}
	return 0;
}
