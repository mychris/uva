#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static const int coins[5] = {1, 5, 10, 25, 50};

static size_t ways[30001];

static size_t coin_exchange_ways(const int change) {
	int c, r, coin;
	memset(ways, 0, sizeof(ways));
	ways[0] = 1;

	for (c = 0; c < 5; ++c) {
		coin = coins[c];
		for (r = coin; r <= change; ++r) {
			ways[r] += ways[r - coin];
		}
	}

	return ways[change];
}

int main() {
	int change;
	while (scanf("%d\n", &change) != EOF) {
		size_t n_ways = coin_exchange_ways(change);
		if (n_ways == 1) {
			printf("There is only 1 way to produce %d cents change.\n", change);
		} else {
			printf("There are %zu ways to produce %d cents change.\n", n_ways, change);
		}
	}
	return 0;
}
