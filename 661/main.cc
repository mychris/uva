#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int device_state[30];
static int device_consumption[30];

int main() {
	int i, sequence = 1;
	;
	int n_devices, n_operations, capacity;
	while (scanf("%d %d %d\n", &n_devices, &n_operations, &capacity) == 3) {
		if (n_devices == 0 && n_operations == 0 && capacity == 0) {
			break;
		}
		memset(&device_state[0], 0, sizeof(device_state));
		int cur_consumption = 0;
		int highest_consumption = 0;
		for (i = 1; i <= n_devices; ++i) {
			scanf("%d\n", &device_consumption[i]);
		}
		while (n_operations--) {
			int device;
			scanf("%d\n", &device);
			if (device_state[device]) {
				cur_consumption -= device_consumption[device];
			} else {
				cur_consumption += device_consumption[device];
				if (cur_consumption > highest_consumption) {
					highest_consumption = cur_consumption;
				}
			}
			device_state[device] = !device_state[device];
		}

		printf("Sequence %d\n", sequence);
		if (highest_consumption > capacity) {
			printf("Fuse was blown.\n\n");
		} else {
			printf("Fuse was not blown.\nMaximal power consumption was %d amperes.\n\n",
			       highest_consumption);
		}
		++sequence;
	}
	return 0;
}
