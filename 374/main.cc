#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static long long pow_mod(long long base, long long exp, long long const mod) {
	if (base == 0 || mod == 1) {
		return 0;
	}
	long long result = 1;
	base = base % mod;
	while (exp) {
		if (exp & 1) {
			result = (result * base) % mod;
		}
		exp >>= 1;
		base = (base * base) % mod;
	}
	return result;
}

int main() {
	long long B, P, M;
	while (scanf("%lld %lld %lld", &B, &P, &M) == 3) {
		printf("%lld\n", pow_mod(B, P, M));
	}
	return 0;
}
