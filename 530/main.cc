#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cinttypes>
#include <cstdio>

using namespace std;

uint64_t binom(uint64_t n, uint64_t k) {
	if (k == 0) {
		return 1;
	} else if (k > n / 2) {
		return binom(n, n - k);
	} else {
		return n * binom(n - 1, k - 1) / k;
	}
}

int main() {
	uint64_t n, k;
	while (scanf("%" SCNu64 " %" SCNu64 "\n", &n, &k) == 2 && (n > 0 || k > 0)) {
		assert(n >= k);
		printf("%" PRIu64 "\n", binom(n, k));
	}
	return 0;
}
