#include <cstdio>

using namespace std;

/* clang-format off */
static const char *out_string[] = {
    "BCG ", "BGC ", "CBG ", "CGB ", "GBC ", "GCB ",
};
/* clang-format on */

int main() {
	int B[3], G[3], C[3], key, i;

	while (scanf("%d %d %d %d %d %d %d %d %d\n",
	             &B[0],
	             &G[0],
	             &C[0],
	             &B[1],
	             &G[1],
	             &C[1],
	             &B[2],
	             &G[2],
	             &C[2]) == 9) {
		// BGC | BCG | GBC | GCB | CBG | CGB
		long BGC = G[0] + C[0] + B[1] + C[1] + B[2] + G[2];
		long BCG = G[0] + C[0] + B[1] + G[1] + B[2] + C[2];
		long GBC = B[0] + C[0] + G[1] + C[1] + B[2] + G[2];
		long GCB = B[0] + C[0] + B[1] + G[1] + G[2] + C[2];
		long CBG = B[0] + G[0] + G[1] + C[1] + B[2] + C[2];
		long CGB = B[0] + G[0] + B[1] + C[1] + G[2] + C[2];

		long out[] = {BCG, BGC, CBG, CGB, GBC, GCB};

		key = 0;
		for (i = 1; i < 6; ++i) {
			if (out[i] < out[key]) {
				key = i;
			}
		}

		printf("%s%ld\n", out_string[key], out[key]);
	}
	return 0;
}
