#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

#define MAX 16

static int is_prime[MAX + MAX + 1];

static int check_prime(int n) {
	if (n <= 1 || n > MAX + MAX) {
		return false;
	}
	for (int i = 2; i < n; ++i) {
		if (n % i == 0) {
			return false;
		}
	}
	return true;
}

static void init(void) {
	for (int i = 0; i <= MAX + MAX; ++i) {
		is_prime[i] = check_prime(i);
	}
}

static int n;
static int numbers[MAX];
static int solution[MAX];

static void run(int pos) {
	if (pos == n) {
		if (!is_prime[solution[0] + solution[n - 1]]) {
			return;
		}
		for (int i = 0; i < n - 1; ++i) {
			printf("%d ", solution[i]);
		}
		printf("%d\n", solution[n - 1]);
		return;
	}
	for (int i = 0; i < n; ++i) {
		if (numbers[i] && is_prime[solution[pos - 1] + numbers[i]]) {
			solution[pos] = numbers[i];
			numbers[i] = 0;
			run(pos + 1);
			numbers[i] = solution[pos];
			solution[pos] = 0;
		}
	}
}

int main() {
	init();
	int the_case = 1;
	if (scanf("%d\n", &n) != 1) {
		return 0;
	}
	for (;;) {
		if (n > MAX || n <= 0 || n % 2 != 0) {
			continue;
		}
		printf("Case %d:\n", the_case++);
		for (int i = 0; i < n; ++i) {
			numbers[i] = i + 1;
		}
		numbers[0] = 0;
		solution[0] = 1;
		run(1);
		if (scanf("%d\n", &n) != 1) {
			break;
		}
		putchar('\n');
	}
	return 0;
}
