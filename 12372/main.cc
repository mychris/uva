#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int a = 0, b = 0, c = 0;
		scanf("%d %d %d\n", &a, &b, &c);
		if (a <= 20 && b <= 20 && c <= 20) {
			printf("Case %d: good\n", the_case);
		} else {
			printf("Case %d: bad\n", the_case);
		}
	}
	return 0;
}
