#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>

using namespace std;

static long long calc(long long const n_houses) {
	long long y = (n_houses * n_houses + n_houses) / 2;
	long long x = (long long) sqrt((double) y);
	if (2 * x * x == (n_houses * n_houses + n_houses)) {
		return x;
	}
	return 0;
}

int main() {
	long long n_houses;
	long long cnt = 0;
	for (n_houses = 8; cnt < 10; ++n_houses) {
		long long result = calc(n_houses);
		if (result) {
			printf("%10lld%10lld\n", result, n_houses);
			++cnt;
		}
	}
	return 0;
}
