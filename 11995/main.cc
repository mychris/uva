#include <cstdio>
#include <queue>
#include <stack>

using namespace std;

int main() {
	int n;
	while (scanf("%d", &n) != EOF && n > 0) {
		stack<int> stack;
		bool stack_ok = true;
		queue<int> queue;
		bool queue_ok = true;
		priority_queue<int> prio_queue;
		bool prio_queue_ok = true;

		while (n--) {
			int a, b;
			scanf("%d %d", &a, &b);
			switch (a) {
			case 1:
				if (stack_ok) {
					stack.push(b);
				}
				if (queue_ok) {
					queue.push(b);
				}
				if (prio_queue_ok) {
					prio_queue.push(b);
				}
				break;
			case 2:
				if (stack_ok) {
					if (stack.empty() || stack.top() != b) {
						stack_ok = false;
					} else {
						stack.pop();
					}
				}
				if (queue_ok) {
					if (queue.empty() || queue.front() != b) {
						queue_ok = false;
					} else {
						queue.pop();
					}
				}
				if (prio_queue_ok) {
					if (prio_queue.empty() || prio_queue.top() != b) {
						prio_queue_ok = false;
					} else {
						prio_queue.pop();
					}
				}
				break;
			default:
				printf("invalid command %d\n", a);
				return 1;
			}
		}
		if (!stack_ok && !queue_ok && !prio_queue_ok) {
			puts("impossible");
			continue;
		}
		int cnt = 0;
		cnt += (stack_ok) ? 1 : 0;
		cnt += (queue_ok) ? 1 : 0;
		cnt += (prio_queue_ok) ? 1 : 0;
		if (cnt > 1) {
			puts("not sure");
		} else if (stack_ok) {
			puts("stack");
		} else if (queue_ok) {
			puts("queue");
		} else if (prio_queue_ok) {
			puts("priority queue");
		}
	}
	return 0;
}
