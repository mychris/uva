#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstdlib>

using namespace std;

static char grids[2][101][101];

// R, S, and P are very close in the ascii table
// P is the smallest with dec value of 80 (0x50)
static char archenemy[4] = {
    'S', // paper is only defeated by scissors
    'A', // invalid
    'P', // rock is only defeated by paper
    'R', // scissors is only defeated by rock
};

int main() {
	int _N;
	if (scanf("%d", &_N) != 1) {
		return 1;
	}
	while (_N--) {
		int height, width, days;
		if (scanf("%d %d %d", &height, &width, &days) != 3) {
			return 2;
		}
		for (int i = 0; i < height; ++i) {
			if (width) {
				scanf("%s", &grids[0][i][0]);
			}
			grids[0][i][width] = '\0';
			grids[1][i][width] = '\0';
		}
		int active = 0, next = 1;
		while (days--) {
			for (int x = 0; x < width; ++x) {
				for (int y = 0; y < height; ++y) {
					char current = grids[active][y][x];
#if !defined(NDEBUG)
					if (current != 'R' && current != 'S' && current != 'P') {
						fprintf(stderr, "0x%X in active grid\n", (unsigned) current);
						abort();
					}
#endif
					char target = archenemy[current - 'P'];
					if (x > 0 && grids[active][y][x - 1] == target) {
						current = target;
					} else if (x < width - 1 && grids[active][y][x + 1] == target) {
						current = target;
					} else if (y > 0 && grids[active][y - 1][x] == target) {
						current = target;
					} else if (y < height - 1 && grids[active][y + 1][x] == target) {
						current = target;
					}
					grids[next][y][x] = current;
				}
			}
			active = 1 - active;
			next = 1 - next;
		}
		for (int i = 0; i < height; ++i) {
			puts(grids[active][i]);
		}
		if (_N) {
			puts("");
		}
	}
	return 0;
}
