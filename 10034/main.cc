#include <cstddef>
#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>

using namespace std;

struct distance {
	size_t from;
	size_t to;
	double distance;
};

bool cmp_distance(struct distance a, struct distance b) {
	return a.distance < b.distance;
}

size_t n_points;
double points[128][2];
int connected[128];
struct distance distances[128 * 128];

int main() {
	int N;
	scanf("%d", &N);
	while (N--) {
		scanf("%zu", &n_points);
		for (size_t i = 0; i < n_points; ++i) {
			scanf("%lf %lf", &points[i][0], &points[i][1]);
		}
		double result = 0.0;
		size_t a = 0, b = 0, c = 0;
		for (a = 0; a < n_points; a++) {
			for (b = a + 1; b < n_points; b++) {
				distances[c].from = a;
				distances[c].to = b;
				distances[c].distance =
				    sqrt((points[a][0] - points[b][0]) * (points[a][0] - points[b][0]) +
				         (points[a][1] - points[b][1]) * (points[a][1] - points[b][1]));
				++c;
			}
		}
		sort(distances, distances + c, cmp_distance);
		memset(connected, 0, sizeof(connected));
		connected[0] = 1;
		for (a = 1; a < n_points; ++a) {
			for (b = 0; b < c; ++b) {
				if ((connected[distances[b].from] && !connected[distances[b].to]) ||
				    (!connected[distances[b].from] && connected[distances[b].to])) {
					connected[distances[b].from] = 1;
					connected[distances[b].to] = 1;
					result += distances[b].distance;
					break;
				}
			}
		}
		printf("%.2lf\n", result);
		if (N) {
			puts("");
		}
	}
	return 0;
}
