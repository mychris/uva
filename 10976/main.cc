#include <cstdio>

using namespace std;

/*
 * 1/k = 1/y + 1/x
 * y = (k * x) / (x - k)
 */

static int solutions[10000];

int main() {
	int k;
	while (scanf("%d\n", &k) == 1 && k > 0 && k <= 10000) {
		int x, cnt = 0;
		for (x = k + 1; x <= 2 * k; ++x) {
			if ((k * x) % (x - k) == 0) {
				solutions[cnt++] = x;
			}
		}
		printf("%d\n", cnt);
		for (x = 0; x < cnt; ++x) {
			printf("1/%d = 1/%d + 1/%d\n",
			       k,
			       (k * solutions[x]) / (solutions[x] - k),
			       solutions[x]);
		}
	}
	return 0;
}
