#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

static int length[4];

int main() {
	int _C;
	scanf("%d", &_C);
	while (_C--) {
		scanf("%d %d %d %d", length, length + 1, length + 2, length + 3);

		if (length[0] == length[1] && length[1] == length[2] &&
		    length[2] == length[3]) {
			puts("square");
			continue;
		}
		sort(length, length + 4);
		if (length[0] == length[1] && length[2] == length[3]) {
			puts("rectangle");
		} else if (length[0] + length[1] + length[2] >= length[3]) {
			puts("quadrangle");
		} else {
			puts("banana");
		}
	}
	return 0;
}
