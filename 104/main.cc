#include <cstring>
#include <iostream>

using namespace std;

#define MAX_DIMENSION (20)
typedef unsigned int uint;

static uint N;
static double table[MAX_DIMENSION][MAX_DIMENSION];
static double cache_value[MAX_DIMENSION][MAX_DIMENSION + 1];
static uint result_length = 0;
static uint result_path[MAX_DIMENSION + 1];

static void
find(const uint start_currency, const uint length, const double value, uint *route) {
	if (length > N || length + 1 >= result_length) {
		return;
	}
	uint before_currency = route[length - 1];
	if (value * table[before_currency][start_currency] > 1.01) {
		if (result_length > length + 1) {
			result_length = length + 1;
			for (uint i = 0; i < length; ++i) {
				result_path[i] = route[i];
			}
			result_path[length] = start_currency;
		}
		return;
	}
	for (uint test_length = 0; test_length < length; ++test_length) {
		if (cache_value[before_currency][test_length] >= value) {
			return;
		}
	}
	cache_value[before_currency][length] = value;
	for (uint i = 0; i < N; ++i) {
		if (i != route[length - 1]) {
			route[length] = i;
			find(start_currency, length + 1, value * table[before_currency][i], route);
		}
	}
}

static void run(void) {
	result_length = 100000;
	memset(cache_value, 0, sizeof(cache_value));
	for (uint i = 0; i < N; ++i) {
		uint route[MAX_DIMENSION + 1];
		route[0] = i;
		find(i, 1, 1.0, &route[0]);
	}
	if (result_length > MAX_DIMENSION) {
		puts("no arbitrage sequence exists");
	} else {
		for (uint i = 0; i < result_length - 1; ++i) {
			printf("%u ", result_path[i] + 1);
		}
		printf("%u\n", result_path[result_length - 1] + 1);
	}
}

int main() {
	for (uint i = 0; i < MAX_DIMENSION; ++i) {
		table[i][i] = 1.0;
	}
	while (scanf("%u", &N) == 1) {
		for (uint i = 0; i < N; ++i) {
			for (uint j = 0; j < N; ++j) {
				if (i != j) {
					scanf("%lf", &table[i][j]);
				}
			}
		}
		run();
	}
}
