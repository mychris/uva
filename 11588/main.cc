#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

static unsigned int map[256];

int main() {
	int _C, the_case = 1;
	scanf("%d\n", &_C);
	while (_C--) {
		unsigned int rows, cols, m, n;
		memset(map, 0, sizeof(map));
		if (scanf("%u %u %u %u\n", &rows, &cols, &m, &n) != 4) {
			break;
		}
		for (unsigned int i = 0; i < rows; ++i) {
			string line;
			getline(cin, line);
			for (unsigned int j = 0; j < cols; ++j) {
				map[(size_t) line[j]] += 1;
			}
		}

		unsigned int max = 0;
		for (unsigned int i = 'A'; i <= 'Z'; ++i) {
			if (map[i] > max) {
				max = map[i];
			}
		}
		unsigned int result = 0;
		for (int i = 'A'; i <= 'Z'; ++i) {
			result += map[i] * ((map[i] == max) ? m : n);
		}
		printf("Case %d: %u\n", the_case++, result);
	}
	return 0;
}
