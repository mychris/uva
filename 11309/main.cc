#include <cstdio>

using namespace std;

static int hh[2];
static int mm[2];

static void next_time(void) {
	++mm[1];
	if (mm[1] < 10) {
		return;
	}

	mm[1] = 0;
	++mm[0];

	if (mm[0] < 6) {
		return;
	}

	mm[0] = 0;
	++hh[1];

	if ((hh[0] < 2 && hh[1] < 10) || hh[1] < 4) {
		return;
	}

	hh[1] = 0;
	++hh[0];
	;

	if (hh[0] <= 2) {
		return;
	}

	hh[0] = 0;
}

static int is_pal(void) {
	if (!hh[0]) {
		if (!hh[1]) {
			return !mm[0] || mm[0] == mm[1];
		}
		return hh[1] == mm[1];
	}
	return hh[0] == mm[1] && hh[1] == mm[0];
}

int main() {
	int _C;
	scanf("%d", &_C);
	while (_C--) {
		int h, m;
		scanf("%d:%d", &h, &m);
		hh[0] = h / 10;
		hh[1] = h % 10;
		mm[0] = m / 10;
		mm[1] = m % 10;

		next_time();
		while (!is_pal()) {
			next_time();
		}

		printf("%d%d:%d%d\n", hh[0], hh[1], mm[0], mm[1]);
	}
	return 0;
}
