#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

#define MIN 13
#define MAX 101

static int cache[MAX];

static int run(int n_regions) {
	int m, i;
	if (cache[n_regions] > 0) {
		return cache[n_regions];
	}
	char turned_off[MAX];
	for (m = 2;; ++m) {
		int running = n_regions;
		int cur = 1;
		memset(&turned_off[0], 0, sizeof(char) * MAX);
		while (1) {
			if (cur == 13 && running > 1) {
				break;
			}
			turned_off[cur] = 1;
			running -= 1;
			if (running == 0) {
				break;
			}
			for (i = 0; i < m;) {
				cur += 1;
				if (cur > n_regions) {
					cur = 0;
					continue;
				}
				if (!turned_off[cur]) {
					++i;
				}
			}
		}
		if (running == 0) {
			cache[n_regions] = m;
			return m;
		}
	}
	return 0;
}

int main() {
	int n;
	memset(&cache[0], 0, sizeof(int) * MAX);
	cache[13] = 1;
	while (scanf("%d", &n) == 1 && n >= MIN && n < MAX) {
		printf("%d\n", run(n));
	}
	return 0;
}
