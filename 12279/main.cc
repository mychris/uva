#include <cstdio>

using namespace std;

int main() {
	int the_case = 1;
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		int cnt = 0;
		while (n--) {
			int x;
			scanf("%d", &x);
			if (x) {
				++cnt;
			} else {
				--cnt;
			}
		}
		printf("Case %d: %d\n", the_case++, cnt);
	}
	return 0;
}
