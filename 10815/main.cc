#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

#include <algorithm>
#include <set>
#include <string>
#include <vector>

using namespace std;

#define TO_UPPER(x) ((x) -0x20)
#define TO_LOWER(x) ((x) + 0x20)

static char word[201];
static set<string> words;
static vector<string> sorted_words;

// -1 = NUL
// 0 = skip
// 1 = OK
// 2 = to lower
static char table[256] = {};

void handle_word(char *start) {
	string input(start);
	if (words.find(input) != words.end()) {
		return;
	}
	words.insert(input);
	auto pos = upper_bound(sorted_words.begin(), sorted_words.end(), input);
	sorted_words.insert(pos, move(input));
}

int main() {
	table[0] = -1;
	for (size_t i = (size_t) 'a'; i <= (size_t) 'z'; ++i) {
		table[i] = 1;
		table[TO_UPPER(i)] = 2;
	}
	while (scanf("%200s", word) == 1) {
		char *start = &word[0];
		// strings like hello-world count as two, hello and world
		for (;;) {
			while (table[(size_t) (*start)] == 0) {
				++start;
			}
			char *end = start;
			while (*end && table[(size_t) (*end)] != 0) {
				if (table[(size_t) (*end)] == 2) {
					*end = TO_LOWER(*end);
				}
				++end;
			}
			*end = '\0';
			if (start == end) {
				break;
			}
			handle_word(start);
			start = end + 1;
		}
		memset(&word[0], '\0', sizeof(word));
	}
	for (auto it = sorted_words.begin(); it < sorted_words.end(); ++it) {
		printf("%s\n", it->c_str());
	}
	return 0;
}
