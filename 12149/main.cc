#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int results[128];

static inline int count(const int n, const int s) {
	const int in_row = n - s + 1;
	const int in_col = n - s + 1;
	return in_row * in_col;
}

int main() {
	for (int n = 1; n <= 100; ++n) {
		int result = 0;
		for (int s = 1; s <= n; ++s) {
			result += count(n, s);
		}
		results[n] = result;
	}
	int n;
	while (scanf("%d", &n) == 1 && n > 0) {
		printf("%d\n", results[n]);
	}
	return 0;
}
