#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int matrix[128][128];
static int col_sum[128];
static int size;

static int check(void) {
	int row_count = 0, col_count = 0, last_row = 0, last_col = 0;

	for (int col = 0; col < size; ++col) {
		col_sum[col] = 0;
	}

	for (int row = 0; row < size; ++row) {
		int sum = 0;
		for (int col = 0; col < size; ++col) {
			sum += matrix[row][col];
			col_sum[col] += matrix[row][col];
		}
		if (sum % 2 != 0) {
			++row_count;
			last_row = row + 1;
		}
	}

	for (int col = 0; col < size; ++col) {
		if (col_sum[col] % 2 != 0) {
			++col_count;
			last_col = col + 1;
		}
	}

	if (col_count > 1 || row_count > 1) {
		return -1;
	}
	if (row_count == 0 && col_count == 0) {
		return 0;
	}
	return last_row * 1000 + last_col;
}

int main() {
	while (scanf("%d\n", &size) == 1 && size > 0) {
		for (int row = 0; row < size; ++row) {
			for (int col = 0; col < size; ++col) {
				scanf("%d", &matrix[row][col]);
			}
		}

		int row = check();
		if (row == 0) {
			puts("OK");
		} else if (row < 0) {
			puts("Corrupt");
		} else {
			printf("Change bit (%d,%d)\n", row / 1000, row % 1000);
		}
	}

	return 0;
}
