#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdint>
#include <cstdio>

using namespace std;

#define SUITE_COUNT(x) (suite_state[(size_t) (x)] & 0xF)
#define HAS_JACK(x) ((suite_state[(size_t) (x)] & 0x40) != 0)
#define SET_HAS_JACK(x) suite_state[(size_t) (x)] |= 0x40
#define HAS_QUEEN(x) ((suite_state[(size_t) (x)] & 0x80) != 0)
#define SET_HAS_QUEEN(x) suite_state[(size_t) (x)] |= 0x80
#define HAS_KING(x) ((suite_state[(size_t) (x)] & 0x100) != 0)
#define SET_HAS_KING(x) suite_state[(size_t) (x)] |= 0x100
#define HAS_ACE(x) ((suite_state[(size_t) (x)] & 0x200) != 0)
#define SET_HAS_ACE(x) suite_state[(size_t) (x)] |= 0x200

// lowest 4 bits are the count
// bit 6: jack present
// bit 7: queen present
// bit 8: king present
// bit 9: ace present
static uint32_t suite_state[256];
static size_t suites[4] = {
    (size_t) 'S',
    (size_t) 'H',
    (size_t) 'D',
    (size_t) 'C',
};

static bool is_suite_stopped(char suite) {
	return HAS_ACE(suite) || (HAS_KING(suite) && SUITE_COUNT(suite) >= 2) ||
	       (HAS_QUEEN(suite) && SUITE_COUNT(suite) >= 3);
}

static bool is_stopped(void) {
	return is_suite_stopped('S') && is_suite_stopped('H') && is_suite_stopped('D') &&
	       is_suite_stopped('C');
}

int main() {
	for (;;) {
		char hand[13][3];
		if (scanf("%s %s %s %s %s %s %s %s %s %s %s %s %s",
		          &hand[0][0],
		          &hand[1][0],
		          &hand[2][0],
		          &hand[3][0],
		          &hand[4][0],
		          &hand[5][0],
		          &hand[6][0],
		          &hand[7][0],
		          &hand[8][0],
		          &hand[9][0],
		          &hand[10][0],
		          &hand[11][0],
		          &hand[12][0]) != 13) {
			return 0;
		}
		suite_state['S'] = suite_state['H'] = suite_state['D'] = suite_state['C'] = 0;
		for (size_t i = 0; i < 13; ++i) {
			suite_state[(size_t) hand[i][1]]++;
			if (hand[i][0] == 'A')
				SET_HAS_ACE(hand[i][1]);
			if (hand[i][0] == 'K')
				SET_HAS_KING(hand[i][1]);
			if (hand[i][0] == 'Q')
				SET_HAS_QUEEN(hand[i][1]);
			if (hand[i][0] == 'J')
				SET_HAS_JACK(hand[i][1]);
		}

		int points = 0;
		int add_points = 0;
		bool stopped = is_stopped();
		char most_cards = 'X';
		int most_cards_count = 0;
		for (size_t i = 0; i < 4; ++i) {
			size_t suite = suites[i];
			int count = SUITE_COUNT(suite);
			if (count > most_cards_count) {
				most_cards_count = count;
				most_cards = (char) suite;
			}
			// 1
			if (HAS_ACE(suite)) {
				points += 4;
			}
			// 2
			if (HAS_KING(suite)) {
				points += 3;
				if (count == 1) {
					points -= 1;
				}
			}
			// 3
			if (HAS_QUEEN(suite)) {
				points += 2;
				if (count < 3) {
					points -= 1;
				}
			}
			// 4
			if (HAS_JACK(suite) && count >= 4) {
				points += 1;
			}

			// 5 + 6 + 7
			switch (count) {
			case 2:
				add_points += 1;
				break;
			case 1:
			case 0:
				add_points += 2;
				break;
			}
		}

		if (points + add_points < 14) {
			puts("PASS");
		} else if (stopped && points >= 16) {
			puts("BID NO-TRUMP");
		} else if (points + add_points >= 14) {
			printf("BID %c\n", most_cards);
		} else {
			assert(0);
		}
	}
	return 0;
}
