#include <cstdio>

using namespace std;

int main() {
	unsigned long long n, k;
	while (scanf("%llu %llu", &n, &k) == 2) {
		unsigned long long smoked = n, buts = n;
		while (buts >= k) {
			smoked += buts / k;
			buts = buts / k + buts % k;
		}
		printf("%llu\n", smoked);
	}
	return 0;
}
