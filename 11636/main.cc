#include <cstdio>

using namespace std;

static int calc(int const x) {
	int i = 1, count = 0;
	while (i < x) {
		i <<= 1;
		++count;
	}
	return count;
}

int main() {
	int x, the_case = 1;
	while (scanf("%d", &x) == 1 && x > 0) {
		printf("Case %d: %d\n", the_case++, calc(x));
	}
	return 0;
}
