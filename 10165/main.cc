#include <cstdio>

using namespace std;

int main() {
	int N;
	while (scanf("%d", &N) == 1 && N > 0) {
		int mask = 0;
		for (int i = 0; i < N; ++i) {
			int x;
			scanf("%d", &x);
			mask ^= x;
		}
		puts(mask ? "Yes" : "No");
	}
	return 0;
}
