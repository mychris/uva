#include <cstdio>

using namespace std;

static const int one_step = 9;

static int move_cw(const int from, const int to) {
	if (from < to) {
		return from + 40 - to;
	}
	return from - to;
}

static int move_ccw(const int from, const int to) {
	if (to < from) {
		return to + 40 - from;
	}
	return to - from;
}

int main() {
	int start, a, b, c;
	while (scanf("%d %d %d %d\n", &start, &a, &b, &c) == 4) {
		int result = 0;
		if (start == 0 && a == 0 && b == 0 && c == 0) {
			break;
		}
		result += 360 + 360;
		result += one_step * move_cw(start, a);
		result += 360;
		result += one_step * move_ccw(a, b);
		result += one_step * move_cw(b, c);
		printf("%d\n", result);
	}
	return 0;
}
