#include <cstdio>
#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>

using namespace std;

static inline int card_value(char const *card) {
	int face = (int) card[0];
	if (face >= 50 && face <= 57) {
		return face - 48;
	}
	return 10;
}

static char cards[52][3];

int main() {
	int _N;
	if (scanf("%d\n", &_N) != 1) {
		return 1;
	}
	for (int test_case = 1; test_case <= _N; ++test_case) {
		if (scanf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s "
		          "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s "
		          "%s %s %s %s %s %s %s %s %s %s\n",
		          cards[51],
		          cards[50],
		          cards[49],
		          cards[48],
		          cards[47],
		          cards[46],
		          cards[45],
		          cards[44],
		          cards[43],
		          cards[42],
		          cards[41],
		          cards[40],
		          cards[39],
		          cards[38],
		          cards[37],
		          cards[36],
		          cards[35],
		          cards[34],
		          cards[33],
		          cards[32],
		          cards[31],
		          cards[30],
		          cards[29],
		          cards[28],
		          cards[27],
		          cards[26],
		          cards[25],
		          cards[24],
		          cards[23],
		          cards[22],
		          cards[21],
		          cards[20],
		          cards[19],
		          cards[18],
		          cards[17],
		          cards[16],
		          cards[15],
		          cards[14],
		          cards[13],
		          cards[12],
		          cards[11],
		          cards[10],
		          cards[9],
		          cards[8],
		          cards[7],
		          cards[6],
		          cards[5],
		          cards[4],
		          cards[3],
		          cards[2],
		          cards[1],
		          cards[9]) != 52) {
			return 1;
		}
		size_t y = 0;
		size_t x = 25;

		for (int i = 0; i < 3; ++i) {
			int current_value = card_value(cards[x]);
			assert(current_value <= 10 && current_value >= 0);
			y += (size_t) current_value;
			x += (size_t) (10 - current_value) + 1;
		}
		// now all cards, from index 25 (included) until index x (excluded) are
		// removed.
		assert(x < 52);

		if (52 - x >= y) {
			printf("Case %d: %s\n", test_case, cards[52 - y]);
		} else {
			y -= (52 - x);
			printf("Case %d: %s\n", test_case, cards[25 - y]);
		}
	}
	return 0;
}
