#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int cache[10001];

static void init(void) {
	cache[0] = 1;
	int cur_fact = 1;
	for (int i = 1; i < 10001; ++i) {
		cur_fact *= i;
		while (cur_fact % 10 == 0) {
			cur_fact /= 10;
		}
		cur_fact %= 100000;
		cache[i] = cur_fact % 10;
	}
}

int main() {
	init();
	int n;
	while (scanf("%d\n", &n) == 1) {
		if (n < 10000) {
			putchar(' ');
		}
		if (n < 1000) {
			putchar(' ');
		}
		if (n < 100) {
			putchar(' ');
		}
		if (n < 10) {
			putchar(' ');
		}
		printf("%d -> %d\n", n, cache[n]);
	}
	return 0;
}
