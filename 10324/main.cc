#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

int main() {
	int the_case = 1;
	for (string line; getline(cin, line);) {
		if (cin.eof()) {
			break;
		}
		printf("Case %d:\n", the_case++);
		unsigned int x;
		scanf("%u\n", &x);
		while (x--) {
			unsigned int i, j;
			scanf("%u %u\n", &i, &j);
			if (i == j) {
				puts("Yes");
				continue;
			}
			if (i > j) {
				unsigned int tmp = i;
				i = j;
				j = tmp;
			}
			bool ok = true;
			char s = line[i++];
			while (i <= j) {
				if (line[i++] != s) {
					ok = false;
					break;
				}
			}
			puts((ok) ? "Yes" : "No");
		}
	}
	return 0;
}
