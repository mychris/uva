#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <unistd.h>

using namespace std;

uint64_t facts[32] = {};
uint64_t perm[32] = {};
char input[32] = {};
char output[32] = {};

char buffer[4096] = {};
ssize_t buffer_p = 0;
ssize_t buffer_e = 0;

int main() {
	int T;
	scanf("%d", &T);
	facts[0] = 1;
	for (uint64_t x = 1; x <= 20; ++x)
		facts[x] = facts[x - 1] * x;
	while (T--) {
		uint64_t n = 0, i = 0;
		scanf("%21s\n", input);
		scanf("%" SCNu64, &i);
		n = strlen(input);
		i %= facts[n];
		sort(input, &input[n]);
		for (uint64_t k = 0; k < n; ++k) {
			perm[k] = i / facts[n - 1 - k];
			i = i % facts[n - 1 - k];
		}
		for (uint64_t k = n - 1; k > 0; --k) {
			for (int64_t j = (int64_t) k - 1; j >= 0; --j) {
				if (perm[j] <= perm[k])
					perm[k]++;
			}
		}
		for (uint64_t k = 0; k < n; ++k)
			output[k] = input[perm[k]];
		output[n] = '\0';
		puts(output);
	}
	return 0;
}
