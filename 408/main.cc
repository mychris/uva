#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

int seen[1000001];

static int test(const int step, const int mod) {
	int cur = 0;
	int count = 0;
	memset(seen, 0, sizeof(seen));
	while (!seen[cur]) {
		seen[cur] = 1;
		cur = (cur + step) % mod;
		++count;
	}
	return count == mod;
}

int main() {
	int step, mod;
	while (scanf("%d %d", &step, &mod) == 2 && step > 0 && mod > 0) {
		int good = test(step, mod);
		printf("%10d%10d    %s\n\n", step, mod, good ? "Good Choice" : "Bad Choice");
	}
	return 0;
}
