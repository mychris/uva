#include <cstdio>

using namespace std;

static char input[2048];

int main() {
	while (fgets(input, sizeof(input), stdin) != nullptr) {
		if (input[0] == '*') {
			break;
		}
		int ctr = 0;
		int sum = 0;
		char *c = &input[0];
		while (*c) {
			switch (*c) {
			case 'W':
				sum += 64;
				break;
			case 'H':
				sum += 32;
				break;
			case 'Q':
				sum += 16;
				break;
			case 'E':
				sum += 8;
				break;
			case 'S':
				sum += 4;
				break;
			case 'T':
				sum += 2;
				break;
			case 'X':
				sum += 1;
				break;
			case '/':
				if (sum == 64) {
					++ctr;
				}
				sum = 0;
				break;
			}
			++c;
		}
		printf("%d\n", ctr);
	}
	return 0;
}
