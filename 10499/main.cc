#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	long long int n;
	while (scanf("%lld\n", &n) == 1 && n >= 0) {
		if (n <= 1) {
			puts("0%");
		} else {
			printf("%lld%%\n", n * 25);
		}
	}
	return 0;
}
