#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int best_val = 0;
		printf("Case #%d:\n", the_case);
		vector<string> best;
		best.reserve(10);
		for (int i = 0; i < 10; ++i) {
			int val;
			string url;
			cin >> url >> val;
			if (val > best_val) {
				best.clear();
				best_val = val;
			}
			if (val == best_val) {
				best.push_back(url);
			}
		}
		for (auto it = best.begin(); it != best.end(); ++it) {
			cout << *it << endl;
		}
	}
}
