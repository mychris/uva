#include <cstdio>
#include <cstring>
#include <queue>

using namespace std;

static int nodes[200];
static int adjmatr[200][200];
static int n_nodes;

static bool check(void) {
	queue<int> queue;
	queue.push(0);
	while (!queue.empty()) {
		int cur = queue.front();
		queue.pop();
		if (nodes[cur]) {
			// has color
			continue;
		}
		int possible_color = 3;
		for (int i = 0; possible_color && i < n_nodes; ++i) {
			if (adjmatr[cur][i]) {
				if (nodes[i] & possible_color) {
					possible_color ^= nodes[i];
				}
				queue.push(i);
			}
		}
		if (possible_color == 0) {
			return false;
		}
		if (possible_color == 3) {
			possible_color = 1;
		}
		nodes[cur] = possible_color;
	}
	return true;
}

int main() {
	while (scanf("%d\n", &n_nodes) == 1 && n_nodes > 0) {
		memset(nodes, 0, sizeof(nodes));
		memset(adjmatr, 0, sizeof(adjmatr));
		int edges;
		scanf("%d\n", &edges);
		for (int i = 0; i < edges; ++i) {
			int from, to;
			scanf("%d %d\n", &from, &to);
			adjmatr[from][to] = 1;
			adjmatr[to][from] = 1;
		}
		if (check()) {
			puts("BICOLORABLE.");
		} else {
			puts("NOT BICOLORABLE.");
		}
	}
	return 0;
}
