#include <cstdio>

using namespace std;

static bool test_nl_or_eof(FILE *stream) {
	int c = getc(stream);
	if (c == EOF) {
		return true;
	}
	ungetc(c, stream);
	return c == '\n';
}

static void remove_spaces(FILE *stream) {
	int c;
	for (;;) {
		c = getc(stream);
		if (c != ' ' && c != '\t') {
			ungetc(c, stream);
			return;
		}
	}
}

static double next(void) {
	int a = 0, b = 0;
	scanf("%d", &a);
	remove_spaces(stdin);
	for (int i = 0; i < 3; ++i) {
		// drop 斤
		getchar();
	}
	if (!test_nl_or_eof(stdin)) {
		scanf("%d", &b);
		remove_spaces(stdin);
		for (int i = 0; i < 3; ++i) {
			// drop 两
			getchar();
		}
	}
	return a * 0.5 + b * 0.05;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		double val = next();
		if ((int) val == val) {
			printf("Case %d: %d\n", the_case, (int) val);
		} else {
			printf("Case %d: %g\n", the_case, val);
		}
	}
}
