#include <cstdio>

using namespace std;

static int a[12];

int main() {
	int problems, the_case = 1;

	while (scanf("%d", &problems) == 1 && problems >= 0) {
		scanf("%d %d %d %d %d %d %d %d %d %d %d %d",
		      &a[0],
		      &a[1],
		      &a[2],
		      &a[3],
		      &a[4],
		      &a[5],
		      &a[6],
		      &a[7],
		      &a[8],
		      &a[9],
		      &a[10],
		      &a[11]);

		printf("Case %d:\n", the_case++);
		for (size_t i = 0; i < 12; i++) {
			int cur;
			scanf("%d", &cur);

			if (problems - cur < 0) {
				puts("No problem. :(");
			} else {
				problems -= cur;
				puts("No problem! :D");
			}
			problems += a[i];
		}
	}

	return 0;
}
