#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int c;
	while ((c = getchar()) != EOF) {
		if (c == 13 || c == 10) {
			putchar(c);
		} else {
			putchar(c - 7);
		}
	}
}
