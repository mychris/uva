#include <cstdio>

using namespace std;

static inline int f91(const int x) {
	return (x <= 100) ? f91(f91(x + 11)) : x - 10;
}

static inline int f91_2(const int x) {
	return (x <= 100) ? 91 : x - 10;
}

int main() {
	int x;
	while (scanf("%d\n", &x) != EOF && x != 0) {
		printf("f91(%d) = %d\n", x, f91_2(x));
	}
	return 0;
}
