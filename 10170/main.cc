#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>

using namespace std;

static inline uint64_t bsearch(const uint64_t target) {
	uint64_t first = UINT64_C(1);
	uint64_t last = target;
	uint64_t middle = (first + last) / 2;
	while (first <= last) {
		uint64_t current;
		const bool overflow = __builtin_mul_overflow(middle, (middle + 1), &current);
		current /= 2;
		if (!overflow && current < target) {
			first = middle + 1;
		} else if (!overflow && current == target) {
			return middle;
		} else {
			last = middle - 1;
		}
		middle = (first + last) / 2;
	}
	return first;
}

int main() {
	uint64_t S, D;
	while (scanf("%" SCNu64 " %" SCNu64, &S, &D) == 2) {
		D += (S * (S - 1)) / 2;
		printf("%" PRIu64 "\n", bsearch(D));
	}
	return 0;
}
