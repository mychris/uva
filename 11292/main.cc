#include <cstdio>
#include <cstdlib>

using namespace std;

static int cmp(const void *a, const void *b) {
	return *((const int *) b) - *((const int *) a);
}

static int diameters[20000];
static int n_diameters;
static int heights[20000];
static int n_heights;

static int calc(void) {
	if (n_heights < n_diameters) {
		return -1;
	}
	int result = 0;
	qsort(&diameters[0], (size_t) n_diameters, sizeof(int), &cmp);
	qsort(&heights[0], (size_t) n_heights, sizeof(int), &cmp);
	--n_diameters;
	--n_heights;
	if (diameters[0] > heights[0]) {
		return -1;
	}
	while (n_diameters >= 0 && n_heights >= 0) {
		if (heights[n_heights] >= diameters[n_diameters]) {
			result += heights[n_heights];
			--n_heights;
			--n_diameters;
		} else {
			--n_heights;
		}
	}
	if (n_diameters >= 0) {
		return -1;
	}
	return result;
}

int main() {
	int i, result;
	while (scanf("%d %d\n", &n_diameters, &n_heights) == 2) {
		if (n_diameters == 0 || n_heights == 0) {
			break;
		}
		for (i = 0; i < n_diameters; ++i) {
			scanf("%d\n", &diameters[i]);
		}
		for (i = 0; i < n_heights; ++i) {
			scanf("%d\n", &heights[i]);
		}
		result = calc();
		if (result < 0) {
			puts("Loowater is doomed!");
		} else {
			printf("%d\n", result);
		}
	}
	return 0;
}
