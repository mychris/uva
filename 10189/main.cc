#include <cstdio>

using namespace std;

int m = 0, n = 0;
char matrix[102][102];

#define INCR(_row, _col)                                      \
	do {                                                      \
		if (_row >= 0 && _row < n && _col >= 0 && _col < m && \
		    matrix[_row][_col] != '*') {                      \
			if (matrix[_row][_col] == '.') {                  \
				matrix[_row][_col] = '0';                     \
			}                                                 \
			++matrix[_row][_col];                             \
		}                                                     \
	} while (0)

void incr(int row, int col) {
	INCR(row - 1, col - 1);
	INCR(row - 1, col);
	INCR(row - 1, col + 1);
	INCR(row, col - 1);
	INCR(row, col);
	INCR(row, col + 1);
	INCR(row + 1, col - 1);
	INCR(row + 1, col);
	INCR(row + 1, col + 1);
}

int main() {
	int field = 1;
	int i, j;
	scanf("%d %d\n", &n, &m);
	for (;;) {
		printf("Field #%d:\n", field++);
		for (i = 0; i < n; ++i) {
			matrix[i][0] = ' ';
			while (matrix[i][0] != '*' && matrix[i][0] != '.') {
				matrix[i][0] = (char) getchar();
			}
			fgets(&matrix[i][1], m, stdin);
		}
		for (i = 0; i < n; ++i) {
			for (j = 0; j < m; ++j) {
				if (matrix[i][j] == '*') {
					incr(i, j);
				} else if (matrix[i][j] == '.') {
					matrix[i][j] = '0';
				}
			}
		}
		for (i = 0; i < n; ++i) {
			for (j = 0; j < m; ++j) {
				putchar(matrix[i][j]);
			}
			putchar('\n');
		}
		if (scanf("%d %d\n", &n, &m) == EOF || n == 0 || m == 0) {
			break;
		}
		putchar('\n');
	}
	return 0;
}
