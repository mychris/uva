#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

int cnt[256];

int main() {
	int _C = 0;
	scanf("%d\n", &_C);
	while (_C--) {
		int max = 0;
		memset(cnt, 0, sizeof(cnt));
		string line;
		getline(cin, line);
		for (char c : line) {
			c = (char) tolower((int) c);
			if (!islower(c)) {
				continue;
			}
			++cnt[(size_t) c];
			if (cnt[(size_t) c] > max) {
				max = cnt[(size_t) c];
			}
		}
		for (int c = 'a'; c <= 'z'; ++c) {
			if (cnt[c] == max) {
				putchar((char) c);
			}
		}
		putchar('\n');
	}
	return 0;
}
