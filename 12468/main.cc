#include <cstdio>

using namespace std;

int main() {
	int a, b;
	while (scanf("%d %d\n", &a, &b) == 2 && a >= 0 && b >= 0) {
		if (a > b) {
			int tmp = a;
			a = b;
			b = tmp;
		}
		int result = b - a;
		if (result > 50) {
			result = 100 - result;
		}
		printf("%d\n", result);
	}
	return 0;
}
