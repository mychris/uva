#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int n, k, p;
		scanf("%d %d %d", &n, &k, &p);
		int answ = (k + p) % n;
		printf("Case %d: %d\n", the_case, (answ == 0) ? n : answ);
	}

	return 0;
}
