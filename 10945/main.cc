#include <cctype>
#include <iostream>

using namespace std;

int main() {
	for (string line; getline(cin, line);) {
		if (line == "DONE") {
			break;
		}
		size_t len = line.size();
		size_t i = 0, j = len - 1;
		bool pal = true;
		while (i < j) {
			while (!isalpha(line[i])) {
				++i;
			}
			while (!isalpha(line[j])) {
				--j;
			}
			if (i >= j) {
				break;
			}
			if (tolower(line[i]) != tolower(line[j])) {
				pal = false;
				break;
			}
			++i;
			--j;
		}
		puts((pal) ? "You won't be eaten!" : "Uh oh..");
	}
	return 0;
}
