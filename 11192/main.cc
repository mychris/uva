#include <iostream>

using namespace std;

int main() {
	int groups;
	while ((cin >> groups) && groups > 0) {
		string line;
		cin >> line;

		int size = (int) line.size();
		int in_group = size / groups;
		for (int group = 0; group < groups; ++group) {
			int end = in_group * group;
			int start = end + in_group - 1;
			if (start >= size) {
				start = size - 1;
			}
			while (start >= end) {
				putchar(line[(size_t) (start--)]);
			}
		}
		putchar('\n');
	}
	return 0;
}
