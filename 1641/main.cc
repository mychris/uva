#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
	size_t h, w;
	while (scanf("%zu %zu\n", &h, &w) == 2 && (h > 0 && w > 0)) {
		unsigned long long int area = 0;
		bool inside = false;
		for (size_t i = 0; i < h; ++i) {
			for (size_t j = 0; j < w; ++j) {
				switch (getchar()) {
				case '.':
					if (inside) {
						area += 2;
					}
					break;
				case '/':
				case '\\':
					area += 1;
					inside = !inside;
					break;
				default:
					abort();
				}
			}
#if !defined(NDEBUG)
			int c = getchar();
			assert(c == '\n');
#else
			getchar();
#endif
		}
		assert((area & 1) == 0);
		printf("%llu\n", area >> 1);
	}
	return 0;
}
