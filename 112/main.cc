#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>

#include <limits>

using namespace std;

static int target_sum;

static char buffer[4096];
static size_t pos = 0;
static size_t len = 0;

static char peek() {
	for (;;) {
		if (pos == len) {
			size_t read = fread(buffer, sizeof(char), sizeof(buffer), stdin);
			if (read == 0) {
				assert(feof(stdin) != 0);
				return '\0';
			}
			len = (size_t) read;
			pos = 0;
		}
		++pos;
		if (!isspace(buffer[pos - 1])) {
			--pos;
			return buffer[pos];
		}
	}
	return '\0';
}

static char next() {
	char c = peek();
	++pos;
	return c;
}

static int next_int() {
	int number = 0;
	bool neg = false;
	if (peek() == '-') {
		neg = true;
		next();
	}
	while (isdigit(peek())) {
		number = number * 10 + (next() - '0');
	}
	if (peek() == '\0') {
		return numeric_limits<int>::min();
	}
	return (neg) ? number * -1 : number;
}

bool parse_tree(int current_sum) {
	int number = next_int();
	int sum = current_sum + number;
	bool left_empty = false, right_empty = false, result = false;
	assert(peek() == '(');
	next();
	if (peek() == ')') {
		next();
		left_empty = true;
	} else {
		result |= parse_tree(sum);
		assert(peek() == ')');
		next();
	}
	assert(peek() == '(');
	next();
	if (peek() == ')') {
		next();
		right_empty = true;
	} else {
		result |= parse_tree(sum);
		assert(peek() == ')');
		next();
	}
	if (right_empty && left_empty && sum == target_sum) {
		result = true;
	}
	return result;
}

int main() {
	while ((target_sum = next_int()) != numeric_limits<int>::min()) {
		assert(peek() == '(');
		next();
		if (peek() != ')') {
			bool result = parse_tree(0);
			puts((result) ? "yes" : "no");
		} else {
			puts("no");
		}
		assert(peek() == ')');
		next();
	}
	return 0;
}
