#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits.h>

#include <vector>

using namespace std;

typedef uint_fast8_t bitset_arr_type;
#define BITSET_POS_INDEX(p) ((p) / (sizeof(bitset_arr_type) * CHAR_BIT))
#define BITSET_POS_BIT(p) ((p) & (sizeof(bitset_arr_type) * CHAR_BIT - 1))

struct bitset {
	size_t len;
	size_t allocated;
	bitset_arr_type *bits;
	bitset(size_t number_of_bits) {
		len = number_of_bits;
		allocated =
		    (number_of_bits / sizeof(bitset_arr_type)) + sizeof(bitset_arr_type);
		bits = (bitset_arr_type *) malloc(allocated);
	}

	~bitset() {
		if (bits) {
			free(bits);
			bits = NULL;
			allocated = 0;
			bits = 0;
		}
	}

	void set(size_t position) {
		bits[BITSET_POS_INDEX(position)] |=
		    (bitset_arr_type) (((bitset_arr_type) 1) << BITSET_POS_BIT(position));
	}

	void set_all() {
		memset(bits, 0xFF, allocated);
	}

	void unset(size_t position) {
		bits[BITSET_POS_INDEX(position)] &=
		    (bitset_arr_type) ~(((bitset_arr_type) 1) << BITSET_POS_BIT(position));
	}

	void unset_all() {
		memset(bits, 0x00, allocated);
	}

	int get(size_t position) {
		return 0 != (bits[BITSET_POS_INDEX(position)] &
		             (((bitset_arr_type) 1) << BITSET_POS_BIT(position)));
	}
};

static size_t isqrt_size_t(size_t x) {
	size_t m, y, b;
	m = ((size_t) 1u) << ((sizeof(size_t) * CHAR_BIT) - 2u);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

static bitset *prime_sieve(const size_t limit) {
	bitset *nums = new bitset(limit + 1);
	nums->set_all();
	nums->unset(0);
	if (limit >= 1)
		nums->unset(1);
	for (size_t i = 2; i <= isqrt_size_t(limit + 1) + 1; ++i) {
		if (nums->get(i)) {
			for (size_t j = i * i; j <= limit; j += i)
				nums->unset(j);
		}
	}
	return nums;
}

static bitset *prime_table;
static vector<int> *prime_list;

static int num_divisors(int x) {
	int total = 1;
	for (int p : *prime_list) {
		int count = 0;
		while (x % p == 0) {
			x /= p;
			count++;
		}
		total *= count + 1;
	}
	return total;
}

static void largest_number_of_divisors(int L, int U, int *b, int *d) {
	int best = 0;
	int best_divisors = 0;
	for (int x = L; x <= U; ++x) {
		int divs = num_divisors(x);
		if (divs > best_divisors) {
			best = x;
			best_divisors = divs;
		}
	}
	*b = best;
	*d = best_divisors;
}

int main() {
	int N;
	scanf("%d\n", &N);
	prime_table = prime_sieve(isqrt_size_t(1000000000U));
	prime_list = new vector<int>;
	for (int i = 2; i <= (int) isqrt_size_t(1000000000U); ++i) {
		if (prime_table->get((size_t) i))
			prime_list->push_back(i);
	}
	while (N--) {
		int L, U, best, best_divisors;
		scanf("%d %d\n", &L, &U);
		largest_number_of_divisors(L, U, &best, &best_divisors);
		printf("Between %d and %d, %d has a maximum of %d divisors.\n",
		       L,
		       U,
		       best,
		       best_divisors);
	}
	return 0;
}
