include config.mk

I := input.txt
E := expected.txt

PROBLEMS != find * -type d -prune |grep '^[0-9]\+$$' |sort -n |tr '\n' ' '

ifeq ($(ONLINE_JUDGE),0)
JUDGE_TARGET := LOCAL
else
JUDGE_TARGET := ONLINE
endif

CFLAGS := $(CFLAGS_$(JUDGE_TARGET)) $(CFLAGS)
CXXFLAGS := $(CXXFLAGS_$(JUDGE_TARGET)) $(CXXFLAGS)
LDFLAGS := $(LDFLAGS_$(JUDGE_TARGET)) $(LDFLAGS)
OUT != echo solution-$(JUDGE_TARGET) |tr A-Z a-z
A != echo actual-$(JUDGE_TARGET).txt |tr A-Z a-z

all: $(PROBLEMS)

$(PROBLEMS): % : %/$(OUT)

run: ${PROBLEMS:%=run-%}
${PROBLEMS:%=%-run}: %-run : run-%
${PROBLEMS:%=run-%}: run-% : %/${A}
	@cat < $<

check: ${PROBLEMS:%=check-%}
${PROBLEMS:%=%-check}: %-check : check-%
${PROBLEMS:%=check-%}: check-% : %/$(A) %/$(E)
	@echo >&2 " [ DIFF ] $$(dirname $<)"
	@$(DIFF) $(DIFF_OPT) $(word 1,$^) $(word 2,$^)

%/$(OUT): %/*.cc
	@echo >&2 " [ CC   ] $$(dirname $<)"
	@$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $<

%/$(OUT): %/*.c
	@echo >&2 " [ CC   ] $$(dirname $<)"
	@$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

%/$(A): %/$(OUT) %/$(I)
	@echo >&2 " [ RUN  ] $$(dirname $<)"
	@$< < $(word 2,$^) > $@

fmt format:
	@clang-format -i */*.c 2>/dev/null || true
	@clang-format -i */*.cc 2>/dev/null || true

clean: clean-check clean-build
clean-build:
	@$(RM) */solution*
clean-check:
	@$(RM) */actual*

list:
	@echo $(PROBLEMS)
print-%:
	@echo $* = $($*)

info:
	@echo "Compiler (CC = $(CC)): $$($(CC) --version |sed 1q)"
	@echo "Compiler (CXX = $(CXX)): $$($(CXX) --version |sed 1q)"
	@echo "CFLAGS: $(CFLAGS)"
	@echo "CXXFLAGS: $(CXXFLAGS)"
	@echo "LDFLAGS: $(LDFLAGS)"
	@echo "Number of Problems: $(words $(PROBLEMS))"

help:
	@echo "Compile and test UVa problems"
	@echo " make all                  # Compile all solutions"
	@echo " make <prob number>        # Compile only the solutions for the specified problems"
	@echo " make run                  # Execute all solutions"
	@echo " make run-<prob number>    # Execute only the solutions for the specified problems"
	@echo " make check                # Verify all solutions"
	@echo " make check-<prob number>  # Verify only the solutions for the specified problems"
	@echo " make clean                # Remove files created by the build system"
	@echo
	@echo " make fmt                  # Format the soruce code of all solutions"
	@echo " make list                 # List all available solutions"
	@echo " make info                 # Information about the compilation environment"
	@echo
	@echo "Use the variable ONLINE_JUDGE to change compilation flags to the ones"
	@echo "used by the judge itself. Use the values 0 or 1."
	@echo "Configure the build by modifying config.mk."

.PHONY: all $(PROBLEMS)
.PHONY: run ${PROBLEMS:%=%-run} ${PROBLEMS:%=run-%}
.PHONY: check ${PROBLEMS:%=%-check} ${PROBLEMS:%=check-%}
.PHONY: fmt format list info help
.PHONY: clean clean-build clean-check

.SUFFIXES:
.DELETE_ON_ERROR:
