#include <cstdio>
#include <cstring>

using namespace std;

static int hartal_params[3651];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		memset(hartal_params, 0, sizeof(hartal_params));
		int days, parties;
		scanf("%d\n%d\n", &days, &parties);
		for (int i = 0; i < parties; ++i) {
			int param;
			scanf("%d\n", &param);
			int cur = param;
			while (cur <= days) {
				hartal_params[cur - 1] = 1;
				cur += param;
			}
		}
		int cnt = 0;
		for (int i = 0; i < days; ++i) {
			if (i % 7 == 5 || i % 7 == 6) {
				continue;
			}
			if (hartal_params[i]) {
				cnt++;
			}
		}
		printf("%d\n", cnt);
	}
	return 0;
}
