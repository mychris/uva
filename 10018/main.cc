#include <cstdio>

using namespace std;

static long long reverse(long long number) {
	long long tmp = 0;
	while (number) {
		tmp = tmp * 10 + number % 10;
		number /= 10;
	}
	return tmp;
}

int main() {
	long long _C;
	scanf("%lld", &_C);
	while (_C--) {
		long long n;
		scanf("%lld", &n);
		long long rev = reverse(n);
		if (n == rev) {
			printf("0 %lld\n", n);
			continue;
		}
		long long counter = 1;
		for (counter = 1; counter <= 1000; counter++) {
			n += rev;
			rev = reverse(n);
			if (n == rev) {
				break;
			}
		}
		printf("%lld %lld\n", counter, n);
	}
	return 0;
}
