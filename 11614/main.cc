#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cinttypes>
#include <cmath>
#include <cstdio>

using namespace std;

// triangle numbers:
//  n * (n + 1) / 2
//
//                          x   = y (y + 1) / 2
//                          x   = 1/2 ( y^2 + Y )
//                          x   = 1/2 ( y^2 + y + (1/2)^2 - (1/2)^2 )
//                          x   = 1/2 ( y + 1/2 )^2 - 1/8
//                          x   = 1/8 (2y + 1)^2 - 1/8
//                         8x   = (2y + 1)^2 - 1
//                     8x + 1   = (2y + 1)^2
//           +/- sqrt(8x + 1)   = 2y + 1
//       +/- sqrt(8x + 1) - 1   = 2y
//  1/2 (+/- sqrt(8x + 1) - 1)  =  y
int main() {
	size_t _N;
	if (scanf("%zu\n", &_N) != 1) {
		return 2;
	}
	while (_N--) {
		uint64_t x;
		while (scanf("%" SCNu64 "\n", &x) == 1) {
			uint64_t result = (uint64_t) (0.5 * (sqrt(8 * x + 1) - 1));
			printf("%" PRIu64 "\n", result);
		}
	}
	return 0;
}
