#include <cstdio>
#include <string>
#include <vector>

using namespace std;

static string indent[20];

static void swap(vector<char> &vars, size_t const a, size_t const b) {
	char tmp = vars[a];
	vars[a] = vars[b];
	vars[b] = tmp;
}

static string join_vars(vector<char> const &vars) {
	string ret(vars.size() + vars.size() - 1, vars[0]);
	for (size_t i = 1; i < vars.size(); ++i) {
		ret[i * 2 - 1] = ',';
		ret[i * 2] = vars[i];
	}
	return ret;
}

static void do_sort(vector<char> &vars, size_t const depth) {
	if (depth == 1) {
		printf("%sreadln(%s);\n", indent[depth].c_str(), join_vars(vars).c_str());
	}
	if (depth == vars.size()) {
		printf("%swriteln(%s)\n", indent[depth].c_str(), join_vars(vars).c_str());
		return;
	}
	for (size_t branch = 0; branch <= depth; ++branch) {
		if (branch == 0) {
			printf("%sif %c < %c then\n",
			       indent[depth].c_str(),
			       vars[depth - branch - 1],
			       vars[depth]);
			do_sort(vars, depth + 1);
		} else if (branch == depth) {
			printf("%selse\n", indent[depth].c_str());
			swap(vars, depth - branch, depth - branch + 1);
			do_sort(vars, depth + 1);
		} else {
			printf("%selse if %c < %c then\n",
			       indent[depth].c_str(),
			       vars[depth - branch - 1],
			       vars[depth - branch + 1]);
			swap(vars, depth - branch, depth - branch + 1);
			do_sort(vars, depth + 1);
		}
	}
	for (size_t branch = depth; branch > 0; --branch) {
		swap(vars, depth - branch, depth - branch + 1);
	}
}

static void run(vector<char> &vars) {
	printf("program sort(input,output);\n");
	printf("var\n%s : integer;\n", join_vars(vars).c_str());
	printf("begin\n");
	do_sort(vars, 1);
	printf("end.\n");
}

int main() {
	unsigned int m, n;
	for (size_t i = 1; i < 20; ++i) {
		indent[i].reserve(2 * i);
		if (i > 0) {
			indent[i].append(indent[i - 1]);
			indent[i].append("  ");
		}
	}
	scanf("%u", &m);
	while (m--) {
		scanf("%u", &n);
		vector<char> vars(n);
		unsigned int c = 'a' + (n - 1);
		while (n) {
			vars[--n] = (char) c--;
		}
		run(vars);
		if (m) {
			printf("\n");
		}
	}
	return 0;
}
