#include <cinttypes>
#include <cstdio>

using namespace std;

static inline size_t cnt1b(uint32_t x) {
	x = x - ((x >> 1) & 0x55555555);
	x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x = (x + (x >> 4)) & 0x0F0F0F0F;
	x = x + (x >> 8);
	x = x + (x >> 16);
	return (size_t) (x & 0x3F);
}

int main() {
	uint32_t x;
	while (scanf("%" SCNu32, &x) == 1 && x != 0) {
		size_t number_of_1_bits = cnt1b(x);
		uint32_t mask = 31;
		printf("The parity of ");
		while (((x >> mask) & 1) == 0 && mask) {
			--mask;
		}
		while (mask) {
			printf("%" PRIu32, (x >> mask) & 1);
			--mask;
		}
		printf("%" PRIu32 " is %zu (mod 2).\n", x & 1, number_of_1_bits);
	}
	return 0;
}
