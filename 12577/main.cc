#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

static inline string &trim(string &s) {
	// ltrim
	s.erase(s.begin(),
	        find_if(s.begin(), s.end(), [](int x) { return !std::isspace(x); }));
	// ritrim
	s.erase(
	    find_if(s.rbegin(), s.rend(), [](int x) { return !std::isspace(x); }).base(),
	    s.end());
	return s;
}

int main() {
	int the_case = 1;
	for (string line; getline(cin, line);) {
		trim(line);
		if (line == "*") {
			break;
		} else if (line == "Hajj") {
			printf("Case %d: Hajj-e-Akbar\n", the_case++);
		} else if (line == "Umrah") {
			printf("Case %d: Hajj-e-Asghar\n", the_case++);
		}
	}
	return 0;
}
