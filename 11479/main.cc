#include <algorithm>
#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		long long a, b, c;
		scanf("%lld %lld %lld\n", &a, &b, &c);

		if (a > b) {
			swap(a, b);
		}
		if (a > c) {
			swap(a, c);
		}
		if (b > c) {
			swap(b, c);
		}

		printf("Case %d: ", the_case);
		if (a + b <= c) {
			puts("Invalid");
		} else {
			if (a == b && a == c && c == b) {
				puts("Equilateral");
			} else if (a == b || a == c || c == b) {
				puts("Isosceles");
			} else {
				puts("Scalene");
			}
		}
	}
	return 0;
}
