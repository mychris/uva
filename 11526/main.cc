#include <cmath>
#include <cstdio>

using namespace std;

long long H(int n) {
	long long res = 0;
	for (int i = 1; i <= n; i = i + 1) {
		res = (res + n / i);
	}
	return res;
}

// http://oeis.org/A006218
long long H_new(int n) {
	long long res = 0;
	int end = (int) sqrt(n);
	for (int i = 1; i <= end; ++i) {
		res += n / i;
	}
	res *= 2;
	res -= end * end;
	return res;
}

int main() {
	int _C;
	scanf("%d", &_C);
	while (_C--) {
		int n;
		scanf("%d\n", &n);
		printf("%lld\n", H_new(n));
	}
	return 0;
}
