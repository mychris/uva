#include <cctype>
#include <cstdio>

using namespace std;

static char input[32];

// poor mans prime check... that's enough...
static bool is_prime(int n) {
	for (int i = 2; i < n; ++i) {
		if (n % i == 0) {
			return false;
		}
	}
	return true;
}

int main() {
	while (scanf("%s\n", input) == 1) {
		int sum = 0;
		char *c = input;
		while (*c) {
			if (isalpha(*c)) {
				sum += (*c >= 'a') ? (*c - 'a' + 1) : (*c - 'A' + 27);
			}
			++c;
		}
		if (is_prime(sum)) {
			puts("It is a prime word.");
		} else {
			puts("It is not a prime word.");
		}
	}
	return 0;
}
