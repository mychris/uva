#include <cstdio>

using namespace std;

static long long gcd(long long a, long long b) {
	long long t;
	while (b) {
		t = a;
		a = b;
		b = t % b;
	}
	return a < 0 ? -a : a; /* abs(u) */
}

static long long lcm(long long a, long long b) {
	return (a * b) / gcd(a, b);
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		long long a, c;
		scanf("%lld %lld\n", &a, &c);
		if (c % a != 0) {
			puts("NO SOLUTION");
		} else {
			long long b = c / a, result;
			for (result = b;; result += b) {
				if (lcm(a, result) == c) {
					break;
				}
			}
			printf("%lld\n", result);
		}
	}
	return 0;
}
