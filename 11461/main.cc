#include <cstdio>
#include <vector>

using namespace std;

#define MAX 100001

static int square_numbers[MAX];
static int square_number_sums[MAX];

static void init(void) {
	square_numbers[1] = 1;
	for (int i = 2; i * i < MAX; ++i) {
		square_numbers[i * i] = 1;
	}

	square_number_sums[1] = 1;
	for (int i = 2; i < MAX; ++i) {
		square_number_sums[i] = square_number_sums[i - 1];
		if (square_numbers[i]) {
			square_number_sums[i] += 1;
		}
	}
}

int main() {
	init();
	int a, b;
	while (scanf("%d %d\n", &a, &b) == 2 && a > 0 && b > 0) {
		printf("%d\n", square_number_sums[b] - square_number_sums[a - 1]);
	}
	return 0;
}
