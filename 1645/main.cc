#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

// http://oeis.org/A003238

static unsigned long long cache[1024];
static unsigned long long mod = 1000000007;

static void init(void) {
	for (size_t i = 1; i < 1001; ++i) {
		cache[i] = 1;
	}
	for (size_t i = 3; i < 1001; ++i) {
		for (size_t j = 2; j < i; ++j) {
			if (i % j == 1) {
				cache[i] += cache[j];
			}
		}
	}
}

int main() {
	init();
	int the_case = 1, x = 0;
	while (scanf("%d\n", &x) == 1) {
		printf("Case %d: %llu\n", the_case++, cache[x] % mod);
	}
	return 0;
}
