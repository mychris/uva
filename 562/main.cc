#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>

using namespace std;

int num_coins;
int coins[128] = {};
int m[128][32768] = {};

int main() {
	int N;
	scanf("%d", &N);
	while (N--) {
		int sum = 0;
		scanf("%d", &num_coins);
		for (int i = 1; i <= num_coins; ++i) {
			scanf("%d", &coins[i]);
			sum += coins[i];
		}
		const int target = sum / 2;
		for (int j = 0; j <= target; ++j)
			m[0][j] = 0;
		for (int i = 1; i <= num_coins; ++i) {
			const int coin = coins[i];
			for (int j = 0; j <= target; ++j) {
				m[i][j] = m[i - 1][j];
				if (j >= coin)
					m[i][j] = max(m[i][j], m[i - 1][j - coin] + coin);
			}
		}
#if 0
		for (int j = 0; j <= target; ++j) {
			for (int i = 0; i <= num_coins; ++i) {
				printf("%02d ", m[i][j]);
			}
			printf("\n");
		}
#endif
		printf("%d\n", sum - (m[num_coins][target] * 2));
	}
	return 0;
}
