#include <cstdio>

using namespace std;

/* clang-format off */
static char decoding[] = {
    '\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07',
    '\x08', '\x09', '\x0A', '\x0B', '\x0C', '\x0D', '\x0E', '\x0F',
    '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17',
    '\x18', '\x19', '\x1A', '\x1B', '\x1C', '\x1D', '\x1E', '\x1F',
    ' ', '!', 'l', '#', '$', '%', '&', 'l',
    '(', ')', '*', '+', 'n', '-', 'm', ',',
    '8', '1', '`', '1', '2', '3', '4', '5',
    '6', '7', 'k', 'k', 'n', '0', 'm', '<',
    '@', 'A', 'c', 'z', 'a', 'q', 's', 'd',
    'f', 'y', 'g', 'h', 'j', 'b', 'v', 'u',
    'i', 'q', 'w', 's', 'e', 't', 'x', 'w',
    'x', 'r', 'z', 'o', '[', 'p', '^', '_',
    '`', 'a', 'c', 'z', 'a', 'q', 's', 'd',
    'f', 'y', 'g', 'h', 'j', 'b', 'v', 'u',
    'i', 'q', 'w', 's', 'e', 't', 'x', 'w',
    'x', 'r', 'z', 'o', 'p', 'p', '~', '\x7F',
};
/* clang-format on */

int main() {
	int c;
	while ((c = getchar()) != EOF) {
		putchar(decoding[(size_t) c]);
	}
}
