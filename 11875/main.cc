#include <algorithm>
#include <cstdio>

using namespace std;

static int team[128];

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int members;
		scanf("%d", &members);
		for (int i = 0; i < members; ++i) {
			scanf("%d", &team[i]);
		}
		sort(team, team + members);
		printf("Case %d: %d\n", the_case, team[members / 2]);
	}
	return 0;
}
