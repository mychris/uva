#include <cstdio>

using namespace std;

static inline int cnt1b(int x) {
	x = x - ((x >> 1) & 0x55555555);
	x = (x & 0x33333333) + ((x >> 2) & 0x33333333);
	x = (x + (x >> 4)) & 0x0F0F0F0F;
	x = x + (x >> 8);
	x = x + (x >> 16);
	return (x & 0x3F);
}

static int interp_as_hex(int n) {
	int ret = 0;
	int mul = 1;
	while (n) {
		ret += (n % 10) * mul;
		mul *= 16;
		n /= 10;
	}
	return ret;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int n;
		scanf("%d\n", &n);
		printf("%d %d\n", cnt1b(n), cnt1b(interp_as_hex(n)));
	}
	return 0;
}
