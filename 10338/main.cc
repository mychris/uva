#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static inline unsigned long long fact(int x) {
	unsigned long long result = 1;
	while (x) {
		result *= static_cast<unsigned long long>(x);
		x -= 1;
	}
	return result;
}

char input[32];
int counts[128];

int main() {
	int n;
	scanf("%d", &n);
	for (int d = 1; d <= n; ++d) {
		scanf("%24s", input);
		memset(counts, 0, sizeof(counts));
		int length = 0;
		for (int i = 0; input[i]; ++i) {
			counts[static_cast<size_t>(input[i])] += 1;
			length += 1;
		}
		unsigned long long result = fact(length);
		for (int i = static_cast<int>('A'); i <= static_cast<int>('Z'); ++i) {
			if (counts[i] > 1) {
				result /= fact(counts[i]);
			}
		}
		printf("Data set %d: %llu\n", d, result);
	}
	return 0;
}
