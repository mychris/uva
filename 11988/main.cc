#include <cstdio>
#include <cstring>

using namespace std;

#define MAX 200000
static char input[MAX];
static char text[MAX];
static size_t pos = 0;
static size_t len = 0;

static void end() {
	memmove(&text[pos], &text[MAX - (len - pos)], sizeof(char) * (len - pos));
	pos = len;
}

static void home() {
	memmove(&text[MAX - len], &text[0], sizeof(char) * pos);
	pos = 0;
}

static void add(char const c) {
	text[pos++] = c;
	++len;
}

static void reset() {
	memset(text, 0, sizeof(text));
	pos = 0;
	len = 0;
}

static void print() {
	end();
	puts(text);
}

int main() {
	int input_len, i;
	while (scanf("%s", input) == 1) {
		input_len = (int) strlen(input);
		for (i = 0; i < input_len; ++i) {
			char c = input[i];
			switch (c) {
			case '[':
				home();
				break;
			case ']':
				end();
				break;
			default:
				add(c);
				break;
			}
		}
		if (input_len > 0) {
			print();
			reset();
		}
	}
}
