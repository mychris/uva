#include <cstdio>
#include <vector>

using namespace std;

static int sieve[1000001];
static vector<int> seq;
static vector<int> comp;

static void init(void) {
	for (int i = 1; i <= 1000000; i++) {
		for (int j = i; j <= 1000000; j += i) {
			sieve[j]++;
		}
	}

	seq.push_back(1);
	int last = 1;
	for (int i = 0; i <= 1000000; i++) {
		if (last > 1000000) {
			break;
		}
		int cur = last + sieve[last];
		seq.push_back(cur);
		last = cur;
	}

	size_t curKey = 0;
	int counter = 0;
	comp.push_back(0);
	for (int i = 1; i <= 1000000; i++) {
		if (i == seq[curKey]) {
			curKey++;
			counter++;
		}
		comp.push_back(counter);
	}
}

int main() {
	init();
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		unsigned int start, end;
		scanf("%u %u\n", &start, &end);
		int result = comp[end] - comp[start - 1];
		printf("Case %d: %d\n", the_case, result);
	}
	return 0;
}
