#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>

using namespace std;

int main() {
	int times = 0;
	int c;
	while ((c = getchar()) != EOF) {
		if (isdigit(c)) {
			times += c - '0';
			continue;
		}
		if (c == '!' || c == '\n') {
			putchar('\n');
			continue;
		}
		if (c == 'b') {
			c = ' ';
		}
		while (times--) {
			putchar(c);
		}
		times = 0;
	}
}
