#include <cstdio>

using namespace std;

typedef enum { Mile, Juice, BOTH } package;

int calls[100];

int calc(int n, package *r_package) {
	int mile = 0, juice = 0;
	int i;
	for (i = 0; i < n; ++i) {
		mile += (calls[i] / 30 + 1) * 10;
		juice += (calls[i] / 60 + 1) * 15;
	}
	if (mile > juice) {
		*r_package = Juice;
		return juice;
	} else if (mile < juice) {
		*r_package = Mile;
		return mile;
	}
	*r_package = BOTH;
	return mile;
}

int main() {
	int _C, i, the_case;
	scanf("%d\n", &_C);
	for (the_case = 1; the_case <= _C; ++the_case) {
		int n;
		scanf("%d\n", &n);
		for (i = 0; i < n; ++i) {
			scanf("%d", &calls[i]);
		}
		package p;
		int result = calc(n, &p);
		if (p == Mile) {
			printf("Case %d: Mile %d\n", the_case, result);
		} else if (p == Juice) {
			printf("Case %d: Juice %d\n", the_case, result);
		} else {
			printf("Case %d: Mile Juice %d\n", the_case, result);
		}
	}
	return 0;
}
