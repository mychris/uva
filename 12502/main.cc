#include <cstdio>

using namespace std;

class Fraction {
  private:
	long long numerator, denominator;

  public:
	Fraction(long long n, long long d) : numerator(n), denominator(d) {
		if (n == 0LL) {
			d = 1LL;
		}
		int sign = 1;
		if (n < 0) {
			sign *= -1;
			n *= -1;
		}
		if (d < 0) {
			sign *= -1;
			d *= -1;
		}

		numerator = n * sign;
		denominator = d;
	}

	operator int() {
		return (int) (numerator / denominator);
	}
	operator double() {
		return ((double) numerator) / (double) denominator;
	}

	friend Fraction operator+(Fraction &lhs, const Fraction &rhs) {
		return Fraction(lhs.numerator * rhs.denominator +
		                    rhs.numerator * lhs.denominator,
		                lhs.denominator * rhs.denominator);
	}

	friend Fraction operator-(const Fraction &lhs, const Fraction &rhs) {
		return Fraction(lhs.numerator * rhs.denominator -
		                    rhs.numerator * lhs.denominator,
		                lhs.denominator * rhs.denominator);
	}

	friend Fraction operator*(const Fraction &lhs, const Fraction &rhs) {
		return Fraction(lhs.numerator * rhs.numerator,
		                lhs.denominator * rhs.denominator);
	}

	friend Fraction operator/(const Fraction &lhs, const Fraction &rhs) {
		return Fraction(lhs.numerator * rhs.denominator,
		                lhs.denominator * rhs.numerator);
	}
};

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int x, y, z;
		scanf("%d %d %d", &x, &y, &z);
		Fraction a_additional = Fraction(x, 1) - Fraction(x + y, 3);
		Fraction b_additional = Fraction(y, 1) - Fraction(x + y, 3);
		if ((double) a_additional <= 0.0) {
			puts("0");
		} else {
			Fraction result =
			    (Fraction(z, 1) / (a_additional + b_additional)) * a_additional;
			printf("%d\n", (int) result);
		}
	}
	return 0;
}
