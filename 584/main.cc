#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

int main() {
	for (;;) {
		int score = 0, frame_score = 0, roll_score = 0;
		int frame = 1;
		int roll = 0;
		char input[16];
		char before[2] = {' ', ' '};
		if (scanf("%s", input) != 1) {
			return 5;
		}
		if (strcmp("Game", input) == 0) {
			return 0;
		}
		do {
			assert(input[0] != '\0');
			assert(input[1] == '\0');
			++roll;
			switch (input[0]) {
			case 'X':
				roll_score = 10;
				break;
			case '/':
				if (frame == 10 && roll == 3) {
					roll_score = 20 - frame_score;
				} else {
					roll_score = 10 - frame_score;
				}
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				roll_score = input[0] - '0';
				break;
			}
			if (before[1] == 'X') {
				score += roll_score;
			}
			if (before[0] == '/' || before[0] == 'X') {
				score += roll_score;
			}
			frame_score += roll_score;
			score += roll_score;
			if (frame < 10 && (roll == 2 || input[0] == 'X')) {
				++frame;
				roll = 0;
				frame_score = 0;
				before[1] = before[0];
				before[0] = input[0];
			} else {
				if (roll == 2) {
					if (frame_score < 10) {
						break;
					}
				} else if (roll == 3) {
					break;
				}
				before[1] = before[0];
				before[0] = ' ';
			}
			if (scanf("%s", input) != 1) {
				return 5;
			}
		} while (1);
		printf("%d\n", score);
	}
	return 0;
}
