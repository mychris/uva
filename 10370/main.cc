#include <cstdio>

using namespace std;

static int grades[2000];

int main() {
	int _C, i;
	scanf("%d\n", &_C);
	while (_C--) {
		int N, sum = 0, cnt_above = 0;
		double avr;
		scanf("%d", &N);
		for (i = 0; i < N; ++i) {
			scanf("%d", &grades[i]);
			sum += grades[i];
		}
		avr = ((double) sum) / ((double) N);
		for (i = 0; i < N; ++i) {
			if (grades[i] > avr) {
				++cnt_above;
			}
		}
		printf("%.3lf%%\n", 100.0 / N * cnt_above);
	}
	return 0;
}
