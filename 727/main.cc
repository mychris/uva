#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>
#include <stack>

using namespace std;

static int precedence[128];

void skip(void) {
	int c;
	while ((c = getchar()) > 0 && isspace(c)) {
	}
	if (c > 0) {
		ungetc(c, stdin);
	}
}

int main() {
	precedence['+'] = precedence['-'] = 2;
	precedence['*'] = precedence['/'] = 3;
	precedence['('] = precedence[')'] = 1;

	int _N;
	if (scanf("%d\n", &_N) != 1) {
		return 2;
	}
	skip();
	while (_N--) {
		stack<char> ops;
		for (;;) {
			int c = getchar();
			if (c <= 0 || c == '\n') {
				break;
			}
			if (!precedence[c]) {
				putchar(c);
#if !defined(NDEBUG)
				fflush(stdout);
#endif
			} else {
				char op = (char) c;
				if (op == '(' || ops.empty()) {
					ops.push((char) c);
				} else if (op == ')') {
					while (ops.top() != '(') {
						putchar(ops.top());
						ops.pop();
					}
					ops.pop();
				} else {
					if (precedence[(size_t) ops.top()] < precedence[(size_t) op]) {
						ops.push(op);
					} else {
						while (!ops.empty() && precedence[(size_t) ops.top()] >=
						                           precedence[(size_t) op]) {
							putchar(ops.top());
							ops.pop();
						}
						ops.push(op);
					}
				}
			}
			c = getchar();
			assert(c == '\n'); // drop NL
		}
		while (!ops.empty()) {
			putchar(ops.top());
			ops.pop();
		}
		if (_N) {
			puts("\n");
		} else {
			puts("");
		}
	}
	return 0;
}
