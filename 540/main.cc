#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

#include <map>
#include <queue>

using namespace std;

map<int, int> team_mapping;
queue<int> main_queue;
queue<int> team_queues[1000];
int teams;

char command[128];

int main() {
	int test_case = 0;
	while (scanf("%d", &teams) == 1 && teams > 0) {
		printf("Scenario #%d\n", ++test_case);
		team_mapping.clear();
		for (int i = 0; i < teams; ++i) {
			int team_members;
			scanf("%d", &team_members);
			while (team_members--) {
				int x;
				scanf("%d", &x);
				team_mapping[x] = i;
			}
		}
		int p, team;
		queue<int> *queue;
		for (;;) {
			scanf("%64s", command);
			if (command[0] == 'E' || command[0] == 'e') {
				scanf("%d", &p);
				team = team_mapping[p];
				queue = &team_queues[team];
				if (queue->empty())
					main_queue.push(team);
				queue->push(p);
			} else if (command[0] == 'D' || command[0] == 'd') {
				team = main_queue.front();
				queue = &team_queues[team];
				printf("%d\n", queue->front());
				queue->pop();
				if (queue->empty())
					main_queue.pop();
			} else {
				break;
			}
		}
		puts("");
		for (team = 0; team < teams; ++team) {
			queue = &team_queues[team];
			while (!queue->empty())
				queue->pop();
		}
		while (!main_queue.empty())
			main_queue.pop();
	}
	return 0;
}
