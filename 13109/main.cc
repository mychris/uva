#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int m, w, e;
		vector<int> elephants;
		scanf("%d %d\n", &m, &w);
		elephants.reserve((size_t) m);
		while (m--) {
			scanf("%d", &e);
			elephants.push_back(e);
		}
		sort(elephants.begin(), elephants.end());
		int cnt = 0;
		for (auto it = elephants.begin(); it != elephants.end() && w - *it > 0; it++) {
			w -= *it;
			++cnt;
		}
		printf("%d\n", cnt);
	}
	return 0;
}
