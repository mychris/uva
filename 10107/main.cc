#include <cstdio>
#include <cstring>

using namespace std;

int buffer[20000];
size_t used = 0;

static ssize_t bin_search(int x) {
	ssize_t low = 0;
	ssize_t high = (ssize_t) used - 1;

	while (low <= high) {
		ssize_t mid = (low + high) >> 1;
		int midVal = buffer[mid];

		if (midVal < x) {
			low = mid + 1;
		} else if (midVal > x) {
			high = mid - 1;
		} else {
			return mid;
		}
	}
	return -(low + 1);
}

void insert(int x) {
	if (used == 0) {
		buffer[used++] = x;
		return;
	}
	ssize_t pos = bin_search(x);
	size_t i_pos = (size_t) pos;
	if (pos < 0) {
		i_pos = (size_t) (-pos - 1);
	}
	memmove(&buffer[i_pos + 1], &buffer[i_pos], sizeof(int) * (used - i_pos));
	buffer[i_pos] = x;
	++used;
}

int main() {
	int x;
	while (scanf("%d", &x) != EOF) {
		insert(x);
		if (used & 1) {
			printf("%d\n", buffer[used / 2]);
		} else {
			printf("%d\n", (buffer[used / 2 - 1] + buffer[used / 2]) / 2);
		}
	}
	return 0;
}
