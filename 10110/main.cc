#include <bitset>
#include <cmath>
#include <cstdio>

using namespace std;

static inline bool is_perfect_square(long long n) {
	long long s = (long long) sqrt(n);
	return s * s == n;
}

int main() {
	long long n;
	while (scanf("%lld\n", &n) == 1 && n > 0) {
		if (is_perfect_square(n)) {
			puts("yes");
		} else {
			puts("no");
		}
	}
}
