#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

// 0 is not a valid index!
static unsigned ladies[50002];
static ssize_t N;
static ssize_t Q;

ssize_t search(unsigned x) {
	ssize_t l = 1, r = N + 1;
	while (l < r) {
		ssize_t m = (r + l) / 2;
		if (ladies[m] < x) {
			l = m + 1;
		} else {
			r = m;
		}
	}
	return l;
}

int main() {
	while (scanf("%zd", &N) == 1) {
		// remove duplicates!
		for (ssize_t i = 1; i <= N;) {
			scanf("%u", &ladies[i]);
			if (ladies[i - 1] != ladies[i]) {
				++i;
			} else {
				--N;
			}
		}
		scanf("%zd", &Q);
		while (Q--) {
			unsigned x;
			scanf("%u", &x);
			ssize_t idx = search(x);
			ssize_t left = idx, right = idx;
			if (ladies[idx] == x) {
				left -= 1;
				right += 1;
			} else {
				left -= 1;
			}
			if (left == 0) {
				printf("X");
			} else {
				printf("%u", ladies[left]);
			}
			if (right == (ssize_t) N + 1) {
				printf(" X\n");
			} else {
				printf(" %u\n", ladies[right]);
			}
		}
	}
	return 0;
}
