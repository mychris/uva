#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int N;
	scanf("%d", &N);
	while (N--) {
		int start, find, refund;
		scanf("%d %d %d", &start, &find, &refund);
		int e = start + find;
		int d = 0;
		while (e >= refund) {
			d += e / refund;
			e = e / refund + e % refund;
		}
		printf("%d\n", d);
	}
	return 0;
}
