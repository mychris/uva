#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <string>
#include <vector>

#define LIMIT 5000

using namespace std;

static void add(string const &a, string const &b, string &result) {
	result.clear();
	int carry = 0;
	size_t end = min(a.size(), b.size());
	for (size_t i = 0; i < end; ++i) {
		int x = a[i] + b[i] - '0' + carry;
		carry = 0;
		if (x > '9') {
			carry = 1;
			x -= 10;
		}
		assert((char) x >= '0');
		assert((char) x <= '9');
		result += (char) x;
	}
	for (size_t i = end; i < a.size(); ++i) {
		int x = a[i] + carry;
		carry = 0;
		if (x > '9') {
			carry = 1;
			x -= 10;
		}
		assert((char) x >= '0');
		assert((char) x <= '9');
		result += (char) x;
	}
	for (size_t i = end; i < b.size(); ++i) {
		int x = b[i] + carry;
		carry = 0;
		if (x > '9') {
			carry = 1;
			x -= 10;
		}
		assert((char) x >= '0');
		assert((char) x <= '9');
		result += (char) x;
	}
	if (carry) {
		result += '1';
	}
}

static string reverse(string const &a) {
	string result(a);
	reverse(result.begin(), result.end());
	return result;
}

static vector<string> fibs;
static string a, b;

static void next(size_t req) {
	for (size_t i = fibs.size(); i <= req; ++i) {
		fibs.push_back(reverse(a));
		string tmp;
		tmp.reserve(max(a.size(), b.size()) + 1);
		add(a, b, tmp);
		a = move(b);
		b = move(tmp);
	}
}

int main() {
	fibs.reserve(LIMIT + 1);
	fibs.push_back("0");
	fibs.push_back("1");
	a += '1';
	b += '2';
	int n;
	while (scanf("%d\n", &n) == 1) {
		next((size_t) n);
		printf("The Fibonacci number for %d is %s\n", n, fibs[(size_t) n].c_str());
	}
	return 0;
}
