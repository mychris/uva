#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int n;
		scanf("%d\n", &n);
		printf("Case #%d: %d\n", the_case, (n - 1) * 2 + 1);
	}
	return 0;
}
