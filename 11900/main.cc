#include <algorithm>
#include <cstdio>

using namespace std;

static int weights[32];

int main() {
	int _C;
	if (scanf("%d\n", &_C) != 1) {
		return 0;
	}
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int n, P, Q;
		scanf("%d %d %d\n", &n, &P, &Q);
		for (int i = 0; i < n; i++) {
			scanf("%d", &weights[i]);
		}
		sort(weights, weights + n);

		int ctr = 0;
		int weight_sum = 0;
		for (int key = 0; key < n && weight_sum + weights[key] <= Q && ctr < P; ++key) {
			weight_sum += weights[key];
			++ctr;
		}
		printf("Case %d: %d\n", the_case, ctr);
	}
	return 0;
}
