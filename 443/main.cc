#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cinttypes>
#include <cstdio>
#include <vector>

using namespace std;

static vector<uint64_t> numbers;

static void init(uint64_t const x) {
	if (x > 2000000000 || find(numbers.begin(), numbers.end(), x) != numbers.end()) {
		return;
	}
	numbers.push_back(x);
	init(x * 2);
	init(x * 3);
	init(x * 5);
	init(x * 7);
}

static char const *s(size_t const n) {
	switch (n % 10) {
	case 1:
		return (n % 100 != 11) ? "st" : "th";
	case 2:
		return (n % 100 != 12) ? "nd" : "th";
	case 3:
		return (n % 100 != 13) ? "rd" : "th";
	default:
		return "th";
	}
}

int main() {
	numbers.reserve(5843);
	init(1);
	sort(numbers.begin(), numbers.end());

	size_t n;
	while (scanf("%zu\n", &n) == 1 && n > 0) {
		assert(n <= 5842);
		printf("The %zu%s humble number is %" PRIu64 ".\n", n, s(n), numbers[n - 1]);
	}
	return 0;
}
