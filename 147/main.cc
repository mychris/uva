#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int coins[] = {10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5};

static unsigned long long cache[30001][30] = {};

unsigned long long recursive_calc(const int input, const size_t coin_position) {
	if (cache[input][coin_position] > 0) {
		return cache[input][coin_position];
	}
	unsigned long long count = 0;
	{
		int current_input = input;
		int current_coin = coins[coin_position];
		while (current_input >= 0) {
			count += recursive_calc(current_input, coin_position + 1);
			current_input -= current_coin;
		}
	}
	cache[input][coin_position] = count;
	return count;
}

static inline unsigned long long calc(const int input) {
	return recursive_calc(input, 0);
}

int main() {
	// pre-calculate the simple cases. Those are easy to reason about in an iterative
	// form. Other cases should be easier to reason about using dynamic programming.
	// Just to speed up the runtime performance.
	{
		size_t pos_5_cents = 0;
		size_t pos_10_cents = 0;
		for (size_t i = 0; i < sizeof(coins) / sizeof(coins[0]); ++i) {
			// if we have nothing left, any coin position will make exactly one change:
			// nothing
			cache[0][i] = 1;
			if (coins[i] == 5) {
				pos_5_cents = i;
			} else if (coins[i] == 10) {
				pos_10_cents = i;
			}
		}
		for (int amount = 5; amount <= 30000; amount += 5) {
			// if we have any amount X and we are at the last coin position (5 cents),
			// we can only take away the amount in all 5c.
			cache[amount][pos_5_cents] = 1;
			// if we have any amount X and we are at the second to last coin position
			// (10 and 5 cents left), we can only take those two coins. Examples:
			//     5c: 1*5c
			//    10c: 1*10c + 0*5c, 2*5c
			//    15c: 1*10c + 1*5c, 3*5c
			//    20c: 2*10c + 0*5c, 1*10c + 2*5c, 4*5c
			//    25c: 2*10c + 1*5c, 1*10c + 3*5c, 5*5c
			//    30c: 3*10c + 0*5c, 2*10c + 2*5c, 1*10c + 4*5c, 6*5c
			//    35c: 3*10c + 1*5c, 2*10c + 3*5c, 1*10c + 5*5c, 7*5c
			// Every 10 cents, another choice can be made, because we can either take
			// the additional 10c as 2*5c or 1*10c.
			cache[amount][pos_10_cents] = cache[amount - 5][pos_10_cents];
			if (amount % 10 == 0) {
				cache[amount][pos_10_cents] += 1;
			}
		}
	}
	int a, b;
	while (scanf("%d.%d", &a, &b) == 2 && (a != 0 || b != 0)) {
		int input_cents = a * 100 + b;
		if (input_cents > 30000 || input_cents % 5 != 0) {
			return 1;
		}
		unsigned long long result = calc(input_cents);
		printf("%3d.%02d%17llu\n", a, b, result);
	}
	return 0;
}
