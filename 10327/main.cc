#include <cstdio>

using namespace std;

static int len;
static int input[1024];

static int sort(void) {
	int cnt = 0;
	for (int pos_to_flip = 0; pos_to_flip < len; ++pos_to_flip) {
		int min = input[pos_to_flip];
		int min_pos = pos_to_flip;
		for (int j = pos_to_flip + 1; j < len; ++j) {
			if (input[j] < min) {
				min = input[j];
				min_pos = j;
			}
		}
		while (min_pos != pos_to_flip) {
			++cnt;
			int tmp = input[min_pos];
			input[min_pos] = input[min_pos - 1];
			input[min_pos - 1] = tmp;
			--min_pos;
		}
	}
	return cnt;
}

int main() {
	while (scanf("%d\n", &len) == 1) {
		for (int i = 0; i < len; ++i) {
			scanf("%d", &input[i]);
		}
		printf("Minimum exchange operations : %d\n", sort());
	}
	return 0;
}
