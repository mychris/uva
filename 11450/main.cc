#include <algorithm>
#include <cstdio>
#include <set>
#include <vector>

using namespace std;

int main() {
	int _N;
	scanf("%d\n", &_N);
	while (_N--) {
		int money;
		unsigned int n_garments;
		scanf("%d %u\n", &money, &n_garments);
		vector<vector<int>> garments(n_garments);
		while (n_garments--) {
			unsigned int n_models;
			scanf("%u", &n_models);
			while (n_models--) {
				int price;
				scanf("%d\n", &price);
				garments[n_garments].push_back(price);
			}
		}
		// always combine two and remove everything over $money
		set<int> result;
		result.insert(0);
		while (!garments.empty()) {
			vector<int> to_add = garments.back();
			garments.pop_back();
			set<int> next;
			for (int x : to_add) {
				for (int y : result) {
					int z = x + y;
					if (z <= money) {
						next.insert(z);
					}
				}
			}
			result = move(next);
		}
		if (result.empty()) {
			puts("no solution");
		} else {
			printf("%d\n", *max_element(result.begin(), result.end()));
		}
	}
	return 0;
}
