#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

#define ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

// 0 = is prime
// 1 == not prime and not yet tested
// 2 == not prime, tested and is carmichael number
// 3 == not prime, tested and is normal
static int numbers[65536] = {};

static void sieve() {
	for (int i = 2; i < (int) ARRAY_LEN(numbers) / 2; ++i) {
		if (numbers[i] == 0) {
			for (int j = i + i; j < (int) ARRAY_LEN(numbers); j += i) {
				numbers[j] = 1;
			}
		}
	}
}

unsigned int powmod(unsigned int base, unsigned int exp, unsigned int mod) {
	if (mod == 1) {
		return 0;
	}
	unsigned int result = 1;
	base = base % mod;
	while (exp > 0) {
		if ((exp & 1) == 1) {
			result = (result * base) % mod;
		}
		exp >>= 1;
		base = (base * base) % mod;
	}
	return result;
}

void pre_calc() {
	sieve();
	printf("        switch (n) {\n");
	for (unsigned int n = 4; n < (unsigned) ARRAY_LEN(numbers); ++n) {
		if (numbers[n] != 1) {
			continue;
		}
		numbers[n] = 2;
		for (unsigned int a = 2; a < n; ++a) {
			if (powmod(a, n, n) != a) {
				numbers[n] = 3;
				break;
			}
		}
		if (numbers[n] == 2) {
			printf("        case %u:\n", n);
		}
	}
	printf("            printf(\"The number %%u is a Carmichael number.\\n\", n);\n");
	printf("            break;\n");
	printf("        default:\n");
	printf("            printf(\"%%u is normal.\\n\", n);\n");
	printf("            break;\n");
	printf("        }\n");
}

int main() {
	//	pre_calc(); return 0;
	unsigned int n;
	while (scanf("%u\n", &n) == 1 && n != 0) {
		switch (n) {
		case 561:
		case 1105:
		case 1729:
		case 2465:
		case 2821:
		case 6601:
		case 8911:
		case 10585:
		case 15841:
		case 29341:
		case 41041:
		case 46657:
		case 52633:
		case 62745:
		case 63973:
			printf("The number %u is a Carmichael number.\n", n);
			break;
		default:
			printf("%u is normal.\n", n);
			break;
		}
	}
	return 0;
}
