#include <cstddef>
#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cstdio>
#include <cstring>

using namespace std;

int rows, cols;

/*
   The path is not tracked from right to left, but from left to right.
   This way it is easier to ensure the "Lexicographically" requirement for
   the best path. Since all the weights are added together, it doesn't matter
   if the path is searched from left to right or right to left.
*/
int matrix[10][100];
int best[10][100];

static inline int min3(int a, int b, int c) {
	int result = (a < b) ? a : b;
	if (c < result)
		return c;
	return result;
}

static inline int max3(int a, int b, int c) {
	int result = (a > b) ? a : b;
	if (c > result)
		return c;
	return result;
}

static inline void sort3(int &a, int &b, int &c) {
	int low = min3(a, b, c);
	int high = max3(a, b, c);
	int mid = a + b + c - low - high;
	a = low;
	b = mid;
	c = high;
}

int main() {
	while (scanf("%d %d", &rows, &cols) == 2) {
		/* scan and prepare */
		for (int row = 0; row < rows; ++row) {
			for (int col = 0; col < cols; ++col) {
				scanf("%d", &matrix[row][col]);
				best[row][col] = INT_MAX;
			}
			best[row][cols - 1] = matrix[row][cols - 1];
		}
		/* calculate the best paths */
		for (int col = cols - 2; col >= 0; --col) {
			for (int row = 0; row < rows; ++row) {
				best[row][col] =
				    matrix[row][col] + min3(best[row][col + 1],
				                            best[(row + 1) % rows][col + 1],
				                            best[(row - 1 + rows) % rows][col + 1]);
			}
		}
		/* find the best solution with the lowest starting row */
		int min_weight = best[0][0];
		int min_weight_row_start = 0;
		for (int row = 1; row < rows; ++row) {
			if (best[row][0] < min_weight) {
				min_weight = best[row][0];
				min_weight_row_start = row;
			}
		}
		/* reconstruct the path */
		int row = min_weight_row_start;
		int weight = min_weight;
		printf("%d", row + 1);
		for (int col = 0; col < cols - 1; ++col) {
			int min_row, mid_row, max_row;
			min_row = (row - 1 + rows) % rows;
			mid_row = row;
			max_row = (row + 1) % rows;
			sort3(min_row, mid_row, max_row);
			int next_weight = weight - matrix[row][col];
			if (best[min_row][col + 1] == next_weight) {
				row = min_row;
			} else if (best[mid_row][col + 1] == next_weight) {
				row = mid_row;
			} else if (best[max_row][col + 1] == next_weight) {
				row = max_row;
			} else {
				assert(0);
			}
			weight = next_weight;
			printf(" %d", row + 1);
		}
		printf("\n%d\n", min_weight);
	}
	return 0;
}
