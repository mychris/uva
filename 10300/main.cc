#include <cmath>
#include <cstdio>

using namespace std;

int main() {
	int _C, i;
	scanf("%d\n", &_C);
	while (_C--) {
		int farmers, result = 0;
		int size, animals, friendliness;
		scanf("%d\n", &farmers);
		for (i = 0; i < farmers; ++i) {
			scanf("%d %d %d\n", &size, &animals, &friendliness);
			result += size * friendliness;
		}
		printf("%d\n", result);
	}
}
