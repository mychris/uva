#include <climits>
#include <cstdio>
#include <cstdlib>

using namespace std;

static int N = 0;
static int numbers[1001];

static int cmp_int(void const *a, void const *b) {
	return (*(int const *) a) - (*(int const *) b);
}

static int search(int const a, int const start) {
	int left = start;
	int right = N - 1;
	int mid = (left + right) >> 1;
	while (left <= right) {
		mid = (left + right) >> 1;
		if (a == numbers[mid]) {
			return mid;
		}
		if (a > numbers[mid]) {
			left = mid + 1;
		} else {
			right = mid - 1;
		}
	}
	return mid;
}

static int closest(int const q, int const a, int const b) {
	return (abs(a - q) <= abs(b - q)) ? a : b;
}

int main() {
	int cas = 1, i, j;
	while (scanf("%d\n", &N) != EOF && N > 0) {
		for (i = 0; i < N; ++i) {
			scanf("%d\n", &numbers[i]);
		}
		qsort(&numbers[0], (size_t) N, sizeof(int), &cmp_int);
		int M;
		scanf("%d\n", &M);
		printf("Case %d:\n", cas++);

		for (i = 0; i < M; ++i) {
			int query;
			scanf("%d\n", &query);

			int ans = numbers[0] + numbers[1];
			for (j = 0; numbers[j] <= (query / 2) && j + 1 < N; ++j) {
				int k = search(query - numbers[j], j + 1);
				if (numbers[j] + numbers[k] == query) {
					ans = query;
					break;
				}
				if (k - 1 != j) {
					ans = closest(query, ans, numbers[j] + numbers[k - 1]);
				}
				ans = closest(query, ans, numbers[j] + numbers[k]);
				if (k + 1 < N) {
					ans = closest(query, ans, numbers[j] + numbers[k + 1]);
				}
			}
			printf("Closest sum to %d is %d.\n", query, ans);
		}
	}
	return 0;
}
