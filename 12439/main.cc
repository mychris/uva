#include <cstdio>
#include <cstdlib>

using namespace std;

static char month_name[128];
static int from_day;
static int from_month;
static int from_year;
static int to_day;
static int to_month;
static int to_year;

static int name_to_int(void) {
	if (month_name[0] == 'J') {
		if (month_name[1] == 'a') {
			return 1;
		}
		if (month_name[1] == 'u' && month_name[2] == 'n') {
			return 6;
		}
		if (month_name[1] == 'u' && month_name[2] == 'l') {
			return 7;
		}
	}
	if (month_name[0] == 'F') {
		return 2;
	}
	if (month_name[0] == 'M') {
		if (month_name[1] == 'a' && month_name[2] == 'r') {
			return 3;
		}
		if (month_name[1] == 'a' && month_name[2] == 'y') {
			return 5;
		}
	}
	if (month_name[0] == 'A') {
		if (month_name[1] == 'p') {
			return 4;
		}
		if (month_name[1] == 'u') {
			return 8;
		}
	}
	if (month_name[0] == 'S') {
		return 9;
	}
	if (month_name[0] == 'O') {
		return 10;
	}
	if (month_name[0] == 'N') {
		return 11;
	}
	if (month_name[0] == 'D') {
		return 12;
	}
	fprintf(stderr, "Invalid month name %s\n", month_name);
	exit(128);
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		scanf("%s %d, %d", month_name, &from_day, &from_year);
		from_month = name_to_int();
		scanf("%s %d, %d", month_name, &to_day, &to_year);
		to_month = name_to_int();

		if (from_month > 2) {
			from_year++;
		}
		if (to_month < 2 || (to_month == 2 && to_day < 29)) {
			to_year--;
		}

		int result = to_year / 4 - (from_year - 1) / 4;
		result -= to_year / 100 - (from_year - 1) / 100;
		result += to_year / 400 - (from_year - 1) / 400;

		printf("Case %d: %d\n", the_case, result);
	}
	return 0;
}
