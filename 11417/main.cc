#include <cstdio>

using namespace std;

static int gcd_map[501][501];

// https://en.wikipedia.org/wiki/Binary_GCD_algorithm#Iterative_version_in_C
static int gcd(int u, int v) {
	int shift;

	/* GCD(0,v) == v; GCD(u,0) == u, GCD(0,0) == 0 */
	if (u == 0)
		return v;
	if (v == 0)
		return u;

	/* Let shift := lg K, where K is the greatest power of 2
	   dividing both u and v. */
	for (shift = 0; ((u | v) & 1) == 0; ++shift) {
		u >>= 1;
		v >>= 1;
	}

	while ((u & 1) == 0)
		u >>= 1;

	/* From here on, u is always odd. */
	do {
		/* remove all factors of 2 in v -- they are not common */
		/*   note: v is not zero, so while will terminate */
		while ((v & 1) == 0) /* Loop X */
			v >>= 1;

		/* Now u and v are both odd. Swap if necessary so u <= v,
		   then set v = v - u (which is even). For bignums, the
		   swapping is just pointer movement, and the subtraction
		   can be done in-place. */
		if (u > v) {
			int t = v;
			v = u;
			u = t;
		}          // Swap u and v.
		v = v - u; // Here v >= u.
	} while (v != 0);

	/* restore common factors of 2 */
	return u << shift;
}

static void init(void) {
	for (int i = 1; i < 501; ++i) {
		for (int j = 1; j < 501; ++j) {
			gcd_map[i][j] = gcd(i, j);
		}
	}
}

int main() {
	init();
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		int sum = 0;
		for (int i = 1; i < n; ++i) {
			for (int j = i + 1; j <= n; ++j) {
				sum += gcd_map[i][j];
			}
		}
		printf("%d\n", sum);
	}
	return 0;
}
