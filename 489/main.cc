#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int char_in_word[256];
static int char_guessed[256];
static int cnt_chars;
static int cnt_wrong;

int main() {
	int round;
	int c;
	while (scanf("%d\n", &round) == 1 && round >= 0) {
		memset(char_in_word, 0, sizeof(char_in_word));
		memset(char_guessed, 0, sizeof(char_guessed));
		cnt_chars = cnt_wrong = 0;
		printf("Round %d\n", round);
		while ((c = getchar()) != EOF && c != '\n') {
			if (!char_in_word[(size_t) c]) {
				++cnt_chars;
				char_in_word[(size_t) c] = 1;
			}
		}
		while ((c = getchar()) != EOF && c != '\n') {
			if (char_guessed[(size_t) c] || cnt_chars == 0 || cnt_wrong >= 7) {
				continue;
			}
			if (!char_in_word[(size_t) c]) {
				++cnt_wrong;
			} else {
				--cnt_chars;
			}
			char_guessed[(size_t) c] = 1;
		}

		if (cnt_wrong >= 7) {
			puts("You lose.");
		} else if (cnt_chars == 0) {
			puts("You win.");
		} else {
			puts("You chickened out.");
		}
	}
	return 0;
}
