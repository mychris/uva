#include <algorithm>
#include <cstdio>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;

#define MAX_BOXES (30)
#define MAX_DIMENSION (10)

static size_t n_boxes = 0;
static size_t n_sides = 0;

static int boxes[MAX_BOXES][MAX_DIMENSION];
static bool stackable[MAX_BOXES][MAX_BOXES];

static size_t longest_path;
static size_t path[MAX_BOXES];

static bool can_stack(const size_t box_a, const size_t box_b) {
	for (size_t i = 0; i < n_sides; ++i) {
		if (boxes[box_a][i] >= boxes[box_b][i]) {
			return false;
		}
	}
	return true;
}

static void search_path_recur(size_t position, size_t depth, size_t *cur_path) {
	if (depth > longest_path) {
		longest_path = depth;
		for (size_t i = 0; i < depth; ++i) {
			path[i] = cur_path[i];
		}
	}
	for (size_t i = 0; i < n_boxes; ++i) {
		if (stackable[position][i]) {
			cur_path[depth] = i;
			search_path_recur(i, depth + 1, cur_path);
		}
	}
}

static void run() {
	for (size_t i = 0; i < n_boxes; ++i) {
		for (size_t j = 0; j < n_boxes; ++j) {
			stackable[i][j] = can_stack(i, j);
		}
	}
	longest_path = 0;

	for (size_t i = 0; i < n_boxes; ++i) {
		size_t cur_path[MAX_BOXES];
		cur_path[0] = i;
		search_path_recur(i, 1, &cur_path[0]);
	}

	printf("%zu\n", longest_path);
	if (longest_path > 0) {
		for (size_t i = 0; i < longest_path - 1; ++i) {
			printf("%zu ", path[i] + 1);
		}
		printf("%zu\n", path[longest_path - 1] + 1);
	}
}

int main() {
	while (scanf("%zu %zu\n", &n_boxes, &n_sides) == 2) {
		for (size_t i = 0; i < n_boxes; ++i) {
			for (size_t j = 0; j < n_sides; ++j) {
				scanf("%d", &boxes[i][j]);
			}
			sort(boxes[i], boxes[i] + n_sides);
		}
		run();
	}
	return 0;
}
