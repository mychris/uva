#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

#include <algorithm>
#include <limits>

using namespace std;

static ssize_t length;
// 1 indexed
// idx 0 = 0
// idx n_cuts + 1 = length
static ssize_t cuts[64];
static ssize_t n_cuts;
static ssize_t cache[64][64];

ssize_t cut_costs(ssize_t left, ssize_t right) {
	if (left + 1 == right) {
		return 0;
	}
	if (cache[left][right] != -1) {
		return cache[left][right];
	}
	ssize_t best_cut = numeric_limits<ssize_t>::max();
	for (ssize_t i = left + 1; i < right; ++i) {
		ssize_t current_cut =
		    cuts[right] - cuts[left] + cut_costs(left, i) + cut_costs(i, right);
		best_cut = min(best_cut, current_cut);
	}
	cache[left][right] = best_cut;
	return best_cut;
}

int main() {
	while (scanf("%zd", &length) == 1 && length > 0) {
		scanf("%zd", &n_cuts);
		for (ssize_t i = 1; i <= n_cuts; ++i) {
			scanf("%zd", &cuts[i]);
		}
		cuts[0] = 0;
		cuts[n_cuts + 1] = length;
		memset(cache, -1, sizeof(cache));
		printf("The minimum cutting is %zd.\n", cut_costs(0, n_cuts + 1));
	}
	return 0;
}
