#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static char coding[] = {
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   '1', '2', '3', 0, '1', '2', 0,   0, '2', '2', '4', '5', '5', 0,
    '1', '2', '6', '2', '3', 0, '1', 0,   '2', 0, '2', 0,   0,   0,   0,   0,
    0,   0,   '1', '2', '3', 0, '1', '2', 0,   0, '2', '2', '4', '5', '5', 0,
    '1', '2', '6', '2', '3', 0, '1', 0,   '2', 0, '2', 0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
    0,   0,   0,   0,   0,   0, 0,   0,   0,   0, 0,   0,   0,   0,   0,   0,
};
static char input[64] = {};
static char output[64] = {};

int main() {
	while (scanf("%32s\n", &input[0]) == 1) {
		char last_code = '\0';
		char *out_ptr = &output[0];
		for (char *in_ptr = &input[0]; *in_ptr; ++in_ptr) {
			char current_code = coding[(size_t) *in_ptr];
			if (current_code != last_code) {
				if (current_code) {
					*out_ptr = current_code;
					++out_ptr;
				}
				last_code = current_code;
			}
		}
		*out_ptr = '\0';
		puts(output);
	}
	return 0;
}
