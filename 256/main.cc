#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>

using namespace std;

static char pre_calced[4][10][30];

static void prev(const int length) {
	size_t counter = 0;
	unsigned long long end = (unsigned long long) pow(10, length / 2);
	unsigned long long max = (unsigned long long) pow(10, length);

	for (unsigned long long i = 0; i < end; i++) {
		for (unsigned long long j = 0; j < end; j++) {
			unsigned long long u = (i + j) * (i + j);
			if (u > max) {
				break;
			}
			unsigned long long t = (unsigned long long) pow(10, length / 2) * i + j;
			if (u == t) {
				sprintf(pre_calced[length / 2 - 1][counter++], "%0*llu", length, t);
			}
		}
	}
}

int main() {
	prev(2);
	prev(4);
	prev(6);
	prev(8);

	int n;
	while (scanf("%d", &n) == 1) {
		n = n / 2 - 1;
		for (int i = 0; i < 10; i++) {
			if (pre_calced[n][i][0] == '\0') {
				break;
			}
			printf("%s\n", pre_calced[n][i]);
		}
	}
}
