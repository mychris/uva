#include <algorithm>
#include <cstdio>
#include <cstring>
#include <vector>

using namespace std;

static vector<unsigned int> deps[101];
static vector<bool> used(101);
static vector<unsigned int> result;
static unsigned int n_tasks;

static void run(void) {
	fill(used.begin(), used.end(), false);
	while (result.size() != n_tasks) {
		for (size_t i = 1; i <= n_tasks; ++i) {
			if (used[i] || !deps[i].empty()) {
				continue;
			}
			result.push_back((unsigned int) i);
			used[i] = true;
			for (size_t j = 1; j <= n_tasks; ++j) {
				auto to_remove = lower_bound(deps[j].begin(), deps[j].end(), i);
				if (to_remove != deps[j].end() && *to_remove == i) {
					deps[j].erase(to_remove);
				}
			}
			break;
		}
	}
}

int main() {
	result.reserve(101);
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		result.clear();
		unsigned int m;
		scanf("%u %u\n", &n_tasks, &m);
		if (n_tasks > 100 || m > 100) {
			continue;
		}
		while (m--) {
			unsigned task, n_deps, dep;
			scanf("%u %u", &task, &n_deps);
			deps[task].clear();
			while (n_deps--) {
				scanf("%u", &dep);
				deps[task].push_back(dep);
			}
			sort(deps[task].begin(), deps[task].end());
			unique(deps[task].begin(), deps[task].end());
		}
		run();
		for (auto it = result.begin(); it != result.end(); ++it) {
			if (it != result.begin()) {
				putchar(' ');
			}
			printf("%u", *it);
		}
		putchar('\n');
		if (_C) {
			putchar('\n');
		}
	}
	return 0;
}
