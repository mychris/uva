#include <cstdio>

using namespace std;

#define MAX 5000

static int repeated_digits[MAX + 1];
static int not_repeated_digits_cnt[MAX + 1];

static int has_repeated_digits(const int n) {
	if (n < 10) {
		return 0;
	}
	if (n < 100) {
		return n / 10 == n % 10;
	}
	if (n < 1000) {
		return n / 100 == n / 10 % 10 || n / 100 == n % 10 || n / 10 % 10 == n % 10;
	}
	int a = n / 1000;
	int b = n / 100 % 10;
	int c = n / 10 % 10;
	int d = n % 10;
	return a == b || a == c || a == d || b == c || b == d || c == d;
}

static void init(void) {
	repeated_digits[0] = 0;
	not_repeated_digits_cnt[0] = 0;
	int i;
	for (i = 1; i <= MAX; ++i) {
		repeated_digits[i] = has_repeated_digits(i);
		not_repeated_digits_cnt[i] =
		    not_repeated_digits_cnt[i - 1] + (repeated_digits[i] ? 0 : 1);
	}
}

int main() {
	init();
	int n, m;
	while (scanf("%d %d\n", &n, &m) == 2) {
		int cnt = not_repeated_digits_cnt[m] - not_repeated_digits_cnt[n];
		if (!repeated_digits[n]) {
			++cnt;
		}
		printf("%d\n", cnt);
	}
	return 0;
}
