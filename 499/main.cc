#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

static int count[255];

int main() {
	for (string line; getline(cin, line);) {
		int max = 0;
		if (line.empty()) {
			continue;
		}
		memset(count, 0, sizeof(count));
		for (char target : line) {
			if (isalpha(target)) {
				++count[(size_t) target];
				if (count[(size_t) target] > max) {
					max = count[(size_t) target];
				}
			}
		}
		for (int x = 'A'; x <= 'Z'; ++x) {
			if (count[x] == max) {
				putchar(x);
			}
		}
		for (int x = 'a'; x <= 'z'; ++x) {
			if (count[x] == max) {
				putchar(x);
			}
		}
		printf(" %d\n", max);
	}
}
