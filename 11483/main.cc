#include <cstdio>

using namespace std;

int main() {
	for (int the_case = 1;; ++the_case) {
		int lines = 0;
		if (scanf("%d\n", &lines) != 1 || lines <= 0) {
			break;
		}
		printf("Case %d:\n", the_case);
		puts("#include<string.h>");
		puts("#include<stdio.h>");
		puts("int main()");
		puts("{");
		fputs("printf(\"", stdout);
		int c;
		while ((c = getchar()) != EOF) {
			if (c == '\n') {
				--lines;
				puts("\\n\");");
				if (lines) {
					fputs("printf(\"", stdout);
				} else {
					break;
				}
			} else {
				if (c == '"' || c == '\\') {
					putchar('\\');
				}
				putchar(c);
			}
		}
		puts("printf(\"\\n\");");
		puts("return 0;");
		puts("}");
	}
	return 0;
}
