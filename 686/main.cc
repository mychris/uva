#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <vector>

using namespace std;

static int sieve[(1 << 15) + 1] = {};
static vector<int> primes;

static void do_sieve() {
	for (int i = 2; i < (1 << 14); ++i) {
		if (!sieve[i]) {
			primes.push_back(i);
			for (int j = i + i; j < (1 << 15); j += i) {
				sieve[j] = 1;
			}
		}
	}
}

int main() {
	do_sieve();
	int n;
	while (scanf("%d", &n) == 1 && n > 0) {
		size_t ctr = 0;
		for (vector<int>::iterator it = primes.begin();
		     it != primes.end() && *it <= n / 2;
		     ++it) {
			if (sieve[n - *it] == 0) {
				++ctr;
			}
		}
		printf("%zu\n", ctr);
	}
	return 0;
}
