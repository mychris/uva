#include <cstdio>
#include <cstring>

using namespace std;

static char irregular_from[21][100];
static char irregular_to[21][100];
static int n_irregular;

static int words;

static int is_consonant(const char x) {
	switch (x) {
	case 'b':
	case 'c':
	case 'd':
	case 'f':
	case 'g':
	case 'h':
	case 'j':
	case 'k':
	case 'l':
	case 'm':
	case 'n':
	case 'p':
	case 'q':
	case 'r':
	case 's':
	case 't':
	case 'v':
	case 'x':
	case 'z':
	case 'w':
	case 'y':
		return 1;
	default:
		return 0;
	}
}

int main() {
	int i, j;
	if (scanf("%d %d\n", &n_irregular, &words) != 2) {
		return 1;
	}
	for (i = 0; i < n_irregular; ++i) {
		if (scanf("%s %s\n", irregular_from[i], irregular_to[i]) != 2) {
			return 1;
		}
	}
	while (words--) {
		char word[100];
		if (scanf("%s\n", word) != 1) {
			return 1;
		}
		if (word[0] == '\0') {
			continue;
		}
		int is_irregular = 0;
		for (j = 0; j < n_irregular; ++j) {
			if (strcmp(word, irregular_from[j]) == 0) {
				is_irregular = 1;
				puts(irregular_to[j]);
				break;
			}
		}
		if (is_irregular) {
			continue;
		}
		size_t len = strlen(word);
		if (len >= 2 && is_consonant(word[len - 2]) && word[len - 1] == 'y') {
			word[len - 1] = 'i';
			word[len] = 'e';
			word[len + 1] = 's';
			word[len + 2] = '\0';
			puts(word);
		} else if (word[len - 1] == 'o' || word[len - 1] == 's' ||
		           word[len - 1] == 'x') {
			word[len] = 'e';
			word[len + 1] = 's';
			word[len + 2] = '\0';
			puts(word);
			continue;
		} else if (len >= 2 && ((word[len - 2] == 'c' && word[len - 1] == 'h') ||
		                        (word[len - 2] == 's' && word[len - 1] == 'h'))) {
			word[len] = 'e';
			word[len + 1] = 's';
			word[len + 2] = '\0';
			puts(word);
		} else {
			word[len] = 's';
			word[len + 1] = '\0';
			puts(word);
		}
	}
	return 0;
}
