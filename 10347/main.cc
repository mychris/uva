#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>

using namespace std;

#define likely(x) __builtin_expect(!!(x), 1)

#pragma GCC optimize("O3,fast-math")
static inline double
triangle_area_given_medians(const double u, const double v, const double w) {
	if (u > 0.0 && v > 0.0 && w > 0.0) {
		const double u_2 = u * u;
		const double v_2 = v * v;
		const double w_2 = w * w;
		const double squares = u_2 * v_2 + v_2 * w_2 + w_2 * u_2;
		const double quads = u_2 * u_2 + v_2 * v_2 + w_2 * w_2;
		const double d_squares = 2 * squares;
		if (likely(d_squares > quads)) {
			const double result = sqrt(d_squares - quads) / 3.0;
			if (likely(result > 0.0)) {
				return result;
			}
		}
	}
	return -1.0;
}

int main() {
	double u, v, w;
	while (scanf("%lf %lf %lf", &u, &v, &w) == 3) {
		printf("%.3lf\n", triangle_area_given_medians(u, v, w));
	}
	return 0;
}
