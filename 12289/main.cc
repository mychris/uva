#include <cstdio>
#include <cstring>

using namespace std;

static char word[10];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		scanf("%s\n", word);
		word[9] = '\0';
		if (strlen(word) == 5) {
			puts("3");
			continue;
		}
		if ((word[0] == 'o' && word[1] == 'n') || (word[0] == 'o' && word[2] == 'e') ||
		    (word[1] == 'n' && word[2] == 'e')) {
			puts("1");
		} else {
			puts("2");
		}
	}
	return 0;
}
