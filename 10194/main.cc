#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

static char input[128];
static char name[128];
static int teams;
static int games;

typedef struct result {
	char name[32];
	int points;
	int games;
	int wins;
	int ties;
	int losses;
	int goal_diff;
	int scored;
	int against;
} result;

int comp(void const *a, void const *b) {
	result const *left = (result const *) a;
	result const *right = (result const *) b;
	if (left->points != right->points) {
		return right->points - left->points;
	}
	if (left->wins != right->wins) {
		return right->wins - left->wins;
	}
	if (left->goal_diff != right->goal_diff) {
		return right->goal_diff - left->goal_diff;
	}
	if (left->scored != right->scored) {
		return right->scored - left->scored;
	}
	if (left->games != right->games) {
		return left->games - right->games;
	}
	return strcasecmp(left->name, right->name);
}

static result ranks[32];

void read_line(char *buf) {
	int c = 0;
	while ((c = getchar()) && c > 0 && isspace(c)) {
	}
	if (c > 0) {
		ungetc(c, stdin);
	} else {
		return;
	}

	int i = 0;
	while ((c = getchar()) && c > 0 && c != '\n') {
		buf[i++] = (char) c;
	}
	buf[i] = '\0';
}

static void handle_input(void) {
	char *team_a = &input[0];
	int team_a_score = 0;
	char *team_b = &input[0];
	int team_b_score = 0;

	team_b = strstr(team_b, "#");
	*team_b = '\0';
	++team_b;
	sscanf(team_b, "%d", &team_a_score);

	team_b = strstr(team_b, "@") + 1;
	sscanf(team_b, "%d", &team_b_score);

	team_b = strstr(team_b, "#") + 1;

	for (int i = 0; i < teams; ++i) {
		if (strcmp(team_a, ranks[i].name) == 0) {
			ranks[i].games += 1;
			ranks[i].goal_diff += team_a_score;
			ranks[i].goal_diff -= team_b_score;
			ranks[i].scored += team_a_score;
			ranks[i].against += team_b_score;
			if (team_a_score > team_b_score) {
				ranks[i].points += 3;
				ranks[i].wins += 1;
			} else if (team_a_score == team_b_score) {
				ranks[i].points += 1;
				ranks[i].ties += 1;
			} else {
				ranks[i].losses += 1;
			}
		}
		if (strcmp(team_b, ranks[i].name) == 0) {
			ranks[i].games += 1;
			ranks[i].goal_diff += team_b_score;
			ranks[i].goal_diff -= team_a_score;
			ranks[i].scored += team_b_score;
			ranks[i].against += team_a_score;
			if (team_b_score > team_a_score) {
				ranks[i].points += 3;
				ranks[i].wins += 1;
			} else if (team_a_score == team_b_score) {
				ranks[i].points += 1;
				ranks[i].ties += 1;
			} else {
				ranks[i].losses += 1;
			}
		}
	}
}

int main() {
	int _N;
	if (scanf("%d\n", &_N) != 1) {
		return 1;
	}
	while (_N--) {
		read_line(&name[0]);
		puts(name);
		scanf("%d\n", &teams);
		for (int i = 0; i < teams; ++i) {
			memset(&ranks[i], 0, sizeof(struct result));
			read_line(&ranks[i].name[0]);
		}
		scanf("%d\n", &games);
		for (int i = 0; i < games; ++i) {
			read_line(&input[0]);
			handle_input();
		}
		qsort(&ranks[0], (size_t) teams, sizeof(struct result), comp);
		for (int i = 0; i < teams; ++i) {
			printf("%d) %s %dp, %dg (%d-%d-%d), %dgd (%d-%d)\n",
			       i + 1,
			       ranks[i].name,
			       ranks[i].points,
			       ranks[i].games,
			       ranks[i].wins,
			       ranks[i].ties,
			       ranks[i].losses,
			       ranks[i].goal_diff,
			       ranks[i].scored,
			       ranks[i].against);
		}
		if (_N) {
			puts("");
		}
	}
	return 0;
}
