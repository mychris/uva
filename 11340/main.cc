#include <cstdio>
#include <cstring>

using namespace std;

unsigned long long cents_per_char[256];
char line[20000];

static size_t read_line() {
	size_t i;
	for (i = 0;; ++i) {
		int c = getchar();
		if (c == '\n') {
			break;
		}
		line[i] = (char) c;
	}
	line[i] = '\0';
	return i;
}

static unsigned long long run(int lines) {
	unsigned long long result = 0;
	while (lines--) {
		size_t chars = read_line();
		size_t i;
		for (i = 0; i < chars; ++i) {
			result += cents_per_char[(size_t) line[i]];
		}
	}
	return result;
}

int main() {
	int tests;
	read_line();
	sscanf(line, "%d", &tests);
	while (tests--) {
		memset(&cents_per_char[0], 0, sizeof(int) * 256);
		int paid_chars, lines;
		read_line();
		sscanf(line, "%d", &paid_chars);
		while (paid_chars--) {
			unsigned int cents;
			char character;
			read_line();
			sscanf(line, "%c %u", &character, &cents);
			cents_per_char[(size_t) character] = cents;
		}
		read_line();
		sscanf(line, "%d", &lines);
		unsigned long long amount = run(lines);
		unsigned long long dollars = amount / 100LLU;
		unsigned long long cents = amount % 100LLU;
		if (cents < 10) {
			printf("%llu.0%llu$\n", dollars, cents);
		} else {
			printf("%llu.%llu$\n", dollars, cents);
		}
	}
	return 0;
}
