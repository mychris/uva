#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cmath>
#include <cstdio>

using namespace std;

/*
   First, go up to the biggest step we can possibly take, then go down to 1 again.
   After that, fill in as many "biggest steps we can possibly take" in between, until
   we reached or overshoot the position.
*/

static unsigned int isqrt(unsigned int x) {
	unsigned int m, y, b;
	m = ((unsigned int) 1u) << ((sizeof(unsigned int) * CHAR_BIT) - 2u);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

static inline unsigned int solve(const unsigned int y) {
	const unsigned int biggest_possible_steps = isqrt(y);
	const unsigned int length_of_biggest_possible_step =
	    ((biggest_possible_steps * (biggest_possible_steps + 1)) +
	     (biggest_possible_steps * (biggest_possible_steps - 1))) /
	    2;
	unsigned int number_of_steps = 2 * biggest_possible_steps - 1;
	unsigned int length = length_of_biggest_possible_step;
	while (length < y) {
		number_of_steps++;
		length += biggest_possible_steps;
	}
	return number_of_steps;
}

int main() {
	int N;
	unsigned int x, y;
	scanf("%d", &N);
	while (N--) {
		scanf("%u %u", &x, &y);
		if (x >= y) {
			puts("0");
		} else {
			printf("%u\n", solve(y - x));
		}
	}
	return 0;
}
