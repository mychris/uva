#include <algorithm>
#include <cstdio>
#include <cstring>

using namespace std;

static char input[16];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		scanf("%s", input);
		size_t len = strlen(input);
		sort(input, input + len);
		do {
			printf("%s\n", input);
		} while (next_permutation(input, input + len));
		putchar('\n');
	}
	return 0;
}
