#include <cstdio>
#include <iostream>
#include <map>
#include <string>

using namespace std;

int main() {
	int _C;
	map<string, int> map;
	string line;

	scanf("%d", &_C);
	getline(cin, line);
	while (_C--) {
		cin >> line;
		auto search = map.find(line);
		if (search != map.end()) {
			search->second += 1;
		} else {
			map.insert({line, 1});
		}
		getline(cin, line);
	}
	for (auto it : map) {
		printf("%s %d\n", it.first.c_str(), it.second);
	}
	return 0;
}
