#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int code[1000];
static int code_num_ctr[10];
static int code_length;

static int guess[1000];
static int guess_num_ctr[10];

int main() {
	int game = 1;
	while (scanf("%d\n", &code_length) == 1 && code_length > 0) {
		memset(&code_num_ctr[0], 0, sizeof(code_num_ctr));
		printf("Game %d:\n", game++);
		for (int i = 0; i < code_length; ++i) {
			scanf("%d", &code[i]);
			code_num_ctr[code[i]]++;
		}
		for (;;) {
			memset(&guess_num_ctr[0], 0, sizeof(guess_num_ctr));
			for (int i = 0; i < code_length; ++i) {
				scanf("%d", &guess[i]);
				guess_num_ctr[guess[i]]++;
			}
			if (guess_num_ctr[0] != 0) {
				break;
			}
			int strong = 0, weak = 0;
			for (int i = 1; i <= 9; ++i) {
				weak += min(code_num_ctr[i], guess_num_ctr[i]);
			}
			for (int i = 0; i < code_length; ++i) {
				if (code[i] == guess[i]) {
					--weak;
					++strong;
				}
			}
			printf("    (%d,%d)\n", strong, weak);
		}
	}
	return 0;
}
