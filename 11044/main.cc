#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int n, m;
		scanf("%d %d\n", &n, &m);
		n -= 2;
		m -= 2;
		n = n / 3 + ((n % 3 == 0) ? 0 : 1);
		m = m / 3 + ((m % 3 == 0) ? 0 : 1);
		printf("%d\n", m * n);
	}
}
