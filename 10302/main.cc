#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>

using namespace std;

static uint64_t cache[50001];

int main() {
	uint64_t x;
	cache[0] = 0;
	cache[1] = 1;
	for (x = 2; x <= 50000; ++x) {
		cache[x] = cache[x - 1] + x * x * x;
	}
	while (scanf("%" SCNu64 "\n", &x) == 1) {
		printf("%" PRIu64 "\n", cache[x]);
	}
	return 0;
}
