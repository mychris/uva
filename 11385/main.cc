#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cinttypes>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>

using namespace std;

static map<uint64_t, uint64_t> fibonacci_numbers;

static uint64_t fibs[128];
static char input[128];
static char output[128];

static void read_input(void) {
	int c;
	while ((c = getc(stdin)) > 0 && !isalpha(c)) {
	}
	ungetc(c, stdin);
	if (c == 0) {
		abort();
	}
	size_t ptr = 0;
	while ((c = getc(stdin)) > 0 && c != '\n') {
		if (isupper(c)) {
			input[ptr++] = (char) c;
		}
	}
}

int main() {
	fibonacci_numbers[1] = 1;
	fibonacci_numbers[2] = 2;
	uint64_t a = 1, b = 2, ptr = 3;
	for (;;) {
		uint64_t tmp = a + b;
		if (tmp > (uint64_t) (UINT64_C(1) << 31)) {
			break;
		}
		fibonacci_numbers[tmp] = ptr++;
		a = b;
		b = tmp;
	}

	size_t _N;
	scanf("%zu\n", &_N);
	while (_N--) {
		memset(output, ' ', sizeof(output));
		size_t f;
		scanf("%zu\n", &f);
		for (size_t i = 0; i < f; ++i) {
			scanf("%" SCNu64, &fibs[i]);
		}
		read_input();

		size_t last = 0;
		for (size_t i = 0; i < f; ++i) {
			assert(fibonacci_numbers.find(fibs[i]) != fibonacci_numbers.end());
			size_t idx = fibonacci_numbers[fibs[i]] - 1;
			assert(idx < sizeof(output) - 1);
			output[idx] = input[i];
			last = max(last, idx);
		}
		output[last + 1] = '\0';
		puts(output);
	}
	return 0;
}
