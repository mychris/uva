#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstring>

#include <unistd.h>

using namespace std;

static char line[32];
static size_t spaces[32];

int main() {
	size_t N;
	while (scanf("%zu", &N) == 1 && N > 0) {
		size_t result = 0;
		size_t min_void = 26;
		while (getchar() != 'X') {
		}
		ungetc('X', stdin);
		for (size_t i = 0; i < N; ++i) {
			size_t r = 0;
			while (r < 26) {
				r += fread(&line[0], sizeof(char), 26 - r, stdin);
			}
			char *left = &line[0];
			char *right = &line[24];
			while (*left == 'X') {
				++left;
			}
			while (*right == 'X') {
				--right;
			}
			size_t cur_spaces = 0;
			if (right >= left) {
				cur_spaces = (size_t) (right - left) + 1;
			}
			if (cur_spaces < min_void) {
				min_void = cur_spaces;
			}
			spaces[i] = cur_spaces;
		}
		for (size_t i = 0; i < N; ++i) {
			result += spaces[i] - min_void;
		}
		printf("%zu\n", result);
	}
	return 0;
}
