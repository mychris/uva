#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

class Dimension {
  public:
	int lbound, ubound, size;
};

class Decl {
  public:
	char name[20];
	int base, elem_size, dimensions;
	Dimension dim[20];
};

static int n_declarations;
static Decl *declarations;

int read_decl(Decl *decl) {
	if (scanf("%s %d %d %d",
	          &decl->name[0],
	          &decl->base,
	          &decl->elem_size,
	          &decl->dimensions) != 4) {
		return 0;
	}
	for (int i = 0; i < decl->dimensions; ++i) {
		if (scanf("%d %d", &decl->dim[i].lbound, &decl->dim[i].ubound) != 2) {
			return 0;
		}
	}
	decl->dim[decl->dimensions - 1].size = decl->elem_size;
	for (int i = decl->dimensions - 2; i >= 0; --i) {
		decl->dim[i].size = (decl->dim[i + 1].ubound - decl->dim[i + 1].lbound + 1) *
		                    decl->dim[i + 1].size;
	}
	return 1;
}

Decl *find_decl(char *name) {
	for (int i = 0; i < n_declarations; ++i) {
		if (strcmp(name, declarations[i].name) == 0) {
			return &declarations[i];
		}
	}
	return nullptr;
}

int main() {
	int _N, _R;
	if (scanf("%d %d\n", &_N, &_R) != 2 || _N < 0 || _R < 0) {
		return 1;
	}
	n_declarations = _N;
	declarations = new Decl[n_declarations];
	while (_N--) {
		if (!read_decl(&declarations[_N])) {
			return 1;
		}
	}
	while (_R--) {
		char name[20];
		Decl *arr_decl;
		int offset = 0;
		if (scanf("%s", &name[0]) != 1) {
			return 2;
		}
		fputs(&name[0], stdout);
		if ((arr_decl = find_decl(&name[0])) == nullptr) {
			return 2;
		}
		char const *sep = "[";
		for (int i = 0; i < arr_decl->dimensions; ++i) {
			int index;
			if (scanf("%d", &index) != 1) {
				return 1;
			}
			printf("%s%d", sep, index);
			sep = ", ";
			offset += (index - arr_decl->dim[i].lbound) * arr_decl->dim[i].size;
		}
		printf("] = %d\n", offset + arr_decl->base);
	}
	return 0;
}
