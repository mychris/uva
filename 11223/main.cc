#include <cstdio>
#include <cstring>
#include <iostream>
#include <map>
#include <string>

using namespace std;

static map<string, char> mapping = {
    {".-", 'A'},     {"-...", 'B'},   {"-.-.", 'C'},   {"-..", 'D'},
    {".", 'E'},      {"..-.", 'F'},   {"--.", 'G'},    {"....", 'H'},
    {"..", 'I'},     {".---", 'J'},   {"-.-", 'K'},    {".-..", 'L'},
    {"--", 'M'},     {"-.", 'N'},     {"---", 'O'},    {".--.", 'P'},
    {"--.-", 'Q'},   {".-.", 'R'},    {"...", 'S'},    {"-", 'T'},
    {"..-", 'U'},    {"...-", 'V'},   {".--", 'W'},    {"-..-", 'X'},
    {"-.--", 'Y'},   {"--..", 'Z'},   {"-----", '0'},  {".----", '1'},
    {"..---", '2'},  {"...--", '3'},  {"....-", '4'},  {".....", '5'},
    {"-....", '6'},  {"--...", '7'},  {"---..", '8'},  {"----.", '9'},
    {".-.-.-", '.'}, {"--..--", ','}, {"..--..", '?'}, {".----.", '\''},
    {"-.-.--", '!'}, {"-..-.", '/'},  {"-.--.", '('},  {"-.--.-", ')'},
    {".-...", '&'},  {"---...", ':'}, {"-.-.-.", ';'}, {"-...-", '='},
    {".-.-.", '+'},  {"-....-", '-'}, {"..--.-", '_'}, {".-..-.", '"'},
    {".--.-.", '@'}, {"@", ' '},
};

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		string line;
		getline(cin, line);
		printf("Message #%d\n", the_case);
		size_t len = line.length(), cnt = 0;
		for (size_t i = 0; i < len; i++) {
			if (isspace(line[i])) {
				if (cnt == 1) {
					cout << ' ';
					cnt = 0;
				} else
					cnt++;
				continue;
			}
			size_t j = i;
			string s = "";
			while (!isspace(line[j]) && j < len) {
				s += line[j];
				j++;
			}
			putchar(mapping[s]);
			cnt = 0;
			i = j - 1;
		}
		putchar('\n');
		if (the_case != _C) {
			putchar('\n');
		}
	}
	return 0;
}
