#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char line[32] = {};
static int img[32][32] = {};
static int dim = 0;

static void zero_neighbours(int x, int y) {
	if (!img[x][y])
		return;
	img[x][y] = 0;

	zero_neighbours(x - 1, y - 1);
	zero_neighbours(x - 1, y);
	zero_neighbours(x - 1, y + 1);

	zero_neighbours(x, y - 1);
	zero_neighbours(x, y);
	zero_neighbours(x, y + 1);

	zero_neighbours(x + 1, y - 1);
	zero_neighbours(x + 1, y);
	zero_neighbours(x + 1, y + 1);
}

int main() {
	int n = 1;
	while (fscanf(stdin, "%d", &dim) == 1) {
#if defined(NDEBUG)
		for (int x = 0; x < 32; ++x) {
			for (int y = 0; y < 32; ++y) {
				assert(img[x][y] == 0);
			}
		}
#endif
		int count = 0;
		for (int x = 0; x < dim; ++x) {
			scanf("%s\n", line);
			assert((int) strlen(line) == dim);
			for (int y = 0; y < dim; ++y) {
				img[x + 1][y + 1] = line[y] - '0';
			}
		}
		for (int x = 1; x <= dim; ++x) {
			for (int y = 1; y <= dim; ++y) {
				if (img[x][y]) {
					count += 1;
					zero_neighbours(x, y);
				}
			}
		}
		printf("Image number %d contains %d war eagles.\n", n, count);
		++n;
	}
	return 0;
}
