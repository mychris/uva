#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int swap(int *arr, size_t size) {
	bool has_swapped = true;
	int ctr = 0;

	while (has_swapped) {
		has_swapped = false;
		for (size_t i = 0; i < size - 1; ++i) {
			if (arr[i] > arr[i + 1]) {
				int tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				has_swapped = true;
				++ctr;
			}
		}
	}
	return ctr;
}

static int arr[200];

int main() {
	int _C;
	scanf("%d", &_C);
	while (_C--) {
		size_t n;
		scanf("%zu", &n);
		for (size_t i = 0; i < n; ++i) {
			scanf("%d", &arr[i]);
		}
		printf("Optimal train swapping takes %d swaps.\n", swap(arr, n));
	}
	return 0;
}
