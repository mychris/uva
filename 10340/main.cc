#include <iostream>
#include <string>

using namespace std;

int main() {
	for (;;) {
		bool is_sub = true;
		string needle, haystack;
		if (!(cin >> needle >> haystack)) {
			break;
		}
		for (string::const_iterator it = needle.cbegin(); it != needle.cend(); ++it) {
			size_t f = haystack.find_first_of(*it);
			if (f == string::npos) {
				is_sub = false;
				break;
			}
			haystack.erase(haystack.begin(), haystack.begin() + (ssize_t) f + 1U);
		}
		puts((is_sub) ? "Yes" : "No");
	}
	return 0;
}
