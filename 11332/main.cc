#include <cstdio>

using namespace std;

static int f(int x) {
	int ret = 0;
	while (x) {
		ret += x % 10;
		x /= 10;
	}
	return ret;
}

int main() {
	int n;
	while (scanf("%d\n", &n) == 1 && n != 0) {
		printf("%d\n", f(f(f(n))));
	}
	return 0;
}
