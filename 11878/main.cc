#include <algorithm>
#include <iostream>
#include <string>

using namespace std;

int main() {
	int counter = 0;
	for (string line; getline(cin, line);) {
		line.erase(remove(line.begin(), line.end(), ' '), line.end());
		if (line.empty() || line.find('?') != string::npos) {
			continue;
		}
		auto sign = line.find('-');
		if (sign == string::npos) {
			sign = line.find('+');
		}
		char signChar = line[sign];

		auto equ = line.find('=');

		int c = stoi(line.substr(equ + 1));
		int a = stoi(line.substr(0, sign));
		int b = stoi(line.substr(sign + 1, equ));

		int calcC = (signChar == '+') ? a + b : a - b;
		if (calcC == c) {
			++counter;
		}
	}
	printf("%d\n", counter);
	return 0;
}
