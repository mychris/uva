#include <cstdio>

using namespace std;

struct Person {
	char name[30];
	int day;
	int month;
	int year;

	friend inline bool operator<(const Person &lhs, const Person &rhs) {
		if (lhs.year != rhs.year) {
			return lhs.year > rhs.year;
		}
		if (lhs.month != rhs.month) {
			return lhs.month > rhs.month;
		}
		if (lhs.day != rhs.day) {
			return lhs.day > rhs.day;
		}
		return false;
	}

	friend inline bool operator>(const Person &lhs, const Person &rhs) {
		if (lhs.year != rhs.year) {
			return lhs.year < rhs.year;
		}
		if (lhs.month != rhs.month) {
			return lhs.month < rhs.month;
		}
		if (lhs.day != rhs.day) {
			return lhs.day < rhs.day;
		}
		return false;
	}
};

int main() {
	int persons;
	scanf("%d\n", &persons);
	if (persons <= 0) {
		return 0;
	}
	if (persons == 1) {
		Person cur;
		scanf("%s %d %d %d\n", cur.name, &cur.day, &cur.month, &cur.year);
		puts(cur.name);
		puts(cur.name);
		return 0;
	}
	Person youngest, oldest;
	youngest.year = -1;
	oldest.year = 99999;
	while (persons--) {
		Person cur;
		scanf("%s %d %d %d\n", cur.name, &cur.day, &cur.month, &cur.year);
		if (cur < youngest) {
			youngest = cur;
		}
		if (cur > oldest) {
			oldest = cur;
		}
	}
	puts(youngest.name);
	puts(oldest.name);
	return 0;
}
