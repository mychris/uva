#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <list>

using namespace std;

static long long Z, I, M, L;

static long long next(void) {
	return (L * Z + I) % M;
}

int main() {
	int the_case = 1;
	while (scanf("%lld %lld %lld %lld\n", &Z, &I, &M, &L) != EOF && Z > 0 && I > 0 &&
	       M > 0 && L > 0) {
		list<long long> seen;
		long long start = L;
		while (find(seen.begin(), seen.end(), L) == end(seen)) {
			seen.push_back(L);
			L = next();
		}
		size_t len = seen.size();
		if (L != start) {
			--len;
		}
		printf("Case %d: %zu\n", the_case++, len);
	}
	return 0;
}
