#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>
#include <vector>

struct Point {
	float x;
	float y;
};

class Shape {
  public:
	virtual bool contains(Point const pt) const = 0;
	virtual ~Shape() {
	}
};

struct Rectangle : Shape {
	Point ul, lr;
	Rectangle(Point &&upper_left, Point &&lower_right)
	    : ul(upper_left), lr(lower_right) {
	}

	bool contains(Point const pt) const {
		return pt.y > lr.y && pt.y < ul.y && pt.x > ul.x && pt.x < lr.x;
	}

	~Rectangle() {
	}
};

struct Circle : Shape {
	Point ctr;
	float rad;
	Circle(Point &&center, float radius) : ctr(center), rad(radius) {
	}

	bool contains(Point const pt) const {
		float a = fabsf(pt.x - ctr.x);
		float b = fabsf(pt.y - ctr.y);
		float distance = a * a + b * b;
		return distance < rad * rad;
	}
	~Circle() {
	}
};

struct Triangle : Shape {
	Point v1, v2, v3;
	Triangle(Point &&vec1, Point &&vec2, Point &&vec3) : v1(vec1), v2(vec2), v3(vec3) {
	}

	static float sign(Point const p1, Point const p2, Point const p3) {
		return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
	}

	bool contains(Point const pt) const {
		bool const b1 = sign(pt, v1, v2) <= 0.0f;
		bool const b2 = sign(pt, v2, v3) <= 0.0f;
		bool const b3 = sign(pt, v3, v1) <= 0.0f;
		return ((b1 == b2) && (b2 == b3));
	}
	~Triangle() {
	}
};

using namespace std;

int main() {
	vector<Shape *> shapes;
	char type;
	while (scanf("%c", &type) == 1 && type != '*') {
		switch (type) {
		case 'r':
			Point ul, lr;
			scanf("%f %f %f %f\n", &ul.x, &ul.y, &lr.x, &lr.y);
			shapes.push_back(new Rectangle(move(ul), move(lr)));
			break;
		case 'c':
			Point center;
			float radius;
			scanf("%f %f %f\n", &center.x, &center.y, &radius);
			shapes.push_back(new Circle(move(center), radius));
			break;
		case 't':
			Point v1, v2, v3;
			scanf("%f %f %f %f %f %f\n", &v1.x, &v1.y, &v2.x, &v2.y, &v3.x, &v3.y);
			shapes.push_back(new Triangle(move(v1), move(v2), move(v3)));
			break;
		default:
			fprintf(stderr, "Invalid type %c\n", type);
			abort();
		}
	}
	if (type != '*') {
		return 2;
	}
	Point pt;
	size_t const shapes_size = shapes.size();
	size_t point_number = 1;
	while (scanf("%f %f\n", &pt.x, &pt.y) == 2 && pt.x < 9999.9 && pt.y < 9999.9) {
		bool contained = false;
		for (size_t i = 0; i < shapes_size; ++i) {
			if (shapes[i]->contains(pt)) {
				contained = true;
				printf("Point %zu is contained in figure %zu\n", point_number, i + 1);
			}
		}
		if (!contained) {
			printf("Point %zu is not contained in any figure\n", point_number);
		}
		++point_number;
	}
#if !defined(ONLINE_JUDGE)
	for (Shape *s : shapes) {
		delete s;
	}
#endif
	return 0;
}
