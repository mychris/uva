#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>

using namespace std;

static bool isVowel(char c) {
	switch (c) {
	case 'a':
	case 'A':
	case 'e':
	case 'E':
	case 'i':
	case 'I':
	case 'o':
	case 'O':
	case 'u':
	case 'U':
		return true;
	default:
		return false;
	}
}

int main() {
	bool in_word = false;
	bool starting_vowel = false;
	int consonant = ' ';
	int c;
	while ((c = getchar()) != EOF) {
		bool is_alpha = isalpha(c);
		if (in_word) {
			if (!is_alpha) {
				if (!starting_vowel) {
					putchar(consonant);
				}
				putchar('a');
				putchar('y');
				in_word = false;
			}
			putchar(c);
		} else if (is_alpha) {
			in_word = true;
			consonant = c;
			if (isVowel((char) c)) {
				starting_vowel = true;
				putchar(c);
			} else {
				starting_vowel = false;
			}
		} else {
			putchar(c);
		}
	}
}
