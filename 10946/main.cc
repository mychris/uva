#include <algorithm>
#include <cstdio>
#include <cstring>
#include <vector>

using namespace std;

struct result {
	int num;
	char c;

	friend bool operator<(const result &lhs, const result &rhs) {
		if (lhs.num == rhs.num) {
			return lhs.c > rhs.c;
		}
		return lhs.num < rhs.num;
	}
};

static char input[64][64];
static int width, height;

static int check(int row, int col, char c) {
	if (row < 0 || col < 0 || row >= height || col >= width) {
		return 0;
	}
	if (input[row][col] == c) {
		input[row][col] = '.';
		return 1 + check(row - 1, col, c) + check(row + 1, col, c) +
		       check(row, col - 1, c) + check(row, col + 1, c);
	}
	return 0;
}

int main() {
	int problem = 1;
	while (scanf("%d %d\n", &height, &width) == 2 && width > 0 && height > 0) {
		vector<result> results;
		results.reserve(26);
		for (int i = 0; i < height; ++i) {
			fgets(input[i], sizeof(input[i]), stdin);
		}
		for (int row = 0; row < height; ++row) {
			for (int col = 0; col < width; ++col) {
				char character = input[row][col];
				if (character == '.') {
					continue;
				}
				int cnt = check(row, col, character);
				if (cnt) {
					results.push_back(result{cnt, character});
				}
			}
		}
		std::sort(results.begin(), results.end());
		std::reverse(results.begin(), results.end());
		printf("Problem %d:\n", problem++);
		for (auto result : results) {
			printf("%c %d\n", result.c, result.num);
		}
	}
	return 0;
}
