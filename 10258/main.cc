#include <algorithm>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#define MAX_C 100
#define MAX_P 10

using namespace std;

struct judge_in {
	unsigned int contestant;
	unsigned int problem;
	unsigned int time;
	char correct;
};

struct score {
	unsigned int contestant;
	unsigned int solved;
	unsigned int penalty;
};

bool score_cmp(score const &a, score const &b) {
	if (a.solved == b.solved) {
		if (a.penalty == b.penalty) {
			return a.contestant < b.contestant;
		}
		return a.penalty < b.penalty;
	}
	return a.solved > b.solved;
}

int main() {
	int _C;
	string line;
	scanf("%d\n", &_C);
	while (_C--) {
		vector<judge_in> input;
		vector<bool> participating(MAX_C);
		while (1) {
			getline(cin, line);
			if (line.empty()) {
				break;
			}
			judge_in in;
			sscanf(line.c_str(),
			       "%u %u %u %c",
			       &in.contestant,
			       &in.problem,
			       &in.time,
			       &in.correct);
			--in.contestant;
			--in.problem;
			participating[in.contestant] = true;
			input.push_back(in);
		}
		sort(input.begin(), input.end(), [](judge_in a, judge_in b) {
			return a.time < b.time;
		});

		vector<score> scores(MAX_C);
		vector<unsigned int> penalty(MAX_C * MAX_P);
		vector<bool> solved(MAX_C * MAX_P);

		for (vector<judge_in>::iterator it = input.begin(); it != input.end(); ++it) {
			unsigned int C = it->contestant;
			unsigned int P = it->problem;
			switch (it->correct) {
			case 'I':
				penalty[C * MAX_P + P] += 20;
				break;
			case 'C':
				if (!solved[C * MAX_P + P]) {
					scores[C].solved += 1;
					scores[C].penalty += penalty[C * MAX_P + P];
					scores[C].penalty += it->time;
					solved[C * MAX_P + P] = true;
				}
				break;
			default:
				break;
			}
		}

		vector<score> result;

		for (size_t i = 0; i < MAX_C; ++i) {
			if (participating[i]) {
				result.push_back(
				    {(unsigned int) i + 1, scores[i].solved, scores[i].penalty});
			}
		}
		sort(result.begin(), result.end(), &score_cmp);
		for (vector<score>::iterator it = result.begin(); it != result.end(); ++it) {
			printf("%u %u %u\n", it->contestant, it->solved, it->penalty);
		}
		if (_C) {
			printf("\n");
		}
	}
	return 0;
}
