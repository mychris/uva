#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

struct item {
	int id;
	int period;
	int cur;

	friend bool operator<(const item &lhs, const item &rhs) {
		if (lhs.cur == rhs.cur) {
			return lhs.id > rhs.id;
		}
		return lhs.cur > rhs.cur;
	}
};

int main() {
	vector<item> items;
	for (string line; getline(cin, line);) {
		if (line == "#") {
			break;
		}
		stringstream in(line);
		in.ignore(9);
		int id, period;
		in >> id >> period;
		items.push_back({id, period, period});
	}
	sort(items.begin(), items.end());
	int k;
	cin >> k;
	while (k--) {
		item cur = items.back();
		items.pop_back();
		cout << cur.id << endl;

		cur.cur += cur.period;
		auto pos = lower_bound(items.begin(), items.end(), cur);
		items.insert(pos, cur);
	}
	return 0;
}
