#include <algorithm>
#include <climits>
#include <cstdio>
#include <cstring>

using namespace std;

static int cache[256];
static char line[1024];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		fgets(line, sizeof(line), stdin);
		memset(cache, 0, sizeof(cache));
		char *c = &line[0];
		while (*c) {
			++cache[(size_t) *c];
			++c;
		}
		// MARGARITA
		int possible = cache[(size_t) 'A'] / 3;
		possible = min(possible, cache[(size_t) 'G']);
		possible = min(possible, cache[(size_t) 'I']);
		possible = min(possible, cache[(size_t) 'M']);
		possible = min(possible, cache[(size_t) 'R'] / 2);
		possible = min(possible, cache[(size_t) 'T']);
		printf("%d\n", possible);
	}
	return 0;
}
