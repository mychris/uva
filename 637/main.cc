#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int pages;
	while (scanf("%d\n", &pages) == 1 && pages > 0) {
		printf("Printing order for %d pages:\n", pages);
		if (pages == 1) {
			puts("Sheet 1, front: Blank, 1");
			continue;
		}
		int first = 1;
		int last = (pages / 4 + ((pages % 4 == 0) ? 0 : 1)) * 4;
		int sheet = 1;
		while (first < last) {
			if (last > pages) {
				printf("Sheet %d, front: Blank, %d\n", sheet, first);
			} else {
				printf("Sheet %d, front: %d, %d\n", sheet, last, first);
			}
			++first;
			--last;
			if (last > pages) {
				printf("Sheet %d, back : %d, Blank\n", sheet, first);
			} else {
				printf("Sheet %d, back : %d, %d\n", sheet, first, last);
			}
			++first;
			--last;
			++sheet;
		}
	}
	return 0;
}
