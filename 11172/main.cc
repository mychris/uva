#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		long long a, b;
		scanf("%lld %lld\n", &a, &b);
		if (a < b) {
			puts("<");
		} else if (a > b) {
			puts(">");
		} else {
			puts("=");
		}
	}
	return 0;
}
