#include <cstdio>

using namespace std;

static const char *results[] = {
    "0",
    "1",
    "2",
    "6",
    "24",
    "120",
    "720",
    "5040",
    "40320",
    "362880",
    "3628800",
    "39916800",
    "479001600",
    "6227020800",
};

int main() {
	int n;
	while (scanf("%d\n", &n) == 1) {
		if (n < 0 && n & 1) {
			puts("Overflow!");
		} else if (n < 0 && !(n & 1)) {
			puts("Underflow!");
		} else if (n >= 14) {
			puts("Overflow!");
		} else if (n <= 7) {
			puts("Underflow!");
		} else {
			puts(results[n]);
		}
	}
	return 0;
}
