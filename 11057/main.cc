#include <algorithm>
#include <cstdio>

using namespace std;

static int books[10001];

static inline bool read_int(int *i) {
	return scanf("%d", i) == 1;
}

int main() {
	for (;;) {
		int sum, n_books;
		if (scanf("%d", &n_books) != 1) {
			break;
		}
		for (int i = 0; i < n_books; ++i) {
			scanf("%d", books + i);
		}
		scanf("%d", &sum);
		sort(books, books + n_books);
		int i = 0, j = n_books - 1;
		int a = books[0], b = books[n_books - 1];
		while (i < j) {
			int s = books[i] + books[j];
			if (s > sum) {
				--j;
			} else if (s < sum) {
				++i;
			} else {
				a = books[i];
				b = books[j];
				--j;
				++i;
			}
		}
		printf("Peter should buy books whose prices are %d and %d.\n\n", a, b);
	}
	return 0;
}
