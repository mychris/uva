#include <cstdio>

using namespace std;

static char buffer[1000];

static int read_next() {
	int c, ret = 0, pos = 0;
	while ((c = getchar())) {
		if (c == EOF) {
			return -1;
		}
		if (c == '.') {
			break;
		}
		buffer[pos++] = (char) c;
	}
	for (int i = 0; i < 3; ++i) {
		buffer[pos++] = (char) getchar();
	}
	for (int i = 7; i > 0; --i) {
		ret <<= 1;
		ret |= (buffer[pos - i] == 'o') ? 1 : 0;
	}
	return ret;
}

int main() {
	int read;
	while ((read = read_next()) >= 0) {
		putchar((char) read);
	}
	return 0;
}
