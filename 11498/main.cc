#include <cstdio>

using namespace std;

int main() {
	int k, n, m, x, y;
	while (scanf("%d\n", &k) == 1 && k > 0) {
		scanf("%d %d\n", &n, &m);
		while (k--) {
			scanf("%d %d\n", &x, &y);
			if (x == n || y == m) {
				puts("divisa");
			} else if (x > n && y > m) {
				/* Northeastern NE */
				puts("NE");
			} else if (x < n && y < m) {
				/* Southwestern SO */
				puts("SO");
			} else if (x < n && y > m) {
				/* Northwestern NO */
				puts("NO");
			} else if (x > n && y < m) {
				/* Southeastern SE */
				puts("SE");
			}
		}
	}
	return 0;
}
