#include <cstdio>

using namespace std;

static const int map[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // NUL -> SI
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // DLE -> US
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // SPACE -> /
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0 -> ?
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // @ -> O
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // P -> _
    0, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, // ` -> o
    1, 2, 3, 4, 1, 2, 3, 1, 2, 3, 4, 0, 0, 0, 0, 0, // p -> DEL
};

static char input[300];

int main() {
	int _C;
	fgets(input, sizeof(input), stdin);
	sscanf(input, "%d", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int sum = 0;
		int c;
		while ((c = getchar()) != EOF && c != '\n') {
			sum += map[c];
		}
		printf("Case #%d: %d\n", the_case, sum);
	}
	return 0;
}
