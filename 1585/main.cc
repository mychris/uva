#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static char input[200];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int sum = 0;
		int to_add = 1;
		char *c = &input[0];
		fgets(c, 200, stdin);
		if (*c == '\0') {
			break;
		}
		while (*c) {
			switch (*c) {
			case 'O':
				sum += to_add;
				++to_add;
				break;
			case 'X':
				to_add = 1;
				break;
			}
			++c;
		}
		printf("%d\n", sum);
	}
	return 0;
}
