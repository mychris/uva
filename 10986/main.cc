#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <climits>
#include <cstdio>
#include <queue>
#include <vector>

using namespace std;

static int nodes;
static int network[20001][20001];
static vector<int> neighbours[20001];
static int cost[20001];

static int solve(const int start, const int target) {
	fill(cost, cost + nodes, INT_MAX);
	queue<int> queue;
	queue.push(start);
	cost[start] = 0;
	while (!queue.empty()) {
		int node = queue.front();
		int nodeCost = cost[node];
		queue.pop();
		for (int neighbour : neighbours[node]) {
			int totalCost = nodeCost + network[node][neighbour];
			if (totalCost < cost[neighbour]) {
				cost[neighbour] = totalCost;
				if (neighbour != target && totalCost < cost[target]) {
					queue.push(neighbour);
				}
			}
		}
	}
	return (cost[target] != INT_MAX) ? cost[target] : -1;
}

int main() {
	int N, n, m, S, T;
	scanf("%d", &N);
	for (int testCase = 1; testCase <= N; ++testCase) {
		scanf("%d %d %d %d", &n, &m, &S, &T);
		nodes = n;
		for (int i = 0; i < n; ++i) {
			fill(network[i], network[i] + nodes, INT_MAX);
			neighbours[i].clear();
		}
		while (m--) {
			int from, to, weight;
			scanf("%d %d %d", &from, &to, &weight);
			if (weight < network[from][to]) {
				neighbours[from].push_back(to);
				network[from][to] = weight;
				neighbours[to].push_back(from);
				network[to][from] = weight;
			}
		}
		int result = solve(S, T);
		if (result < 0) {
			printf("Case #%d: unreachable\n", testCase);
		} else {
			printf("Case #%d: %d\n", testCase, result);
		}
	}
	return 0;
}
