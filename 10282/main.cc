#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

#include <map>
#include <string>

using namespace std;

static char input_buffer[4096];
static size_t input_buffer_length;

static char *line = &input_buffer[0];
static size_t line_length;

static map<string, string> dict;

bool read_line() {
	memmove(&input_buffer[0],
	        &input_buffer[line_length],
	        input_buffer_length - line_length);
	input_buffer_length -= line_length;
	line_length = 0;
	while (line_length == 0) {
		for (size_t i = 0; i < input_buffer_length; ++i) {
			if (input_buffer[i] == '\n') {
				input_buffer[i] = '\0';
				line_length = i + 1;
				return true;
			}
		}
		size_t read = fread(input_buffer + input_buffer_length,
		                    sizeof(char),
		                    sizeof(input_buffer) - input_buffer_length,
		                    stdin);
		if (read == 0) {
			return false;
		}
		input_buffer_length += read;
	}
	return false;
}

int main() {
	// read dictionary
	while (read_line()) {
		char *english = &line[0];
		char *foreign = nullptr;
		if (line[0] == '\0') {
			break;
		}
		for (size_t i = 0; i < line_length; ++i) {
			if (line[i] == ' ') {
				foreign = &line[i + 1];
				line[i] = '\0';
				break;
			}
		}
		dict.insert({string(foreign), string(english)});
	}
	// translate
	while (read_line()) {
		if (line[0] == '\0') {
			break;
		}
		auto translated = dict.find(string(line));
		if (translated == dict.end()) {
			puts("eh");
		} else {
			printf("%s\n", translated->second.c_str());
		}
	}
	return 0;
}
