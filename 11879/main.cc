#include <iostream>
#include <string>

using namespace std;

static inline bool div_by_17(const string &n) {
	long long s = 0;
	for (char c : n) {
		s = s * 10 + (c - '0');
		s %= 17;
	}
	return s % 17 == 0;
}

int main() {
	for (string line; getline(cin, line);) {
		if (line.size() == 1 && line[0] == '0') {
			break;
		}
		if (div_by_17(line)) {
			puts("1");
		} else {
			puts("0");
		}
	}
	return 0;
}
