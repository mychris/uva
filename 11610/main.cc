#include <algorithm>
#include <cmath>
#include <cstdio>
#include <vector>

using namespace std;

static int sieve[1000006];
static vector<int> reverse_primes;
static vector<int> factors;
static int factors_sum[1000006];

static void calc_sieve(void) {
	int upper_bound = 1000005;
	int upper_bound_square_root = (int) sqrt(upper_bound);
	for (int m = 2; m <= upper_bound_square_root; m++) {
		if (!sieve[m]) {
			for (int k = m * m; k <= upper_bound; k += m) {
				sieve[k] = m;
			}
		}
	}
}

static int count_prime_factors(int x) {
	int counter = 0;
	while (!(x % 2)) {
		counter++;
		x /= 2;
	}

	while (!(x % 3)) {
		counter++;
		x /= 3;
	}

	while (!(x % 5)) {
		counter++;
		x /= 5;
	}

	while (sieve[x]) {
		counter++;
		x /= sieve[x];
	}

	if (x != 1) {
		counter++;
	}
	return counter;
}

static void get_factors(void) {
	for (int rp : reverse_primes) {
		factors.push_back(count_prime_factors(rp));
	}
}

static int reverse(int x) {
	int tmp = 0;
	while (x > 0) {
		tmp = tmp * 10 + x % 10;
		x /= 10;
	}
	return tmp;
}

static void get_reverse_primes(void) {
	for (int i = 1000010; i < 10000000; i += 10) {
		int rev = reverse(i);
		if (rev < 1000000 && !sieve[rev])
			reverse_primes.push_back(i);
	}
	sort(reverse_primes.begin(), reverse_primes.end());
}

static void sum(int start_index) {
	if (!(start_index)) {
		factors_sum[0] = factors[0];
		start_index = 1;
	}
	size_t factorsSize = factors.size();
	for (size_t i = (size_t) start_index; i < factorsSize; i++) {
		factors_sum[i] = factors_sum[i - 1] + factors[i];
	}
}

static int bin_search(const vector<int> &arr, int key) {
	int left = 0, right = (int) arr.size() - 1, mid;

	while (left <= right) {
		mid = (int) ((left + right) / 2);
		if (key == arr[(size_t) mid]) {
			return mid;
		} else if (key > arr[(size_t) mid]) {
			left = mid + 1;
		} else {
			right = mid - 1;
		}
	}

	return -1;
}

static void del(int x) {
	int key = bin_search(reverse_primes, x);
	if (key < 0) {
		return;
	}

	reverse_primes.erase(reverse_primes.begin() + key);
	factors.erase(factors.begin() + key);
	sum(key);
}

int main() {
	calc_sieve();
	get_reverse_primes();
	get_factors();
	sum(0);

	char s[30];
	int x;
	while (scanf("%s %d", s, &x) == 2) {
		if (s[0] == 'q') {
			printf("%d\n", factors_sum[x]);
		} else {
			del(x);
		}
	}
}
