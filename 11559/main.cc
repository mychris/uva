#include <climits>
#include <cstdio>

using namespace std;

int main() {
	int N, B, H, W, i;
	while (scanf("%d %d %d %d\n", &N, &B, &H, &W) != EOF) {
		int result = INT_MAX;
		while (H--) {
			int P;
			scanf("%d\n", &P);
			for (i = 0; i < W; ++i) {
				int a;
				scanf("%d", &a);
				if (a >= N && P * N < result) {
					result = P * N;
				}
			}
		}
		if (result > B) {
			puts("stay home");
		} else {
			printf("%d\n", result);
		}
	}
	return 0;
}
