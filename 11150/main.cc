#include <cstdio>

using namespace std;

static int cache[256];

// https://oeis.org/A032766
static int calc(int x) {
	return (3 * x + 2) / 2 - 1;
}

static void init() {
	for (int x = 1; x < 256; ++x) {
		cache[x] = calc(x);
	}
}

int main() {
	init();
	int N;
	while (scanf("%d\n", &N) == 1) {
		if (N < 0) {
			puts("0");
		} else if (N <= 255) {
			printf("%d\n", cache[N]);
		} else {
			printf("%d\n", calc(N));
		}
	}
	return 0;
}
