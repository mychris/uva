#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

int main() {
	int t;
	string line1, line2;
	int index;
	string value;
	vector<pair<int, string>> store(128);
	cin >> t;
	getchar();
	while (t--) {
		store.clear();
		getchar();
		getline(cin, line1);
		getline(cin, line2);
		stringstream ss1(line1), ss2(line2);
		while (ss1 >> index && ss2 >> value)
			store.push_back(pair<int, string>(index, value));
		sort(store.begin(), store.end());
		for (auto elem : store)
			cout << elem.second << endl;
		if (t)
			cout << endl;
	}
	return 0;
}
