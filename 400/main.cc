#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

#include <algorithm>
#include <string>
#include <vector>

using namespace std;

char input_buffer[128];

int main() {
	int n;
	while (scanf("%d", &n) == 1 && n > 0) {
		puts("------------------------------------------------------------");
		vector<string> v;
		size_t longest = 0;
		while (n--) {
			scanf("%100s", input_buffer);
			string s(input_buffer);
			if (s.length() > longest) {
				longest = s.length();
			}
			v.push_back(move(s));
		}
		const size_t num_columns = (60 - longest) / (longest + 2) + 1;
		const size_t num_rows =
		    v.size() / num_columns + ((v.size() % num_columns) ? 1 : 0);
		sort(v.begin(), v.end());
		for (size_t row = 0; row < num_rows; ++row) {
			printf("%-*s", static_cast<int>(longest), v[row].c_str());
			for (size_t col = 1; col < num_columns; ++col) {
				size_t idx = col * num_rows + row;
				if (idx >= v.size())
					break;
				printf("  %-*s",
				       static_cast<int>(longest),
				       v[col * num_rows + row].c_str());
			}
			puts("");
		}
	}
	return 0;
}
