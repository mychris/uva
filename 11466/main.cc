#include <bitset>
#include <cmath>
#include <cstdio>
#include <vector>

using namespace std;

#define UPPER_BOUND 9999999

static vector<unsigned int> primes;

static void sieve(void) {
	unsigned int last = 0;

	unsigned int upper_bound_square_root = (unsigned int) sqrt(UPPER_BOUND);
	bitset<UPPER_BOUND + 1> is_composite;

	for (unsigned int m = 2; m <= upper_bound_square_root; ++m) {
		if (!is_composite[m]) {
			primes.push_back(m);
			last = m;
			for (unsigned int k = m * m; k <= UPPER_BOUND; k += m) {
				is_composite[k] = true;
				is_composite.set(k, true);
			}
		}
	}

	if (upper_bound_square_root == last) {
		upper_bound_square_root++;
	}

	for (unsigned int m = upper_bound_square_root; m <= UPPER_BOUND; ++m) {
		if (!is_composite[m]) {
			primes.push_back(m);
		}
	}
}

static long factor(long n) {
	long max = 0;
	int cnt = 0;
	for (unsigned p : primes) {
		if (n % p == 0) {
			max = p;
			++cnt;
			while (n % p == 0) {
				n /= p;
			}
		}
		if (p > n) {
			break;
		}
	}
	if (n > 1) {
		if (n > max) {
			max = n;
		}
		++cnt;
	}
	return (cnt == 1) ? -1 : max;
}

int main() {
	sieve();
	long n;
	while (scanf("%ld\n", &n) == 1 && n != 0) {
		if (n == 1 || n == -1) {
			puts("-1");
		} else {
			printf("%ld\n", factor(abs(n)));
		}
	}
	return 0;
}
