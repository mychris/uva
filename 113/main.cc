#include <cmath>
#include <cstdio>

using namespace std;

/**
 * k**n      = p
 * ln(k**n)  = ln(p)
 * n * ln(k) = ln(p)
 * ln(k)     = ln(p) / n
 * k         = e**(ln(p) / n)
 */

int main() {
	double n, p;
	while (scanf("%lf\n%lf\n", &n, &p) == 2 && n > 0.0 && p > 0.0) {
		printf("%.0lf\n", exp(log(p) / n));
	}
	return 0;
}
