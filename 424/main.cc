#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cctype>
#include <cstdio>
#include <cstring>

using namespace std;

static char number[512];
static char cur[256];
static size_t cur_len;

static void add(void) {
	int number_ptr = sizeof(number) - 1;
	int cur_ptr = (int) strlen(cur) - 1;
	int carry = 0;
	while (cur_ptr >= 0 || carry) {
		if (number[number_ptr] == '\0') {
			number[number_ptr] = '0';
		}
		if (cur_ptr >= 0) {
			number[number_ptr] = (char) (number[number_ptr] + (cur[cur_ptr] - '0'));
		}
		number[number_ptr] = (char) (number[number_ptr] + carry);
		if (number[number_ptr] > '9') {
			carry = 1;
			number[number_ptr] = (char) (number[number_ptr] - 10);
		} else {
			carry = 0;
		}
		--cur_ptr;
		--number_ptr;
	}
}

static void print(void) {
	size_t ptr = sizeof(number) - 1;
	while (number[ptr] != '\0') {
		--ptr;
	}
	while (++ptr < sizeof(number)) {
		putchar(number[ptr]);
	}
	putchar('\n');
}

int main() {
	number[sizeof(number) - 1] = '0';
	int _C = 200;
	while (fgets(cur, sizeof(cur), stdin) != nullptr && _C--) {
		if (cur[0] == '\0' || (cur[0] == '0' && cur[1] == '\0')) {
			break;
		}
		cur_len = strlen(cur);
		while (!isdigit(cur[cur_len - 1])) {
			cur[cur_len - 1] = '\0';
			--cur_len;
		}
		add();
	}
	print();
	return 0;
}
