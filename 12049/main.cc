#include <algorithm>
#include <cstdio>
#include <vector>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		size_t len1, len2;
		scanf("%zu %zu\n", &len1, &len2);
		vector<int> l1(len1);
		vector<int> l2(len2);
		for (size_t i = 0; i < len1; ++i) {
			scanf("%d", &l1[i]);
		}
		for (size_t i = 0; i < len2; ++i) {
			scanf("%d", &l2[i]);
		}
		sort(l1.begin(), l1.end());
		sort(l2.begin(), l2.end());
		size_t p1 = 0, p2 = 0;
		size_t cnt = 0;
		while (p1 < len1 && p2 < len2) {
			if (l1[p1] == l2[p2]) {
				++p1;
				++p2;
			} else if (l1[p1] > l2[p2]) {
				++cnt;
				++p2;
			} else {
				++cnt;
				++p1;
			}
		}
		if (p1 != len1) {
			cnt += len1 - p1;
		}
		if (p2 != len2) {
			cnt += len2 - p2;
		}
		printf("%zu\n", cnt);
	}
}
