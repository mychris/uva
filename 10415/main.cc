#include <cstdio>
#include <cstring>
#include <iostream>

using namespace std;

static const int needed[256][10] = {
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},

    {1, 1, 1, 0, 0, 0, 0, 0, 0, 0}, // A
    {1, 1, 0, 0, 0, 0, 0, 0, 0, 0}, // B
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0}, // C
    {1, 1, 1, 1, 0, 0, 1, 1, 1, 0}, // D
    {1, 1, 1, 1, 0, 0, 1, 1, 0, 0}, // E
    {1, 1, 1, 1, 0, 0, 1, 0, 0, 0}, // F
    {1, 1, 1, 1, 0, 0, 0, 0, 0, 0}, // G
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {0, 1, 1, 0, 0, 0, 0, 0, 0, 0}, // a
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0}, // b
    {0, 1, 1, 1, 0, 0, 1, 1, 1, 1}, // c
    {0, 1, 1, 1, 0, 0, 1, 1, 1, 0}, // d
    {0, 1, 1, 1, 0, 0, 1, 1, 0, 0}, // e
    {0, 1, 1, 1, 0, 0, 1, 0, 0, 0}, // f
    {0, 1, 1, 1, 0, 0, 0, 0, 0, 0}, // g
};

static int pressed[10];
static int count[10];

static char input[256];

int main() {
	int _C;
	scanf("%d", &_C);
	getchar();
	while (_C--) {
		memset(count, 0, sizeof(count));
		memset(pressed, 0, sizeof(pressed));
		fgets(input, sizeof(input), stdin);
		char *c = &input[0];
		while (*c) {
			size_t note = (size_t) *c;
			for (int finger = 0; finger < 10; ++finger) {
				if (needed[note][finger]) {
					if (!pressed[finger]) {
						++count[finger];
					}
					pressed[finger] = 1;
				} else {
					pressed[finger] = 0;
				}
			}
			++c;
		}
		for (int i = 0; i < 10; ++i) {
			if (i) {
				putchar(' ');
			}
			printf("%d", count[i]);
		}
		putchar('\n');
	}
	return 0;
}
