#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int set[256];

static void merge(int a, int b, int max) {
	int x = set[b];
	for (int i = 'A'; i <= max; ++i) {
		if (set[i] == x) {
			set[i] = set[a];
		}
	}
}

int main() {
	int _N, max;
	if (scanf("%d\n", &_N) != 1) {
		return 2;
	}
	while ((max = getchar()) > 0 && (max == ' ' || max == '\n' || max == '\t')) {
	}
	if (max) {
		ungetc(max, stdin);
	}
	while (_N--) {
		int a, b, subgraphs;
		max = getchar();
		assert(max >= 'A' && max <= 'Z');
		for (int i = 'A'; i <= max; ++i) {
			set[i] = i;
		}
		subgraphs = max - 'A' + 1;
		a = getchar();
		assert(a == '\n');
		for (;;) {
			a = getchar();
			if (a == '\n' || a <= 0) {
				break;
			}
			b = getchar();
			assert(a >= 'A' && a <= max);
			assert(b >= 'A' && b <= max);
			if (set[a] != set[b]) {
				merge(a, b, max);
				--subgraphs;
			}
			a = getchar();
			assert(a == '\n');
		}
		printf("%d\n", subgraphs);
		if (_N) {
			puts("");
		}
	}
	return 0;
}
