#include <cstdio>
#include <cstring>

using namespace std;

static inline int abs(const int a) {
	return (a < 0) ? -a : a;
}

static char found[5000];

int main() {
	int n, i;
	while (scanf("%d", &n) != EOF && n > 0) {
		int is_jj = 1;
		memset(found, 0, sizeof(found));
		int last, cur, diff;
		scanf("%d", &last);
		for (i = 1; i < n; ++i) {
			scanf("%d", &cur);
			diff = abs(cur - last);
			if (diff <= 0 || diff >= n || found[diff]) {
				is_jj = 0;
			}
			if (diff > 0 && diff < n) {
				found[diff] = 1;
			}
			last = cur;
		}
		puts((is_jj) ? "Jolly" : "Not jolly");
	}
	return 0;
}
