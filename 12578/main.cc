#include <cmath>
#include <cstdio>

using namespace std;

int main() {
	const double PI = acos(-1);
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		double L;
		while (scanf("%lf", &L) == 1) {
			double width = L * 6.0 / 10.0;
			double radius = L / 5.0;

			double circle_area = PI * radius * radius;
			double rectangle_area = L * width;

			printf("%.2f %.2f\n", circle_area, rectangle_area - circle_area);
		}
	}
	return 0;
}
