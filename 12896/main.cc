#include <cstdio>

using namespace std;

static char map[10][4] = {
    {' ', '#', '#', '#'},
    {'.', ',', '?', '"'},
    {'a', 'b', 'c', '#'},
    {'d', 'e', 'f', '#'},
    {'g', 'h', 'i', '#'},
    {'j', 'k', 'l', '#'},
    {'m', 'n', 'o', '#'},
    {'p', 'q', 'r', 's'},
    {'t', 'u', 'v', '#'},
    {'w', 'x', 'y', 'z'},
};

static int num[200];

int main() {
	int _C, i;
	scanf("%d\n", &_C);
	while (_C--) {
		int n;
		scanf("%d\n", &n);
		for (i = 0; i < n; ++i) {
			scanf("%d", &num[i]);
		}
		for (i = 0; i < n; ++i) {
			int pressed;
			scanf("%d", &pressed);
			putchar(map[num[i]][pressed - 1]);
		}
		putchar('\n');
	}
	return 0;
}
