#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char in[150][150];

int main() {
	size_t longest = 0;
	int line = 0;
	while (fgets(in[line], sizeof(in[line]), stdin) != NULL) {
		size_t length = strlen(in[line]) - 1;
		if (length > longest) {
			longest = length;
		}
		if (length == 0) {
			continue;
		}
		in[line][length] = '\0';
		++line;
	}
	for (size_t i = 0; i < longest; ++i) {
		for (int x = line - 1; x >= 0; --x) {
			if (!in[x][i]) {
				putchar(' ');
			} else {
				putchar(in[x][i]);
			}
		}
		putchar('\n');
	}
	return 0;
}
