#include <cstdio>
#include <list>

using namespace std;

int main() {
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		list<int> cards;
		for (int i = 1; i <= n; ++i) {
			cards.push_back(i);
		}
		printf("Discarded cards:");
		while (cards.size() > 1) {
			if (cards.size() == 2) {
				printf(" %d", cards.front());
			} else {
				printf(" %d,", cards.front());
			}
			cards.pop_front();
			int h = cards.front();
			cards.pop_front();
			cards.push_back(h);
		}
		printf("\nRemaining card: %d\n", cards.front());
	}
}
