#include <cmath>
#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <bitset>
#include <cassert>
#include <climits>
#include <cstdint>
#include <cstdio>
#include <limits>
#include <vector>

using namespace std;

static size_t isqrt_size_t(size_t x) {
	size_t m, y, b;
	m = ((size_t) 1u) << ((sizeof(size_t) * CHAR_BIT) - 2u);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

static bitset<1000001> prime_sieve() {
	bitset<1000001> nums;
	nums.set();
	nums.set(0, false);
	nums.set(1, false);
	for (size_t i = 2; i <= isqrt_size_t(1000001) + 1; ++i) {
		if (nums[i]) {
			for (size_t j = i * i; j <= 1000000; j += i)
				nums.set(j, false);
		}
	}
	return nums;
}

static bitset<1000001> prime_table;
static int prime_list[50000] = {};

int main() {
	prime_table = prime_sieve();
	int *prime_ptr = prime_list;
	for (int i = 2; i <= (1000000 / 2); ++i) {
		if (prime_table[(size_t) i]) {
			*prime_ptr = i;
			++prime_ptr;
		}
	}
	int n = 0;
	while (scanf("%d", &n) == 1 && n > 0) {
		for (prime_ptr = prime_list;; ++prime_ptr) {
			int a = *prime_ptr;
			int b = n - a;
			if (prime_table[(size_t) b]) {
				printf("%d = %d + %d\n", n, a, b);
				break;
			}
		}
	}
	return 0;
}
