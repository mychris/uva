#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int queue[32];

static inline void p(int x) {
	if (x < 10) {
		putchar(' ');
		putchar(' ');
		putchar('0' + x);
	} else if (x < 100) {
		putchar(' ');
		putchar('0' + (x / 10));
		putchar('0' + (x % 10));
	}
}

static void run(const int N, const int k, const int m) {
	for (int i = 0; i < N; ++i) {
		queue[i] = i + 1;
	}
	int kp = N - 1, mp = 0;
	for (int remaining = N;;) {
		for (int ki = 0; ki < k;) {
			kp = (kp + 1) % N;
			ki += static_cast<int>(queue[kp] != 0);
		}
		for (int mi = 0; mi < m;) {
			mp = (mp + N - 1) % N;
			mi += static_cast<int>(queue[mp] != 0);
		}
		if (kp == mp) {
			p(queue[kp]);
			queue[kp] = 0;
			remaining -= 1;
		} else {
			p(queue[kp]);
			p(queue[mp]);
			queue[kp] = 0;
			queue[mp] = 0;
			remaining -= 2;
		}
		if (remaining) {
			putchar(',');
		} else {
			putchar('\n');
			return;
		}
	}
}

int main() {
	int N, k, m;
	while (scanf("%d %d %d\n", &N, &k, &m) == 3 && (N > 0 || k > 0 || m > 0)) {
		run(N, k, m);
	}
	return 0;
}
