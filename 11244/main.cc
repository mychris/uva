#include <cstdio>
#include <cstring>

using namespace std;

static char sky[105][105];
static int r = 0;
static int c = 0;

static int check(int row, int col) {
	if (sky[row][col] == '.') {
		return 0;
	}
	if (sky[row - 1][col - 1] == '*') {
		return 0;
	}
	if (sky[row - 1][col] == '*') {
		return 0;
	}
	if (sky[row - 1][col + 1] == '*') {
		return 0;
	}
	if (sky[row][col - 1] == '*') {
		return 0;
	}
	if (sky[row][col + 1] == '*') {
		return 0;
	}
	if (sky[row + 1][col - 1] == '*') {
		return 0;
	}
	if (sky[row + 1][col] == '*') {
		return 0;
	}
	if (sky[row + 1][col + 1] == '*') {
		return 0;
	}
	return 1;
}

int main() {
	int i, j;
	while (scanf("%d %d\n", &r, &c) == 2 && r > 0 && c > 0) {
		memset(sky, ' ', sizeof(sky));
		for (i = 1; i <= r; ++i) {
			for (j = 1; j <= c; ++j) {
				int ch;
				do {
					ch = getchar();
				} while (ch != '.' && ch != '*');
				sky[i][j] = (char) ch;
			}
		}
		int cnt = 0;
		for (i = 1; i <= r; ++i) {
			for (j = 1; j <= c; ++j) {
				if (check(i, j)) {
					++cnt;
				}
			}
		}
		printf("%d\n", cnt);
	}
	return 0;
}
