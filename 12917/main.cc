#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	size_t p, h, o;
	while (scanf("%zu %zu %zu\n", &p, &h, &o) == 3) {
		if (o < p || o - p < h) {
			puts("Hunters win!");
		} else {
			puts("Props win!");
		}
	}
	return 0;
}
