#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <vector>

using namespace std;

vector<int> longest_increasing_subsequence(const vector<int> &input) {
	vector<int> output;
	vector<int> p(input.size());
	int u, v;

	if (input.empty()) {
		return output;
	}

	output.push_back(0);
	for (size_t i = 1; i < input.size(); ++i) {
		if (input[(size_t) output.back()] < input[i]) {
			p[i] = output.back();
			output.push_back((int) i);
			continue;
		}

		u = 0;
		v = (int) output.size() - 1;
		while (u < v) {
			int c = (u + v) / 2;
			if (input[(size_t) output[(size_t) c]] < input[i]) {
				u = c + 1;
			} else {
				v = c;
			}
		}

		if (input[i] < input[(size_t) output[(size_t) u]]) {
			if (u > 0) {
				p[i] = output[(size_t) (u - 1)];
			}
			output[(size_t) u] = (int) i;
		}
	}

	v = output.back();
	for (u = (int) output.size() - 1; u >= 0; --u) {
		output[(size_t) u] = input[(size_t) v];
		v = p[(size_t) v];
	}
	return output;
}

int main() {
	vector<int> input;
	int n;
	while (scanf("%d\n", &n) == 1) {
		input.push_back(n);
	}
	vector<int> output = longest_increasing_subsequence(input);
	printf("%zu\n-\n", output.size());
	for (int o : output) {
		printf("%d\n", o);
	}
	return 0;
}
