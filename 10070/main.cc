#include <iostream>
#include <string>

using namespace std;

static inline bool div_by_4(const string &n) {
	size_t len = n.size();
	if (len <= 1) {
		return (n[len - 1] - '0') % 4 == 0;
	}
	return ((n[len - 2] - '0') * 10 + (n[len - 1] - '0')) % 4 == 0;
}

static inline bool div_by_100(const string &n) {
	size_t len = n.size();
	if (len < 2) {
		return false;
	}
	return n[len - 2] == '0' && n[len - 1] == '0';
}

static inline bool div_by_5(const string &n) {
	size_t len = n.size();
	if (len <= 0) {
		return false;
	}
	return n[len - 1] == '0' || n[len - 1] == '5';
}

static inline bool div_by_3(const string &n) {
	unsigned long long sum = 0;
	for (char c : n) {
		sum += (unsigned long long) ((unsigned char) c - (unsigned char) '0');
	}
	return sum % 3LLU == 0LLU;
}

static inline bool div_by_11(const string &n) {
	long long sum = 0LL;
	long long sign = 1LL;
	for (char c : n) {
		sum += ((long long) (c - '0')) * sign;
		sign *= -1LL;
	}
	return sum % 11LL == 0LL;
}

static inline bool div_by_15(const string &n) {
	return div_by_5(n) && div_by_3(n);
}

static inline bool div_by_400(const string &n) {
	long long s = 0;
	for (char c : n) {
		s = s * 10 + (c - '0');
		s %= 400;
	}
	return s % 400 == 0;
}

static inline bool div_by_55(const string &n) {
	return div_by_5(n) && div_by_11(n);
}

static inline bool is_leap(const string &year) {
	return (div_by_4(year) && !div_by_100(year)) || div_by_400(year);
}

static inline bool is_huluculu(const string &year) {
	return div_by_15(year);
}

static inline bool is_bulukulu(const string &year) {
	return is_leap(year) && div_by_55(year);
}

int main() {
	string year;
	if (!(cin >> year)) {
		return 0;
	}
	for (;;) {
		bool ordinary = true;
		if (is_leap(year)) {
			puts("This is leap year.");
			ordinary = false;
		}
		if (is_huluculu(year)) {
			puts("This is huluculu festival year.");
			ordinary = false;
		}
		if (is_bulukulu(year)) {
			puts("This is bulukulu festival year.");
			ordinary = false;
		}
		if (ordinary) {
			puts("This is an ordinary year.");
		}
		if (!(cin >> year)) {
			break;
		}
		putchar('\n');
	}
	return 0;
}
