#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static char input[1024];
static double masses[128];

static int read_quantity(char *in) {
	int q = 0;
	while (*in >= '0' && *in <= '9') {
		q *= 10;
		q += *in - '0';
		++in;
	}
	return (q) ? q : 1;
}

int main() {
	masses[(size_t) 'C'] = 12.01;
	masses[(size_t) 'H'] = 1.008;
	masses[(size_t) 'O'] = 16.00;
	masses[(size_t) 'N'] = 14.01;

	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		double mass = 0.0;
		if (fgets(input, sizeof(input), stdin) == nullptr) {
			break;
		}
		char *in = input;
		while (*in) {
			char elem = *in++;
			if (masses[(size_t) elem] <= 0.0) {
				continue;
			}
			mass += masses[(size_t) elem] * read_quantity(in);
		}
		printf("%.3lf\n", mass);
	}
	return 0;
}
