#include <cstdio>

using namespace std;

static long long result[3];

static void extended_euclid(long long p, long long q) {
	if (q == 0) {
		result[0] = p;
		result[1] = 1;
		result[2] = 0;
		return;
	}
	extended_euclid(q, p % q);
	long long a = result[2];
	long long b = result[1] - (p / q) * a;

	result[1] = a;
	result[2] = b;
	return;
}

int main() {
	long long a, b;
	while (scanf("%lld %lld\n", &a, &b) == 2) {
		extended_euclid(a, b);
		printf("%lld %lld %lld\n", result[1], result[2], result[0]);
	}
	return 0;
}
