#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int m[32];

int main() {
	int T;
	scanf("%d\n", &T);
	while (T--) {
		int N = 0, G = 0, price = 0, weight = 0, result = 0, cap = 0;
		memset(m, 0, sizeof(m));
		scanf("%d\n", &N);
		while (N--) {
			scanf("%d %d\n", &price, &weight);
			for (int i = 30; i >= weight; i--) {
				if (m[i] < m[i - weight] + price)
					m[i] = m[i - weight] + price;
			}
		}

		scanf("%d\n", &G);
		while (G--) {
			scanf("%d\n", &cap);
			result += m[cap];
		}

		printf("%d\n", result);
	}
	return 0;
}
