#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static long map[1000000];

static long nextFib(long k) {
	if (k % 2 == 0) {
		return k / 2;
	}
	return 3 * k + 1;
}

static long cycle(long k) {
	if (map[k] != 0) {
		return map[k];
	}

	long counter = 0, test = k;

	while (k > 1) {
		k = nextFib(k);
		if (k < 1000000 && map[k] != 0) {
			counter += map[k];
			k = -1;
		} else {
			++counter;
		}
	}

	++counter;
	map[test] = counter;
	return counter;
}

int main() {
	int i, j, k, first, second;
	long longest, cur;

	while (scanf("%d %d", &first, &second) == 2) {
		i = first;
		j = second;

		if (i > j) {
			i = second;
			j = first;
		}

		if (i < 1 || j < 1 || i >= 1000000 || j >= 1000000) {
			continue;
		}

		longest = 0;
		for (k = i; k <= j; k++) {
			cur = cycle(k);
			if (cur > longest)
				longest = cur;
		}

		printf("%d %d %ld\n", first, second, longest);
	}
	return 0;
}
