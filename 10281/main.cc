#include <iostream>
#include <string>

using namespace std;

static const int secs_in_hour = 60 * 60;
static const int secs_in_min = 60;

int main() {
	double trav = 0.0;
	int last_sec = 0;
	double speed_km_sec = 0.0;
	for (string line; getline(cin, line);) {
		if (line.empty()) {
			continue;
		}
		int h, m, s, amount;
		int n = sscanf(line.c_str(), "%d:%d:%d %d", &h, &m, &s, &amount);
		int trav_sec = (h * secs_in_hour + m * secs_in_min + s) - last_sec;
		trav += speed_km_sec * trav_sec;
		switch (n) {
		case 3:
			printf("%s %.2lf km\n", line.c_str(), trav);
			break;
		case 4:
			speed_km_sec = amount / (double) secs_in_hour;
			break;
		default:
			puts("invalid input");
			return 1;
		}
		last_sec += trav_sec;
	}
	return 0;
}
