#include <cmath>
#include <cstdio>

using namespace std;

static const double PI = 3.141592653589793;

int main() {
	double d, v;
	while (scanf("%lf %lf\n", &d, &v) == 2 && d > 0.0 && v > 0.0) {
		double tmp = (d * d * d * (PI * 2.0 / 3.0) - 4 * v) / (PI * 2.0 / 3.0);
		double result = pow(tmp, 1.0 / 3.0);
		printf("%.3lf\n", result);
	}
}
