#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char input[100];

int main() {
	while (scanf("%s", &input[0]) != EOF) {
		input[99] = '\0';
		if (strcmp("#", input) == 0) {
			break;
		}
		size_t const len = strlen(input);
		if (next_permutation(&input[0], &input[len])) {
			puts(input);
		} else {
			puts("No Successor");
		}
		memset(input, 0, sizeof(input));
	}
	return 0;
}
