#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

#include <vector>

using namespace std;

int results[1024];

static void next(vector<int> &v, const int x) {
	int sum = 0;
	int carry = 0;
	for (auto it = v.begin(); it < v.end(); ++it) {
		int res = carry + *it * x;
		*it = res % 10;
		carry = res / 10;
		sum += *it;
	}
	while (carry != 0) {
		int a = carry % 10;
		v.push_back(a);
		sum += a;
		carry /= 10;
	}
	results[x] = sum;
}

int main() {
	{
		vector<int> n;
		n.push_back(1);
		for (int i = 1; i <= 1000; ++i)
			next(n, i);
		results[0] = 1;
	}
	int n;
	while (scanf("%d", &n) == 1 && n >= 0) {
		printf("%d\n", results[n]);
	}
	return 0;
}
