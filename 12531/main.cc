#include <cstdio>

using namespace std;

// angle between two positions is 1.2 (360 / (60 * 5))
// multiply by 10, check for divisibility by 12
int main() {
	int angle;
	while (scanf("%d\n", &angle) == 1) {
		angle *= 10;
		if (angle % 12 == 0) {
			puts("Y");
		} else {
			puts("N");
		}
	}
	return 0;
}
