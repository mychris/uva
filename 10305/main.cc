#include <cstdio>
#include <cstring>

using namespace std;

static int n_tasks;
static int done[102];
static int pred[102][102];
static int n_pred[102];

static int result[102];
static int ptr_result;

int main() {
	int n, m, i, j;
	while (scanf("%d %d\n", &n, &m) == 2) {
		if (n == 0 && m == 0) {
			break;
		}
		memset(done, 0, sizeof(done));
		memset(n_pred, 0, sizeof(n_pred));
		n_tasks = n;
		while (m--) {
			int cur, bef;
			scanf("%d %d\n", &bef, &cur);
			pred[cur][n_pred[cur]++] = bef;
		}

		ptr_result = 0;
		for (i = n_tasks; i > 0; --i) {
			if (n_pred[i] == 0) {
				done[i] = 1;
				result[ptr_result++] = i;
			}
		}
		while (ptr_result < n_tasks) {
			for (i = n_tasks; i > 0; --i) {
				if (done[i]) {
					continue;
				}
				int exec = 1;
				for (j = 0; j < n_pred[i]; ++j) {
					if (!done[pred[i][j]]) {
						exec = 0;
						break;
					}
				}
				if (exec) {
					done[i] = 1;
					result[ptr_result++] = i;
					break;
				}
			}
		}

		for (i = 0; i < n_tasks - 1; ++i) {
			printf("%d ", result[i]);
		}
		printf("%d\n", result[n_tasks - 1]);
	}
	return 0;
}
