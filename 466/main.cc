#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char in[16][16];
static char out[16][16];
static char tmp[16][16];
static size_t size;

static void skip_ws(void) {
	int c = getchar();
	while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
		c = getchar();
	}
	ungetc(c, stdin);
}

static int check_end(void) {
	int c = getchar();
	while (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
		c = getchar();
	}
	ungetc(c, stdin);
	return c == EOF;
}

static int is_preserved(void) {
	for (size_t row = 0; row < size; ++row) {
		for (size_t col = 0; col < size; ++col) {
			if (in[row][col] != out[row][col]) {
				return 0;
			}
		}
	}
	return 1;
}

static int is_rotated(void) {
	int rotated90 = 1;
	int rotated180 = 1;
	int rotated270 = 1;
	for (size_t row = 0; row < size; ++row) {
		for (size_t col = 0; col < size; ++col) {
			if (in[row][col] != out[col][size - 1 - row])
				rotated90 = 0;
			if (in[row][col] != out[size - row - 1][size - col - 1])
				rotated180 = 0;
			if (in[row][col] != out[size - 1 - col][row])
				rotated270 = 0;
		}
		if (!rotated90 && !rotated180 && !rotated270) {
			return -1;
		}
	}
	if (rotated90) {
		return 90;
	}
	if (rotated180) {
		return 180;
	}
	if (rotated270) {
		return 270;
	}
	return -1;
}

static int is_reflected(void) {
	for (size_t row = 0; row < size; row++) {
		for (size_t col = 0; col < size; col++) {
			if (in[row][col] != out[size - 1 - row][col])
				return 0;
		}
	}
	return 1;
}

static void do_reflect(void) {
	for (size_t row = 0; row < size; ++row) {
		memmove(tmp[size - 1 - row], in[row], size);
	}
	memmove(in, tmp, 16 * 16);
}

int main() {
	int _C = 0;
	while (!check_end()) {
		_C++;
		skip_ws();
		scanf("%zu\n", &size);
		if (size > 10) {
			fprintf(stderr, "Invalid size %zu\n", size);
			return 1;
		}
		for (size_t i = 0; i < size; ++i) {
			skip_ws();
			for (size_t j = 0; j < size; ++j) {
				in[i][j] = (char) getchar();
			}
			skip_ws();
			for (size_t j = 0; j < size; ++j) {
				out[i][j] = (char) getchar();
			}
		}
		if (is_preserved()) {
			printf("Pattern %d was preserved.\n", _C);
			continue;
		}

		int rotated = is_rotated();
		if (rotated >= 0) {
			printf("Pattern %d was rotated %d degrees.\n", _C, rotated);
			continue;
		}

		if (is_reflected()) {
			printf("Pattern %d was reflected vertically.\n", _C);
			continue;
		}

		do_reflect();
		rotated = is_rotated();
		if (rotated >= 0) {
			printf("Pattern %d was reflected vertically and rotated %d degrees.\n",
			       _C,
			       rotated);
			continue;
		}

		printf("Pattern %d was improperly transformed.\n", _C);
	}
	return 0;
}
