#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <bitset>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <vector>

using namespace std;

static bitset<1000001> prime_set;
static vector<unsigned int> primes;

static void sieve(void) {
	unsigned int max = 1000000;
	unsigned int root = (unsigned int) sqrt(1000001);
	prime_set.set(1, true);
	prime_set.set(0, true);
	for (unsigned int m = 2; m <= root; m++) {
		if (!prime_set[m]) {
			for (unsigned int k = m * m; k <= max; k += m) {
				prime_set.set(k, true);
			}
		}
	}

	for (unsigned int m = 2; m <= max; m++) {
		if (!prime_set[m]) {
			primes.push_back(m);
		}
	}
}

int main() {
	sieve();
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		unsigned int from, to;
		scanf("%u %u\n", &from, &to);

		if (from > to) {
			swap(from, to);
		}

		unsigned int from_prime = from;
		for (unsigned int i = from; i <= to; i++) {
			if (!prime_set[i]) {
				from_prime = i;
				break;
			}
		}

		unsigned int to_prime = to;
		for (unsigned int i = to; i >= from; i--) {
			if (!prime_set[i]) {
				to_prime = i;
				break;
			}
		}

		if (prime_set[to_prime] || from_prime == to_prime) {
			puts("No jumping champion");
			continue;
		}

		auto start = lower_bound(primes.begin(), primes.end(), from_prime);
		auto end = lower_bound(primes.begin(), primes.end(), to_prime);
		unsigned int *divs =
		    (unsigned int *) calloc(*end - *start + 1, sizeof(unsigned int));

		bool champ = false;
		unsigned int maxDivs = 0;
		unsigned int index = 0;

		auto end_idx = end - primes.begin();
		for (auto i = start - primes.begin(); i < end_idx; ++i) {
			unsigned int div = primes[(size_t) (i + 1)] - primes[(size_t) i];
			divs[div]++;
			if (maxDivs < divs[div]) {
				maxDivs = divs[div];
				index = div;
				champ = true;
			} else if (maxDivs == divs[div]) {
				champ = false;
			}
		}

		if (champ) {
			printf("The jumping champion is %u\n", index);
		} else {
			puts("No jumping champion");
		}

		free(divs);
	}
}
