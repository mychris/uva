#include <algorithm>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

static int matrix[128][128];
static int tmp[128];
static size_t N;

static int max_in_arr(void) {
	int m = tmp[0];
	for (size_t i = 1; i < N; ++i) {
		m = max(m, tmp[i]);
	}
	return m;
}

static int max_subarray_sum() {
	int max_so_far = 0;
	int max_ending_here = 0;
	bool all_neg = true;
	for (size_t i = 0; i < N; ++i) {
		max_ending_here = max_ending_here + tmp[i];
		if (max_ending_here < 0) {
			max_ending_here = 0;
		}
		max_so_far = max(max_so_far, max_ending_here);
		if (tmp[i] > 0) {
			all_neg = false;
		}
	}

	return (all_neg) ? max_in_arr() : max_so_far;
}

static int run(void) {
	int max_sum = INT_MIN;
	for (size_t left = 0; left < N; left++) {
		memset(tmp, 0, sizeof(int) * N);

		for (size_t right = left; right < N; right++) {
			for (size_t i = 0; i < N; ++i) {
				tmp[i] += matrix[i][right];
			}
			max_sum = max(max_subarray_sum(), max_sum);
		}
	}
	return max_sum;
}

int main() {
	while (scanf("%zu", &N) == 1) {
		for (size_t i = 0; i < N; ++i) {
			for (size_t j = 0; j < N; ++j) {
				scanf("%d", &matrix[i][j]);
			}
		}
		printf("%d\n", run());
	}
	return 0;
}
