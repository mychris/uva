#include <algorithm>
#include <cmath>
#include <cstdio>
#include <functional>

using namespace std;

static unsigned long long lands[128];

static unsigned long long pow(unsigned long long x, size_t y) {
	unsigned long long ret = 1;
	while (y--) {
		ret *= x;
	}
	return ret;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		size_t land_key = 0;
		for (;;) {
			unsigned long long land_price;
			if (scanf("%llu\n", &land_price) != 1 || land_price <= 0) {
				break;
			}
			lands[land_key++] = land_price;
		}
		sort(lands, lands + land_key, greater<unsigned long long>());
		unsigned long long amount = 0;
		for (size_t key = 0; key < land_key; ++key) {
			amount += 2 * pow(lands[key], key + 1);
			if (amount > 5000000LLU) {
				break;
			}
		}
		if (amount > 5000000LLU) {
			puts("Too expensive");
		} else {
			printf("%llu\n", amount);
		}
	}
	return 0;
}
