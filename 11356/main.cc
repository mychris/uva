#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

static char month_name[128]; // also includes day... scanf...
static int day;
static int month;
static int year;

static const char *month_names[] = {
    "",
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
};

static void finish_parse(void) {
	if (month_name[0] == 'J') {
		if (month_name[1] == 'a') {
			month = 1;
			sscanf(month_name + 8, "%d", &day);
		}
		if (month_name[1] == 'u' && month_name[2] == 'n') {
			month = 6;
			sscanf(month_name + 5, "%d", &day);
		}
		if (month_name[1] == 'u' && month_name[2] == 'l') {
			month = 7;
			sscanf(month_name + 5, "%d", &day);
		}
	}
	if (month_name[0] == 'F') {
		month = 2;
		sscanf(month_name + 9, "%d", &day);
	}
	if (month_name[0] == 'M') {
		if (month_name[1] == 'a' && month_name[2] == 'r') {
			month = 3;
			sscanf(month_name + 6, "%d", &day);
		}
		if (month_name[1] == 'a' && month_name[2] == 'y') {
			month = 5;
			sscanf(month_name + 4, "%d", &day);
		}
	}
	if (month_name[0] == 'A') {
		if (month_name[1] == 'p') {
			month = 4;
			sscanf(month_name + 6, "%d", &day);
		}
		if (month_name[1] == 'u') {
			month = 8;
			sscanf(month_name + 7, "%d", &day);
		}
	}
	if (month_name[0] == 'S') {
		month = 9;
		sscanf(month_name + 10, "%d", &day);
	}
	if (month_name[0] == 'O') {
		month = 10;
		sscanf(month_name + 8, "%d", &day);
	}
	if (month_name[0] == 'N') {
		month = 11;
		sscanf(month_name + 9, "%d", &day);
	}
	if (month_name[0] == 'D') {
		month = 12;
		sscanf(month_name + 9, "%d", &day);
	}
}

static inline bool is_leap_year(const int y) {
	return ((y & 3) == 0 && ((y % 25) != 0 || (y & 15) == 0));
}

static int days_in_year(const int y) {
	return (is_leap_year(y)) ? 366 : 365;
}

static int days_in_month(int m, int y) {
	switch (m) {
	case 1:
		return 31;
	case 2:
		return (is_leap_year(y)) ? 29 : 28;
	case 3:
		return 31;
	case 4:
		return 30;
	case 5:
		return 31;
	case 6:
		return 30;
	case 7:
		return 31;
	case 8:
		return 31;
	case 9:
		return 30;
	case 10:
		return 31;
	case 11:
		return 30;
	case 12:
		return 31;
	default:
		fprintf(stderr, "invalid month %d\n", m);
		exit(127);
	}
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		scanf("%d-%s\n", &year, month_name);
		finish_parse();
		int to_add;
		scanf("%d\n", &to_add);
		day += to_add;
		if (day > days_in_year(year)) {
			while (month > 2) {
				day -= days_in_month(month, year);
				++month;
				if (month > 12) {
					month = 1;
					++year;
				}
			}
		}
		while (day > days_in_year(year)) {
			day -= days_in_year(year);
			++year;
		}
		while (day > days_in_month(month, year)) {
			day -= days_in_month(month, year);
			++month;
			if (month > 12) {
				month = 1;
				++year;
			}
		}
		printf("Case %d: %d-%s-%02d\n", the_case, year, month_names[month], day);
	}
	return 0;
}
