#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

int main() {
	for (string line; getline(cin, line);) {
		while (!line.empty()) {
			auto end = line.find_first_of(' ');
			if (end == string::npos) {
				for (auto it = line.rbegin(); it < line.rend(); ++it) {
					cout << *it;
				}
				break;
			} else {
				for (auto it = line.begin() + (ssize_t) end - 1; it >= line.begin();
				     --it) {
					cout << *it;
				}
				line.erase(0, end + 1);
			}
			cout << ' ';
		}
		cout << endl;
	}
	return 0;
}
