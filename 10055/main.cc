#include <cstdio>

using namespace std;

int main() {
	unsigned long long a, b;
	while (scanf("%llu %llu\n", &a, &b) == 2) {
		printf("%llu\n", (a > b) ? (a - b) : (b - a));
	}
	return 0;
}
