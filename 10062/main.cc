#include <cctype>
#include <climits>
#include <cstdio>
#include <cstring>

using namespace std;

static char input[1024];
static int freq[256];

static int min_freq(void) {
	int min = INT_MAX;
	for (size_t i = 0; i < 256; ++i) {
		if (freq[i] < min && freq[i] != 0) {
			min = freq[i];
		}
	}
	return (min == INT_MAX) ? 0 : min;
}

int main() {
	if (fgets(input, sizeof(input), stdin) == nullptr) {
		return 0;
	}
	for (;;) {
		for (char c : input) {
			if (c == '\0') {
				break;
			}
			if (c == '\n') {
				continue;
			}
			freq[(size_t) c] += 1;
		}
		int last_freq = min_freq();
		if (last_freq == 0) {
		} else {
			int next_freq = INT_MAX;
			while (last_freq != INT_MAX) {
				next_freq = INT_MAX;
				for (int i = 255; i >= 0; --i) {
					if (freq[i] == last_freq) {
						printf("%d %d\n", i, freq[i]);
						freq[i] = 0;
					} else if (freq[i] > 0 && freq[i] < next_freq) {
						next_freq = freq[i];
					}
				}
				last_freq = next_freq;
			}
		}
		if (fgets(input, sizeof(input), stdin) == nullptr) {
			break;
		}
		putchar('\n');
	}
	return 0;
}
