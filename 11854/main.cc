#include <cstdio>

using namespace std;

int main() {
	int a, b, c;
	while (scanf("%d %d %d", &a, &b, &c) == 3) {
		if (a == 0 && b == 0 && c == 0) {
			break;
		}

		if (c < a) {
			int tmp = a;
			a = c;
			c = tmp;
		}

		if (c < b) {
			int tmp = b;
			b = c;
			c = tmp;
		}

		if ((a * a) + (b * b) == (c * c)) {
			puts("right");
		} else {
			puts("wrong");
		}
	}
	return 0;
}
