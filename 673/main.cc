#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <iostream>
#include <stack>
#include <string>

using namespace std;

static inline bool is_open(char const p) {
	return p == '(' || p == '[' || p == '{';
}

static inline char get_match(char const p) {
	switch (p) {
	case '{':
		return '}';
	case '}':
		return '{';
	case '[':
		return ']';
	case ']':
		return '[';
	case '(':
		return ')';
	case ')':
		return '(';
	default:
		return '\0';
	}
}

static bool process(string const &line) {
	stack<char> stack;
	for (string::const_iterator it = line.begin(); it < line.end(); ++it) {
		if (is_open(*it)) {
			stack.push(*it);
		} else {
			if (!stack.empty() && stack.top() == get_match(*it)) {
				stack.pop();
			} else {
				return false;
			}
		}
	}
	return stack.empty();
}

int main() {
	string line;
	getline(cin, line);
	int t = stoi(line);
	while (t-- && getline(cin, line)) {
		puts(process(line) ? "Yes" : "No");
	}
}
