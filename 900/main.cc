#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

#define MAX 50

// Fibonacci Sequence
static unsigned long long cache[MAX + 1];

int main() {
	cache[0] = 0;
	cache[1] = 1;
	cache[2] = 2;
	cache[3] = 3;
	for (size_t i = 4; i <= MAX; ++i) {
		cache[i] = cache[i - 1] + cache[i - 2];
	}
	size_t length;
	while (scanf("%zu\n", &length) == 1 && length > 0) {
		if (length > MAX) {
			continue;
		}
		printf("%llu\n", cache[length]);
	}
	return 0;
}
