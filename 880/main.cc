#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>

using namespace std;

int main() {
	long long n;
	while (scanf("%lld\n", &n) == 1) {
		long long col_n = (long long) ceil((sqrt(1 + (8 * n)) - 1) / 2);
		long long max = (col_n * col_n + col_n) / 2;
		long long den = col_n - (max - n);
		long long num = (max - n) + 1;
		printf("%lld/%lld\n", num, den);
	}
	return 0;
}
