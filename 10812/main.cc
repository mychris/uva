#include <cstdio>

using namespace std;

/*  x + y = s
 *  x - y = d
 * -2 * y = d - s
 *      y = (d - s) / (-2)
 *      x = d + y
 */

int main() {
	long long _C = 0;
	scanf("%lld\n", &_C);
	while (_C--) {
		long long s, d, y;
		scanf("%lld %lld\n", &s, &d);
		if (d - s > 0 || (d & 1) != (s & 1)) {
			puts("impossible");
		} else {
			y = (d - s) / (-2);
			printf("%lld %lld\n", d + y, y);
		}
	}
	return 0;
}
