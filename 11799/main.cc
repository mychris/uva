#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int N, max = 0, speed;
		scanf("%d", &N);
		while (N--) {
			scanf("%d", &speed);
			if (speed > max) {
				max = speed;
			}
		}
		printf("Case %d: %d\n", the_case, max);
	}
	return 0;
}
