#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <limits>

using namespace std;

#define BOARD(__x, __y) board[(__x) + ((__y) << 3)]
static int board[8 * 8];
#define QUEUED(__x, __y) queued[(__x) + ((__y) << 3)]
static int queued[8 * 8];

static int delta_x[] = {-2, -2, -1, -1, +1, +1, +2, +2};
static int delta_y[] = {-1, +1, -2, +2, -2, +2, -1, +1};

static int jump(int x, int y) {
	assert(x >= 0 && x < 8);
	assert(y >= 0 && y < 8);
	assert(sizeof(delta_x) == sizeof(delta_y));
	assert(sizeof(delta_x[0]) == sizeof(delta_y[0]));
	int any_queued = 0;
	int moves = BOARD(x, y) + 1;
	QUEUED(x, y) = 0;
	for (size_t delta = 0; delta < sizeof(delta_x) / sizeof(delta_x[0]); ++delta) {
		int m_x = x + delta_x[delta];
		int m_y = y + delta_y[delta];
		if (m_x < 0 || m_x >= 8 || m_y < 0 || m_y >= 8) {
			continue;
		}
		if (BOARD(m_x, m_y) > moves) {
			any_queued = 1;
			QUEUED(m_x, m_y) = 1;
			BOARD(m_x, m_y) = moves;
		}
	}
	return any_queued;
}

static int solve(int f_x, int f_y, int t_x, int t_y) {
	memset(&queued[0], 0, sizeof(queued));
	for (int x = 0; x < 8; ++x) {
		for (int y = 0; y < 8; ++y) {
			BOARD(x, y) = numeric_limits<int>::max();
		}
	}
	BOARD(f_x, f_y) = 0;
	QUEUED(f_x, f_y) = 1;
	int any_queued = 1;
	while (any_queued) {
		any_queued = 0;
		for (int x = 0; x < 8; ++x) {
			for (int y = 0; y < 8; ++y) {
				if (QUEUED(x, y) > 0) {
					any_queued |= jump(x, y);
				}
			}
		}
	}
	return BOARD(t_x, t_y);
}

int main() {
	char from_s[4], to_s[4];
	while (scanf("%2s %2s\n", &from_s[0], &to_s[0]) == 2) {
		int from_x = static_cast<int>(from_s[0] - 'a');
		int from_y = static_cast<int>(from_s[1] - '1');
		int to_x = static_cast<int>(to_s[0] - 'a');
		int to_y = static_cast<int>(to_s[1] - '1');
		int s = solve(from_x, from_y, to_x, to_y);
		printf("To get from %s to %s takes %d knight moves.\n", from_s, to_s, s);
	}
	return 0;
}
