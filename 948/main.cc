#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cstdio>
#include <vector>

using namespace std;

static vector<long long> fibs;

static void init(void) {
	fibs.push_back(0);
	fibs.push_back(1);
	fibs.push_back(1);
	long long fib1 = 1;
	long long fib2 = 2;
	while (fib2 < 100000000LL) {
		fibs.push_back(fib2);
		long long tmp = fib2;
		fib2 = fib1 + tmp;
		fib1 = tmp;
	}
}

static long long calc(long long n) {
	long long result = 0;
	for (size_t x = fibs.size() - 1; x > 0; --x) {
		if (fibs[x] <= n) {
			n -= fibs[x];
			result |= 1LL << (x - 2);
		}
	}
	return result;
}

static void print_binary(unsigned long long x) {
	unsigned long long highest = 1LLU << (sizeof(long long) * CHAR_BIT - 1);
	while (highest && !(x & highest)) {
		highest >>= 1;
	}
	if (!highest) {
		putchar('0');
	} else {
		while (highest) {
			putchar('0' + ((x & highest) != 0));
			highest >>= 1;
		}
	}
}

int main() {
	init();
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		long long n, result;
		scanf("%lld\n", &n);
		result = calc(n);
		printf("%lld = ", n);
		print_binary((unsigned long long) result);
		puts(" (fib)");
	}
}
