#include <cstdio>

using namespace std;

int main() {
	int the_case = 0;
	int R, N;
	while (scanf("%d %d\n", &R, &N) == 2) {
		if (R <= 0 || N <= 0) {
			break;
		}
		++the_case;
		int result = (R - N) / N;
		if (R <= N) {
			result = 0;
		} else if (R % N > 0) {
			++result;
		}
		if (result > 26) {
			printf("Case %d: impossible\n", the_case);
		} else {
			printf("Case %d: %d\n", the_case, result);
		}
	}
	return 0;
}
