#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int n;
	while (scanf("%d", &n) == 1) {
		size_t result = 1;
		long long a = 1;
		while (a % n != 0) {
			a = (a * 10LL + 1LL) % n;
			++result;
		}
		printf("%zu\n", result);
	}
	return 0;
}
