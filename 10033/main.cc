#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

static int registers[10];
static int ram[1000];

int run(void) {
	int cnt = 0;
	int ptr = 0;
	for (;;) {
		++cnt;
		int a, b, c;
		int instr = ram[ptr++];
		a = instr / 100 % 10;
		b = instr / 10 % 10;
		c = instr % 10;
		if (a == 1) {
			break;
		} else if (a == 2) {
			registers[b] = c;
		} else if (a == 3) {
			registers[b] = (registers[b] + c) % 1000;
		} else if (a == 4) {
			registers[b] = (registers[b] * c) % 1000;
		} else if (a == 5) {
			registers[b] = registers[c];
		} else if (a == 6) {
			registers[b] = (registers[b] + registers[c]) % 1000;
		} else if (a == 7) {
			registers[b] = (registers[b] * registers[c]) % 1000;
		} else if (a == 8) {
			registers[b] = ram[registers[c]];
		} else if (a == 9) {
			ram[registers[c]] = registers[b];
		} else if (a == 0) {
			if (registers[c]) {
				ptr = registers[b];
			}
		}
	}
	return cnt;
}

int main() {
	int _C;
	scanf("%d\n\n", &_C);
	while (_C--) {
		memset(registers, 0, sizeof(registers));
		memset(ram, 0, sizeof(ram));
		int i = 0;
		for (string line; getline(cin, line);) {
			if (line.empty()) {
				break;
			}
			int instr = stoi(line);
			ram[i++] = instr % 1000;
		}
		printf("%d\n", run());
		if (_C) {
			putchar('\n');
		}
	}
	return 0;
}
