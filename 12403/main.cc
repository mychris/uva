#include <cstdio>
#include <cstring>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	unsigned long long sum = 0;
	while (_C--) {
		char action[10];
		scanf("%s", action);
		action[9] = '\0';
		if (strcmp(action, "donate") == 0) {
			unsigned long long amount;
			scanf("%llu\n", &amount);
			sum += amount;
		} else {
			printf("%llu\n", sum);
		}
	}
	return 0;
}
