#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

static void bubble(size_t from, size_t to, string &s) {
	assert(from < to);
	assert(to < s.size());
	assert(from < s.size());
	char to_char = s[to];
	copy(s.begin() + static_cast<string::difference_type>(to) + 1,
	     s.end(),
	     s.begin() + static_cast<string::difference_type>(to));
	copy(s.begin() + static_cast<string::difference_type>(from),
	     s.end() - 1,
	     s.begin() + static_cast<string::difference_type>(from) + 1);
	s[from] = to_char;
}

static string solve(const vector<string> &input) {
	bool already_seen[128] = {};
	string result{};
	if (input.size() == 0) {
		return result;
	}
	if (input.size() == 1) {
		for (char c : input[0]) {
			if (!already_seen[static_cast<size_t>(c)]) {
				already_seen[static_cast<size_t>(c)] = true;
				result += c;
			}
		}
		return result;
	}
	size_t c = 0;
	while (c < 20) {
		size_t before = 0, current = 0;
		while (before < input.size() && input[before].size() <= c) {
			before++;
		}
		if (before < input.size() &&
		    !already_seen[static_cast<size_t>(input[before][c])]) {
			already_seen[static_cast<size_t>(input[before][c])] = true;
			result += input[before][c];
		}
		while (before < input.size()) {
			current = before + 1;
			while (current < input.size() && input[current].size() <= c) {
				current++;
			}
			if (current >= input.size()) {
				break;
			}
			const char before_char = input[before][c];
			const char current_char = input[current][c];
			if (!already_seen[static_cast<size_t>(current_char)]) {
				already_seen[static_cast<size_t>(current_char)] = true;
				result += current_char;
			}
			if (before_char != current_char &&
			    strncmp(input[before].c_str(), input[current].c_str(), c) == 0) {
				const size_t before_pos = result.find(before_char);
				const size_t current_pos = result.find(current_char);
				if (before_pos > current_pos) {
					bubble(current_pos, before_pos, result);
					c = SIZE_MAX;
					break;
				}
			}
			before = current;
		}
		++c;
	}
	return result;
}

int main() {
	for (;;) {
		vector<string> input{};
		for (;;) {
			input.resize(input.size() + 1);
			if (!(cin >> input[input.size() - 1])) {
				return 0;
			}
			if ("#" == input[input.size() - 1]) {
				input.pop_back();
				break;
			}
		}
		cout << solve(input) << endl;
	}
	return 0;
}
