#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static char input[1024][1024];
static int m, n;

void clear(int row, int col) {
	if (row < 0 || col < 0 || row >= m || col >= n) {
		return;
	}
	if (input[row][col] != '@') {
		return;
	}
	input[row][col] = '*';

	clear(row - 1, col - 1);
	clear(row, col - 1);
	clear(row + 1, col - 1);

	clear(row - 1, col);
	clear(row + 1, col);

	clear(row - 1, col + 1);
	clear(row, col + 1);
	clear(row + 1, col + 1);
}

int main() {
	while (scanf("%d %d\n", &m, &n) == 2 && m > 0) {
		for (int i = 0; i < m; ++i) {
			scanf("%s\n", input[i]);
		}
		int cnt = 0;
		for (int row = 0; row < m; ++row) {
			for (int col = 0; col < n; ++col) {
				if (input[row][col] == '@') {
					++cnt;
					clear(row, col);
				}
			}
		}
		printf("%d\n", cnt);
	}
	return 0;
}
