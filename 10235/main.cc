#include <bitset>
#include <cmath>
#include <cstdio>

using namespace std;

static bitset<1000001> not_primes;

static void sieve() {
	unsigned int square_root = (unsigned int) sqrt(1000000);
	for (unsigned int m = 2; m <= square_root; m++) {
		if (!not_primes[m]) {
			for (unsigned int k = m * m; k <= 1000000; k += m) {
				not_primes[k] = true;
			}
		}
	}
}

static unsigned int reverse(unsigned int n) {
	unsigned int reversed = 0;
	while (n != 0) {
		reversed = reversed * 10 + n % 10;
		n /= 10;
	}
	return reversed;
}

int main() {
	sieve();
	unsigned int n;
	while (scanf("%u\n", &n) == 1) {
		if (n <= 1 || n >= 1000000) {
			continue;
		}
		if (!not_primes[n]) {
			unsigned int reversed = reverse(n);
			if (reversed != n && !not_primes[reversed]) {
				printf("%u is emirp.\n", n);
			} else {
				printf("%u is prime.\n", n);
			}
		} else {
			printf("%u is not prime.\n", n);
		}
	}
	return 0;
}
