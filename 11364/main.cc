#include <cstdio>
#include <cstdlib>

using namespace std;

static int nums[20];

static int cmp(const void *a, const void *b) {
	return *((const int *) a) - *((const int *) b);
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		unsigned int n;
		scanf("%u\n", &n);
		for (unsigned int i = 0; i < n; ++i) {
			scanf("%d", &nums[i]);
		}
		qsort(nums, n, sizeof(int), &cmp);
		int result = 0;
		for (unsigned int i = 1; i < n; ++i) {
			result += nums[i] - nums[i - 1];
		}
		result += nums[n - 1] - nums[0];
		printf("%d\n", result);
	}
	return 0;
}
