#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstdlib>

using namespace std;

struct Box {
	int width;
	int height;

	bool operator<(Box &other) {
		if (width == other.width) {
			return height < other.height;
		}
		return width < other.width;
	}
};

static inline Box parse(void) {
	int w, h;
	if (scanf("%d %d\n", &w, &h) != 2) {
		exit(0);
	}
	return {(w > h) ? w : h, (w > h) ? h : w};
}

static Box pallets_in[6];
static Box pallets_use[3];

static bool test(void) {
	int found = 0;
	// find pairs of pallets (opposite sites)
	for (int i = 0; i < 6; ++i) {
		if (pallets_in[i].width > 0) {
			for (int j = i + 1; j < 6; ++j) {
				if (pallets_in[i].width == pallets_in[j].width &&
				    pallets_in[i].height == pallets_in[j].height) {
					pallets_use[found].width = pallets_in[i].width;
					pallets_use[found].height = pallets_in[i].height;
					pallets_in[i].width = 0;
					pallets_in[j].width = 0;
					++found;
					break;
				}
			}
			if (found == 3) {
				break;
			}
		}
	}
	if (found != 3) {
		return false;
	}
	sort(pallets_use, pallets_use + 3);
	do {
		if (pallets_use[0].width == pallets_use[1].width &&
		    pallets_use[0].height == pallets_use[2].height &&
		    pallets_use[1].height == pallets_use[2].width) {
			return true;
		}
	} while (next_permutation(pallets_use, pallets_use + 3));
	return false;
}

int main() {
	for (;;) {
		pallets_in[0] = parse();
		pallets_in[1] = parse();
		pallets_in[2] = parse();
		pallets_in[3] = parse();
		pallets_in[4] = parse();
		pallets_in[5] = parse();
		puts((test()) ? "POSSIBLE" : "IMPOSSIBLE");
	}
	return 0;
}
