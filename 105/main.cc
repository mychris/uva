#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

struct building {
	unsigned int li;
	unsigned int hi;
	unsigned int ri;
};

static bool building_cmp(struct building const &a, struct building const &b) {
	return a.li < b.li;
}

static unsigned int height_at(vector<struct building> const &buildings,
                              unsigned int pos) {
	unsigned int ret = 0;
	for (auto it = buildings.begin(); it < buildings.end(); ++it) {
		if (it->li <= pos && it->ri > pos) {
			if (ret < it->hi) {
				ret = it->hi;
			}
		}
	}
	return ret;
}

void run(vector<struct building> const &buildings, vector<unsigned int> const &points) {
	printf("%u %u", buildings[0].li, buildings[0].hi);
	unsigned int last_height = buildings[0].hi;
	for (size_t i = 0; i < buildings.size() * 2; ++i) {
		unsigned int height = height_at(buildings, points[i]);
		if (height != last_height) {
			printf(" %d %u", points[i], height);
			last_height = height;
		}
	}
	putchar('\n');
}

int main() {
	unsigned int li, hi, ri;
	vector<unsigned int> points;
	points.reserve(10000);
	vector<struct building> buildings;
	buildings.reserve(5000);
	while (scanf("%u %u %u\n", &li, &hi, &ri) == 3) {
		struct building cur = {li, hi, ri};
		buildings.push_back(cur);
		points.push_back(li);
		points.push_back(ri);
	}
	sort(buildings.begin(), buildings.end(), &building_cmp);
	sort(points.begin(), points.end());
	run(buildings, points);
	return 0;
}
