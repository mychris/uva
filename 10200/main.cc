#include <cmath>
#include <cstdio>

using namespace std;

static int sieve[10001];

static bool is_prime(const int n) {
	if (n == 2 || n == 3) {
		return true;
	}

	if (n < 2 || n % 2 == 0) {
		return false;
	}

	if (n < 9) {
		return true;
	}

	if (n % 3 == 0) {
		return false;
	}

	int r = (int) sqrt(n);
	int f = 5;
	while (f <= r) {
		if (n % f == 0 || n % (f + 2) == 0) {
			return false;
		}
		f += 6;
	}
	return true;
}

static double calc(int a, int b) {
	int counter = sieve[b];
	if (a > 0) {
		counter -= sieve[a - 1];
	}
	return round((double) counter / (b - a + 1) * 100.0 * 100.0) / 100.0;
}

int main() {
	int counter = 0;
	for (int n = 0; n <= 10000; ++n) {
		if (is_prime(n * n + n + 41)) {
			++counter;
		}
		sieve[n] = counter;
	}
	int a, b;
	while (scanf("%d %d\n", &a, &b) == 2) {
		printf("%.2lf\n", calc(a, b));
	}

	return 0;
}
