#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <map>

using namespace std;

map<int, int> order{};
int s[21] = {};
int dp[21] = {};

int main() {
	int n, tmp;
	scanf("%d", &n);
	assert(n > 0);

	for (int i = 0; i < n; ++i) {
		scanf("%d", &tmp);
		order[i + 1] = tmp;
	}

	while (scanf("%d", &tmp) == 1) {
		s[tmp] = 1;
		for (int i = 2; i <= n; ++i) {
			scanf("%d", &tmp);
			s[tmp] = i;
		}

		for (int i = 1; i <= n; ++i)
			s[i] = order[s[i]];

		int maximum = 0;
		dp[1] = 1;
		for (int i = 2; i <= n; ++i) {
			dp[i] = 1;
			for (int j = 1; j < i; ++j) {
				if (s[i] > s[j])
					dp[i] = max(dp[i], dp[j] + 1);
			}
			if (dp[i] > maximum)
				maximum = dp[i];
		}
		printf("%d\n", maximum);
	}

	return 0;
}
