#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

#include <vector>

using namespace std;

int results[400][10] = {};

static void multiply_and_count_digits(vector<int> &v, const int x) {
	int carry = 0;
	for (auto it = v.begin(), end = v.end(); it < end; ++it) {
		int res = carry + *it * x;
		*it = res % 10;
		carry = res / 10;
		++results[x][*it];
	}
	while (carry != 0) {
		int res = carry % 10;
		v.push_back(res);
		carry /= 10;
		++results[x][res];
	}
}

int main() {
	{
		vector<int> v;
		v.push_back(1);
		results[0][1] = 1;
		for (int i = 1; i <= 366; ++i)
			multiply_and_count_digits(v, i);
	}
	int n;
	while (scanf("%d", &n) == 1 && n > 0) {
		printf("%d! --\n"
		       "   (0)%5d    (1)%5d    (2)%5d    (3)%5d    (4)%5d\n"
		       "   (5)%5d    (6)%5d    (7)%5d    (8)%5d    (9)%5d\n",
		       n,
		       results[n][0],
		       results[n][1],
		       results[n][2],
		       results[n][3],
		       results[n][4],
		       results[n][5],
		       results[n][6],
		       results[n][7],
		       results[n][8],
		       results[n][9]);
	}
	return 0;
}
