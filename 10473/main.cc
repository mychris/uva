#include <cstdio>

using namespace std;

static char input[1000];

int main() {
	while (fgets(input, 1000, stdin) != nullptr) {
		if (input[0] == '0' && input[1] == 'x') {
			unsigned int x;
			sscanf(input, "0x%X\n", &x);
			printf("%d\n", x);
		} else {
			int x;
			sscanf(input, "%d\n", &x);
			if (x < 0) {
				break;
			}
			printf("0x%X\n", x);
		}
	}
	return 0;
}
