#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cinttypes>
#include <cstdio>

using namespace std;

int main() {
	int32_t input, output;
	uint32_t uinput, uoutput;
	while (scanf("%" SCNd32, &input) == 1) {
		uinput = (uint32_t) input;
		uoutput = (uinput << 24) | ((uinput & 0xFF00) << 8) |
		          ((uinput & 0xFF0000) >> 8) | ((uinput >> 24) & 0xFF);
		output = (int32_t) uoutput;
		printf("%" PRId32 " converts to %" PRId32 "\n", input, output);
	}
	return 0;
}
