#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char input[10000];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		scanf("%s\n", input);
		size_t len = strlen(input);
		if ((input[0] == '1' && len == 1) || (input[0] == '4' && len == 1) ||
		    (input[0] == '7' && input[1] == '8' && len == 2)) {
			puts("+");
		} else if (input[len - 2] == '3' && input[len - 1] == '5') {
			puts("-");
		} else if (input[0] == '9' && input[len - 1] == '4') {
			puts("*");
		} else {
			puts("?");
		}
	}
	return 0;
}
