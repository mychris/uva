#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

#define LIMIT 7489

static size_t cache[LIMIT + 1];
static size_t coin_types[5] = {50, 25, 10, 5, 1};

static size_t
coin_change(size_t const *const arr, size_t const arr_len, size_t const n) {
	static size_t table[LIMIT + 1];
	memset(table, 0, sizeof(table));
	table[0] = 1;
	for (size_t i = 0; i < arr_len; ++i) {
		for (size_t j = arr[i]; j <= n; ++j) {
			table[j] += table[j - arr[i]];
		}
	}
	return table[n];
}

int main() {
	size_t m;
	while (scanf("%zu\n", &m) == 1) {
		assert(m <= LIMIT);
		if (!cache[m]) {
			cache[m] = coin_change(
			    &coin_types[0], sizeof(coin_types) / sizeof(coin_types[0]), m);
		}
		printf("%zu\n", cache[m]);
	}
	return 0;
}
