#include <cstdio>
#include <cstring>

using namespace std;

static char names[100][110];
static const char *words[16] = {
    "Happy",
    "birthday",
    "to",
    "you",
    "Happy",
    "birthday",
    "to",
    "you",
    "Happy",
    "birthday",
    "to",
    "Rujia",
    "Happy",
    "birthday",
    "to",
    "you",
};

int main() {
	int n, i, j, pos, word_pos;
	scanf("%d\n", &n);
	for (i = 0; i < n; ++i) {
		scanf("%s\n", names[i]);
	}
	pos = 0;
	word_pos = 0;
	for (i = 0; i < n; i += 16) {
		for (j = 0; j < 16; ++j) {
			printf("%s: %s\n", names[pos], words[word_pos]);
			pos = (pos + 1) % n;
			word_pos = (word_pos + 1) % 16;
		}
	}
	return 0;
}
