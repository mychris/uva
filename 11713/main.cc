#include <cstdio>
#include <cstring>

using namespace std;

static char first_arr[128];
static char second_arr[128];
static int vowels[128];

int main() {
	int _C;
	vowels[(size_t) 'a'] = vowels[(size_t) 'e'] = vowels[(size_t) 'i'] =
	    vowels[(size_t) 'o'] = vowels[(size_t) 'u'] = 1;
	scanf("%d\n", &_C);
	while (_C--) {
		char *first = first_arr;
		char *second = second_arr;
		scanf("%s\n%s\n", first, second);
		for (; *first && *second; ++first, ++second) {
			if (*first == *second) {
				continue;
			}
			if (!vowels[(size_t) *first] || !vowels[(size_t) *second]) {
				break;
			}
		}
		if (*first || *second) {
			puts("No");
		} else {
			puts("Yes");
		}
	}
	return 0;
}
