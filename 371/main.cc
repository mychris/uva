#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <limits>
#include <utility>

using namespace std;

int main() {
	long long a, b;
	while (scanf("%lld %lld", &a, &b) == 2 && a != 0 && b != 0) {
		if (a > b) {
			swap(a, b);
		}
		long long peak_value;
		size_t max_count = 0;
		for (long long cur = a; cur <= b; ++cur) {
			size_t cur_count = 0;
			long long n = cur;
			do {
				if ((n & 1) == 1) {
					n = 3 * n + 1;
				} else {
					n >>= 1;
				}
				++cur_count;
			} while (n > 1);
			if (max_count < cur_count) {
				peak_value = cur;
				max_count = cur_count;
			}
		}
		printf("Between %lld and %lld, %lld generates the longest sequence of %zu "
		       "values.\n",
		       a,
		       b,
		       peak_value,
		       max_count);
	}
	return 0;
}
