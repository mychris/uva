#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <map>
#include <vector>

using namespace std;

static unsigned long long const LIMIT = 200LLU * 200LLU * 200LLU;

static ssize_t binary_search(unsigned long long const *start,
                             size_t const nelems,
                             unsigned long long const target) {
	ssize_t l = 0;
	ssize_t r = (ssize_t) nelems - 1;
	while (l <= r) {
		ssize_t m = (l + r) / 2;
		if (start[m] < target) {
			l = m + 1;
		} else if (start[m] > target) {
			r = m - 1;
		} else {
			return m;
		}
	}
	return -1;
}

struct result {
	unsigned long long a, b, c, d;
};

static unsigned long long cubes[201];
static vector<result> results;

int main() {
	for (unsigned long long a = 0, ptr = 0; a <= 200; ++a, ++ptr) {
		cubes[ptr] = a * a * a;
	}
	for (unsigned long long b = 2; b < 200; ++b) {
		unsigned long long x = b * b * b;
		for (unsigned long long c = b; c < 200; ++c) {
			unsigned long long y = x + c * c * c;
			if (y > LIMIT) {
				break;
			}
			for (unsigned long long d = c; d < 200; ++d) {
				unsigned long long z = y + d * d * d;
				if (z > LIMIT) {
					break;
				}
				ssize_t idx =
				    binary_search(&cubes[0], sizeof(cubes) / sizeof(cubes[0]), z);
				if (idx >= 0) {
					results.push_back({static_cast<unsigned long long>(idx), b, c, d});
				}
			}
		}
	}

	sort(results.begin(), results.end(), [](result const &lhs, result const &rhs) {
		if (lhs.a == rhs.a) {
			if (lhs.b == rhs.b) {
				return lhs.c < rhs.c;
			}
			return lhs.b < rhs.b;
		}
		return lhs.a < rhs.a;
	});
	for (auto r : results) {
		printf("Cube = %llu, Triple = (%llu,%llu,%llu)\n", r.a, r.b, r.c, r.d);
	}
	return 0;
}
