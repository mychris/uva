#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

/* Create a grid which stores how many rats could be killed if
 * the bomb is placed on the corresponding place.
 * Iterate through the rat populations and for each population:
 *   - Iterate through all the places which would kill the population,
 *     if the bomb would be placed on that place.
 *     For each of these places:
 *       - Add the current rat population size to the place on the grid.
 * Iterate through the grid and find the place with the highest number.
 * Start the iteration form the lowest x and y coordinates.
 */

/* make the grid bigger, so the coordinates can be offset by the max
 * bomb strength and there does not need to be a check for the edges
 */
static unsigned int grid[1128][1128];

int main() {
	unsigned int TC;
	unsigned int strength;
	unsigned int rat_pops;
	if (scanf("%u", &TC) != 1) {
		return 1;
	}
	while (TC--) {
		if (scanf("%u %u", &strength, &rat_pops) != 2) {
			return 1;
		}
		strength = (strength > 50) ? 50 : strength;
		memset(&grid[0], 0, sizeof(grid));
		while (rat_pops--) {
			unsigned int x, y, size;
			unsigned int x_coord, y_coord;
			scanf("%u %u %u", &x, &y, &size);
			x += 50;
			y += 50;
			for (x_coord = x - strength; x_coord <= x + strength; ++x_coord) {
				for (y_coord = y - strength; y_coord <= y + strength; ++y_coord) {
					grid[x_coord][y_coord] += size;
				}
			}
		}
		unsigned int best = 0, best_x, best_y;
		unsigned int x_coord, y_coord;
		for (x_coord = 50; x_coord <= 1024 + 50; ++x_coord) {
			for (y_coord = 50; y_coord <= 1024 + 50; ++y_coord) {
				if (best < grid[x_coord][y_coord]) {
					best = grid[x_coord][y_coord];
					best_x = x_coord;
					best_y = y_coord;
				}
			}
		}
		printf("%u %u %u\n", best_x - 50, best_y - 50, best);
	}
	return 0;
}
