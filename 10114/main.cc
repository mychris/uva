#include <algorithm>
#include <cstdio>
#include <vector>

using namespace std;

int main() {
	int dur, records;
	double down_payment, loan;
	double dept, worth;
	while (scanf("%d %lf %lf %d\n", &dur, &down_payment, &loan, &records) == 4) {
		if (dur <= 0) {
			break;
		}
		dept = loan + (loan / dur);
		worth = loan + down_payment;

		vector<double> dep_records;
		dep_records.assign((size_t) dur + 1, 0.0);
		while (records--) {
			int cur_rec;
			double cur_dep;
			scanf("%d %lf\n", &cur_rec, &cur_dep);
			if (cur_rec < dur) {
				fill(dep_records.begin() + cur_rec, dep_records.end(), cur_dep);
			}
		}

		for (int month = 0; month <= dur; ++month) {
			dept -= loan / dur;
			worth -= worth * dep_records.at((size_t) month);
			if (worth > dept) {
				if (month == 1) {
					printf("%d month\n", month);
				} else {
					printf("%d months\n", month);
				}
				break;
			}
		}
	}
	return 0;
}
