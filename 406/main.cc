#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

#define MAX_N 1024

// Could be pre-calculated
static int SIEVE[MAX_N] = {};

// Could be pre-calculated
static unsigned int PRIME_COUNT[MAX_N] = {};

// Could be pre-calculated
static int PRIME_LIST_CACHE[MAX_N][MAX_N] = {};

// create the sieve array.
//   1 = not a prime number
//   0 = prime number
static void prepare_sieve(void) {
	SIEVE[0] = 1;
	SIEVE[1] = 0;
	for (size_t i = 2; i < ((sizeof(SIEVE) / sizeof(SIEVE[0])) >> 1); ++i) {
		if (SIEVE[i] == 0) {
			for (size_t j = i + i; j < (sizeof(SIEVE) / sizeof(SIEVE[0])); j += i) {
				SIEVE[j] = 1;
			}
		}
	}
}

static void prepare_prime_count(void) {
	unsigned int count = 0;
	for (size_t i = 0; i < sizeof(SIEVE) / sizeof(SIEVE[0]); ++i) {
		if (SIEVE[i] == 0) {
			++count;
		}
		PRIME_COUNT[i] = count;
	}
}

static int *get_prime_list(size_t n) {
	size_t prime_list_length = (size_t) PRIME_COUNT[n];
	int *prime_list = (int *) &PRIME_LIST_CACHE[n];
	if (prime_list[0] == 0) {
		size_t ptr = 0;
		size_t sieve_ptr = 1;
		while (ptr < prime_list_length) {
			if (SIEVE[sieve_ptr] == 0) {
				prime_list[ptr] = (int) sieve_ptr;
				++ptr;
			}
			++sieve_ptr;
		}
	}
	return prime_list;
}

int main() {
	static size_t N, C;
	prepare_sieve();
	prepare_prime_count();
	while (scanf("%zu %zu\n", &N, &C) == 2) {
		if (N > 1000) {
			return 1;
		}
		size_t prime_list_length = (size_t) PRIME_COUNT[N];
		int *prime_list = get_prime_list(N);
		printf("%zu %zu:", N, C);
		{
			// lower half
			ssize_t index = (ssize_t) (prime_list_length / 2 - C);
			// if the list has an odd length, skip one entry
			index += (((prime_list_length & 1) == 1) ? 1 : 0);
			// might be negative, if C is too big
			index = (index < 0) ? 0 : index;
			while (index < (ssize_t) prime_list_length / 2) {
				printf(" %d", prime_list[index]);
				++index;
			}
		}
		{
			// upper half
			size_t index = prime_list_length / 2;
			while (index < (prime_list_length / 2 + C) && index < prime_list_length) {
				printf(" %d", prime_list[index]);
				++index;
			}
		}
		printf("\n\n");
	}
	return 0;
}
