#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>

using namespace std;

static int w, h;
static char input[128][128];
static int results[4096];

// clears connected X fields
static void clear(int row, int col) {
	if (row < 0 || col < 0 || row >= h || col >= w || input[row][col] != 'X') {
		return;
	}
	input[row][col] = '*';

	clear(row, col - 1);
	clear(row - 1, col);
	clear(row + 1, col);
	clear(row, col + 1);
}

static int process(int row, int col, int cnt) {
	if (row < 0 || col < 0 || row >= h || col >= w) {
		return cnt;
	}
	if (input[row][col] == 'X') {
		++cnt;
		clear(row, col);
	}
	if (input[row][col] == '*') {
		input[row][col] = '.';

		cnt = process(row, col - 1, cnt);
		cnt = process(row - 1, col, cnt);
		cnt = process(row + 1, col, cnt);
		cnt = process(row, col + 1, cnt);
	}
	return cnt;
}

int main() {
	int T = 1;
	while (scanf("%d %d\n", &w, &h) == 2 && w > 0 && h > 0 && w <= 50 && h <= 50) {
		for (int i = 0; i < h; ++i) {
			scanf("%s\n", input[i]);
		}
		int result_ptr = 0;
		results[0] = 0;
		for (int row = 0; row < h; ++row) {
			for (int col = 0; col < w; ++col) {
				if (input[row][col] != '.') {
					results[result_ptr] = process(row, col, 0);
					if (results[result_ptr] > 0) {
						results[++result_ptr] = 0;
					}
				}
			}
		}
		if (result_ptr == 0) {
			printf("Throw %d\n\n\n", T++);
		} else {
			sort(results, results + result_ptr);
			printf("Throw %d\n", T++);
			for (int i = 0; i < result_ptr - 1; ++i) {
				printf("%d ", results[i]);
			}
			printf("%d\n\n", results[result_ptr - 1]);
		}
	}
	return 0;
}
