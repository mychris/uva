#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>

using namespace std;

int n_students;
int students[1024];
int sum;

static inline int solve() {
	int above = 0;
	int below = 0;
	int shareBelow = sum / n_students;
	int shareAbove = shareBelow + (sum % n_students ? 1 : 0);
	for (int i = 0; i < n_students; ++i) {
		if (students[i] > shareAbove) {
			above += students[i] - shareAbove;
		} else if (students[i] < shareBelow) {
			below += shareBelow - students[i];
		}
	}
	return max(above, below);
}

int main() {
	int S = 0;
	while ((scanf("%d\n", &S)) == 1 && S != 0) {
		n_students = S;
		sum = 0;
		while (S--) {
			int D, C;
			scanf("%d.%d\n", &D, &C);
			students[S] = D * 100 + C;
			sum += D * 100 + C;
		}
		int result = solve();
		printf("$%d.%02d\n", result / 100, result % 100);
	}
	return 0;
}
