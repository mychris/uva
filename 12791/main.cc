#include <cstdio>

using namespace std;

static int simulate(int fast, int slow) {
	double slow_incr = (double) fast / (double) slow;
	double fast_pos = 1.0, slow_pos = slow_incr;
	int round = 1;
	while (fast_pos - slow_pos < 1.0) {
		++round;
		fast_pos += 1;
		slow_pos += slow_incr;
	}
	return round;
}

int main() {
	int fast, slow;
	while (scanf("%d %d\n", &fast, &slow) == 2) {
		printf("%d\n", simulate(fast, slow));
	}
	return 0;
}
