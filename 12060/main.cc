#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int sum;
static int numbers[128];

static char a_str[128];
static int a_len;
static char b_str[128];
static int b_len;
static char c_str[128];
static int c_len;

static char const *spaces =
    "                                                                         ";
static char const *hyphens =
    "-------------------------------------------------------------------------";

static inline int abs(int const a) {
	return (a < 0) ? -a : a;
}

static inline int max(int const a, int const b) {
	return (a > b) ? a : b;
}

static int gcd(int a, int b) {
	int t;
	while (b != 0) {
		t = b;
		b = a % b;
		a = t;
	}
	return a;
}

static void print(int const sign, int const a, int const b, int const c) {
	static int test_case = 1;
	printf("Case %d:\n", test_case++);
	if (b == 0) {
		if (sign < 0) {
			printf("- %d\n", a);
		} else {
			printf("%d\n", a);
		}
	} else {
		if (a == 0) {
			if (sign < 0) {
				a_str[0] = '-';
				a_str[1] = ' ';
				a_str[2] = '\0';
				a_len = 2;
			} else {
				a_str[0] = '\0';
				a_len = 0;
			}
		} else {
			if (sign < 0) {
				a_len = sprintf(a_str, "- %d", a);
			} else {
				a_len = sprintf(a_str, "%d", a);
			}
		}
		b_len = sprintf(b_str, "%d", b);
		c_len = sprintf(c_str, "%d", c);
		printf("%.*s%s\n", a_len + max(c_len - b_len, 0), spaces, b_str);
		printf("%s%.*s\n", a_str, max(b_len, c_len), hyphens);
		printf("%.*s%s\n", a_len + max(b_len - c_len, 0), spaces, c_str);
	}
}

int main() {
	for (;;) {
		size_t n;
		scanf("%zu", &n);
		if (n <= 0) {
			break;
		}
		assert(n <= 101);
		sum = 0;
		for (size_t i = 0; i < n; ++i) {
			scanf("%d", &numbers[i]);
			assert(numbers[i] <= 10000 && numbers[i] >= -10000);
			sum += numbers[i];
		}
		int sign = (sum < 0) ? -1 : 1;
		int a, b, c;
		a = abs(sum / (int) n);
		b = abs(sum % (int) n);
		c = (int) n;
		if (b != 0) {
			int g = gcd(b, c);
			c /= g;
			b /= g;
		}
		print(sign, a, b, c);
	}
	return 0;
}
