#include <iostream>

using namespace std;

int main() {
	int needle, cnt, x;
	while (scanf("%d\n", &needle) == 1) {
		cnt = 0;
		for (int i = 0; i < 5; ++i) {
			scanf("%d", &x);
			if (x == needle) {
				++cnt;
			}
		}
		printf("%d\n", cnt);
	}
	return 0;
}
