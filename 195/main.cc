#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

bool cmp(char const a, char const b) {
	if (tolower(a) == tolower(b)) {
		return a < b;
	}
	return tolower(a) < tolower(b);
}

int main() {
	int n;
	scanf("%d\n", &n);
	while (n--) {
		string line;
		getline(cin, line);
		sort(line.begin(), line.end(), &cmp);
		do {
			puts(line.c_str());
		} while (next_permutation(line.begin(), line.end(), &cmp));
	}
	return 0;
}
