#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <vector>

using namespace std;

static vector<int> primes;

static bool is_prime(int n) {
	for (int i = 2; i < n; ++i) {
		if (n % i == 0) {
			return false;
		}
	}
	return n > 1;
}

static void init(void) {
	for (int i = 0; i <= 100; ++i) {
		if (is_prime(i)) {
			primes.push_back(i);
		}
	}
}

int main() {
	init();
	int n;
	while (scanf("%d", &n) == 1 && n > 0) {
		if (n >= 100) {
			printf("%d! =", n);
		} else {
			printf("% 3d! =", n);
		}
		int print_cnt = 0;
		for (int prime : primes) {
			if (prime > n) {
				break;
			}
			int cnt = 0;
			for (int i = 2; i <= n; ++i) {
				int x = i;
				while (x && x % prime == 0) {
					x /= prime;
					++cnt;
				}
			}
			if (print_cnt > 0 && print_cnt % 15 == 0) {
				printf("\n      ");
			}
			printf("% 3d", cnt);
			++print_cnt;
		}
		putchar('\n');
	}
	return 0;
}
