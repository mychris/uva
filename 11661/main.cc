#include <cstdio>
#include <cstring>

using namespace std;

static char input[2000005];

int main() {
	int L;
	while (scanf("%d\n", &L) == 1 && L > 0) {
		fgets(input, sizeof(input), stdin);
		size_t len = strlen(input);
		size_t last_start = 0;
		char last_thing = ' ';
		size_t smallest_distance = (size_t) L;
		for (size_t i = 0; i < len && smallest_distance; ++i) {
			char cur_thing = input[i];
			switch (cur_thing) {
			case '.':
				break;
			case 'R':
			case 'D':
				if ((cur_thing == 'R' && last_thing == 'D') ||
				    (cur_thing == 'D' && last_thing == 'R')) {
					size_t distance = i - last_start;
					if (distance > 0 && distance < smallest_distance) {
						smallest_distance = distance;
					}
				}
				last_thing = cur_thing;
				last_start = i;
				break;
			case 'Z':
				smallest_distance = 0;
				break;
			}
		}
		printf("%zu\n", smallest_distance);
	}
	return 0;
}
