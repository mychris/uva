#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char input[4097];
static size_t len;

static bool check(size_t period) {
	if (len % period != 0) {
		return false;
	}
	size_t fragments = len / period;
	size_t offset = period;
	while (--fragments) {
		for (size_t p = 0; p < period; ++p) {
			if (input[p] != input[p + offset]) {
				return false;
			}
		}
		offset += period;
	}
	return true;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		scanf("%s", input);
		len = strlen(input);
		size_t period = 1;
		for (period = 1; !check(period); ++period) {
		}
		printf("%zu\n", period);
		if (_C) {
			putchar('\n');
		}
	}
	return 0;
}
