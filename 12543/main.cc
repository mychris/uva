#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>

using namespace std;

static string longest;

static void skip_to_whitespace(void) {
	int c;
	while ((c = getchar()) != EOF) {
		if (isspace(c)) {
			ungetc(c, stdin);
			break;
		}
	}
}

int main() {
	int c;
	for (;;) {
		string cur;
		while ((c = getchar()) != EOF) {
			if (!isalpha(c) && c != '-') {
				break;
			}
			cur.push_back((char) c);
		}
		if (cur == "E-N-D") {
			break;
		}
		if (c == EOF) {
			break;
		}
		if (isspace(c) || ispunct(c)) {
			if (longest.length() < cur.length()) {
				longest = cur;
			}
		} else {
			skip_to_whitespace();
		}
	}
	transform(longest.begin(), longest.end(), longest.begin(), ::tolower);
	cout << longest << endl;
}
