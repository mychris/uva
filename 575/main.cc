#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <iostream>

using namespace std;

unsigned long long compute(string const &in) {
	unsigned long long ret = 0ULL;
	unsigned long long pow = 1ULL << ((int) in.size());
	for (auto it = in.begin(); it < in.end(); ++it) {
		ret += (unsigned long long) (*it - '0') * (pow - 1ULL);
		pow >>= 1ULL;
	}
	return ret;
}

int main() {
	for (string line; getline(cin, line);) {
		if (line == "0") {
			break;
		}
		printf("%llu\n", compute(line));
	}
	return 0;
}
