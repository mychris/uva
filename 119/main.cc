#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

int main() {
	unsigned int people;
	cin >> people;
	for (;;) {
		map<string, int> map;
		vector<string> people_vec(people);
		for (unsigned int i = 0; i < people; ++i) {
			string person;
			cin >> person;
			people_vec[i] = person;
			map[person] = 0;
		}
		for (unsigned int i = 0; i < people; ++i) {
			string giver;
			int amount;
			int to_how_many;
			cin >> giver >> amount >> to_how_many;
			if (to_how_many <= 0) {
				continue;
			}
			map[giver] -= (amount / to_how_many) * to_how_many;
			for (int j = 0; j < to_how_many; ++j) {
				string receiver;
				cin >> receiver;
				map[receiver] += amount / to_how_many;
			}
		}
		for (unsigned int i = 0; i < people; ++i) {
			cout << people_vec[i] << ' ' << map[people_vec[i]] << endl;
		}
		if (cin >> people) {
			cout << endl;
		} else {
			break;
		}
	}
	return 0;
}
