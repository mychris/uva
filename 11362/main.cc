#include <algorithm>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

class Node {
  public:
	Node() : last(false), childs(nullptr) {
	}

	~Node() {
		if (childs != nullptr) {
			delete[] childs;
		}
	}

	bool insert(const char *val) {
		if (!*val) {
			last = true;
			return true;
		}
		if (last) {
			return false;
		}
		if (childs == nullptr) {
			childs = new Node[10];
		}
		return childs[val[0] - '0'].insert(val + 1);
	}

	bool last;
	Node *childs;
};

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		Node root;
		unsigned int n;
		char in[128];
		scanf("%u\n", &n);
		vector<string> input(n);
		for (unsigned int i = 0; i < n; ++i) {
			scanf("%s\n", in);
			string number(in);
			input[i] = number;
		}
		std::sort(input.begin(), input.end());
		bool ok = true;
		for (unsigned int i = 0; i < n; ++i) {
			if (!ok) {
				continue;
			}
			ok = root.insert(input[i].c_str());
		}
		puts((ok) ? "YES" : "NO");
	}
	return 0;
}
