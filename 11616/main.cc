#include <cstdio>
#include <string>

using namespace std;

static int to_arabic(char *roman) {
	if (roman[0] == '\0') {
		return 0;
	}
	if (roman[0] == 'M') {
		return 1000 + to_arabic(roman + 1);
	}
	if (roman[0] == 'C' && roman[1] == 'M') {
		return 900 + to_arabic(roman + 2);
	}
	if (roman[0] == 'D') {
		return 500 + to_arabic(roman + 1);
	}
	if (roman[0] == 'C' && roman[1] == 'D') {
		return 400 + to_arabic(roman + 2);
	}
	if (roman[0] == 'C') {
		return 100 + to_arabic(roman + 1);
	}
	if (roman[0] == 'X' && roman[1] == 'C') {
		return 90 + to_arabic(roman + 2);
	}
	if (roman[0] == 'L') {
		return 50 + to_arabic(roman + 1);
	}
	if (roman[0] == 'X' && roman[1] == 'L') {
		return 40 + to_arabic(roman + 2);
	}
	if (roman[0] == 'X') {
		return 10 + to_arabic(roman + 1);
	}
	if (roman[0] == 'I' && roman[1] == 'X') {
		return 9 + to_arabic(roman + 2);
	}
	if (roman[0] == 'V') {
		return 5 + to_arabic(roman + 1);
	}
	if (roman[0] == 'I' && roman[1] == 'V') {
		return 4 + to_arabic(roman + 2);
	}
	if (roman[0] == 'I') {
		return 1 + to_arabic(roman + 1);
	}
	return 0;
}

/* clang-format off */
static const string romans[] = {
    "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I",
};
static const int arab[] = {
    1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1, 0,
};
/* clang-format on */

static string to_roman(int n) {
	string result;
	for (int i = 0; n > 0 && arab[i]; ++i) {
		while (n - arab[i] >= 0) {
			n -= arab[i];
			result.append(romans[i]);
		}
	}
	return result;
}

static char input[2048];

int main() {
	while (fgets(input, sizeof(input), stdin) != nullptr) {
		if (input[0] == '\n' || input[0] == '\0') {
			continue;
		}
		int possible_dec;
		if (sscanf(input, "%d", &possible_dec) == 1) {
			string roman_str = to_roman(possible_dec);
			printf("%s\n", roman_str.c_str());
		} else {
			possible_dec = to_arabic(input);
			printf("%d\n", possible_dec);
		}
	}
}
