#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cctype>
#include <iostream>
#include <string>

using namespace std;

int main() {
	for (string line; getline(cin, line);) {
		if (!line.empty()) {
			reverse(line.begin(), line.end());
			if (isdigit(line[0])) {
				for (size_t idx = 0; idx < line.length();) {
					int z = (line[idx] - '0') * 10;
					z += line[idx + 1] - '0';
					if (line[idx] == '1') {
						z = z * 10 + (line[idx + 2] - '0');
						idx += 1;
					}
					idx += 2;
					putchar((char) z);
				}
			} else {
				for (char c : line) {
					int x = (int) c;
					while (x) {
						putchar((x % 10) + '0');
						x /= 10;
					}
				}
			}
		}
		putchar('\n');
	}
	return 0;
}
