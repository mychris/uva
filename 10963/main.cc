#include <algorithm>
#include <cstdio>
#include <iostream>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		bool valid = true;
		int height_before = 0;
		int columns;
		scanf("%d\n", &columns);
		while (columns--) {
			int y1, y2;
			scanf("%d %d\n", &y1, &y2);
			int height = abs(y1 - y2);
			if (height_before && height_before != height) {
				valid = false;
			}
			height_before = height;
		}
		puts((valid) ? "yes" : "no");
		if (_C) {
			putchar('\n');
		}
	}
	return 0;
}
