#include <cinttypes>
#include <cstdio>

using namespace std;

#define CACHE_SIZE 1024
static int_fast8_t cache[CACHE_SIZE];

static uint64_t doit(uint64_t x) {
	uint64_t ret = 0;
	while (x) {
		uint64_t y = x % 10;
		ret += y * y;
		x /= 10;
	}
	return ret;
}

static bool check(uint64_t x) {
	for (;;) {
		if (x < CACHE_SIZE && cache[x]) {
			return (cache[x] & 0x10) != 0;
		}
		x = doit(x);
	}
}

int main() {
	cache[0] = 0x01;
	cache[1] = 0x10;
	cache[2] = 0x01;
	cache[3] = 0x01;
	cache[4] = 0x01;
	for (uint64_t i = 5; i < CACHE_SIZE; ++i) {
		if (check(i)) {
			cache[i] = 0x10;
		} else {
			cache[i] = 0x01;
		}
	}
	size_t _N, tc;
	if (scanf("%zu\n", &_N) != 1) {
		return 2;
	}
	for (tc = 1; tc <= _N; ++tc) {
		uint64_t x;
		if (scanf("%" SCNu64 "\n", &x) != 1) {
			return 3;
		}
		printf("Case #%zu: %" PRIu64 " is %s number.\n",
		       tc,
		       x,
		       (check(x)) ? "a Happy" : "an Unhappy");
	}
	return 0;
}
