#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <map>

using namespace std;

static map<long long, long long> test;

static long long next_col(long long n) {
	long long next = 1;
	long long col = 1;

	while (next < n) {
		next += (col & 1) ? 1 : (col * 2);
		++col;
	}
	return next;
}

static void init(void) {
	long long col = 1;
	long long adder = 4;
	long long n = 1;

	while (n < 10006102LL) {
		test[n] = col;
		col += 1;
		n += 1;

		test[n] = col;
		col += 1;
		n += adder;

		adder += 4;
	}
}

int main() {
	init();
	long long n;
	while (scanf("%lld\n", &n) == 1) {
		long long next_col_number = next_col(n);
		long long column_number = test[next_col_number];

		if (next_col_number == n) {
			printf("TERM %lld IS 1/%lld\n", n, test[n]);
		} else if (next_col_number - column_number + 1 <= n) {
			long long s = next_col_number - n;
			printf("TERM %lld IS %lld/%lld\n", n, s + 1, column_number - s);
		} else {
			next_col_number = next_col_number - ((column_number - 1) * 2);
			--column_number;
			long long a = n - next_col_number;
			printf("TERM %lld IS %lld/%lld\n", n, a + 1, column_number - a);
		}
	}
}
