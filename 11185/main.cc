#include <cstdio>

using namespace std;

static char output[1024];

int main() {
	int n;
	while (scanf("%d\n", &n) == 1 && n >= 0) {
		output[1023] = '\0';
		char *o = &output[1022];
		while (n >= 3) {
			*o-- = (char) (n % 3 + '0');
			n /= 3;
		}
		*o = (char) (n + '0');
		printf("%s\n", o);
	}
}
