#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int generators[100100];

static int digit_sum(int n) {
	int result = n;
	while (n) {
		result += n % 10;
		n /= 10;
	}
	return result;
}

static void init(void) {
	for (int i = 0; i < 100001; ++i) {
		int gen = digit_sum(i);
		if (!generators[gen] || generators[gen] > i) {
			generators[gen] = i;
		}
	}
}

int main() {
	init();
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int n;
		scanf("%d\n", &n);
		if (n > 100000) {
			continue;
		}
		printf("%d\n", generators[n]);
	}
	return 0;
}
