#include <cstdio>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int n1, n2, n3;
		scanf("%d %d %d\n", &n1, &n2, &n3);
		int mid;
		if (n1 > n2) {
			if (n3 > n2) {
				if (n3 < n1) {
					mid = n3;
				} else {
					mid = n1;
				}
			} else {
				mid = n2;
			}
		} else {
			if (n2 > n3) {
				if (n1 > n3) {
					mid = n1;
				} else {
					mid = n3;
				}
			} else {
				mid = n2;
			}
		}
		printf("Case %d: %d\n", the_case, mid);
	}
}
