#include <cmath>
#include <cstdio>

using namespace std;

static const long long max_cc_only = 24 * 60 * 60 * 100;
static const long long max_dec_cc_only = 10000000;

int main() {
	long long input;
	while (scanf("%lld\n", &input) == 1) {
		long long hh, mm, ss, cc;
		hh = input / 1000000;
		mm = input / 10000 % 100;
		ss = input / 100 % 100;
		cc = input % 100;
		long long cc_only = hh * 60 * 60 * 100 + mm * 60 * 100 + ss * 100 + cc;
		if (cc_only == 0) {
			puts("0000000");
		} else {
			long long div = (cc_only * max_dec_cc_only) / (max_cc_only);
			printf("%07lld\n", div);
		}
	}
	return 0;
}
