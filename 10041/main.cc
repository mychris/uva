#include <algorithm>
#include <cstdio>

using namespace std;

static int street_numbers[500];
static int relatives;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		scanf("%d", &relatives);
		for (int i = 0; i < relatives; ++i) {
			scanf("%d", &street_numbers[i]);
		}
		sort(street_numbers, street_numbers + relatives);
		int median = street_numbers[relatives >> 1];
		int distance = 0;
		for (int i = 0; i < (relatives >> 1); ++i) {
			distance += median - street_numbers[i];
		}
		for (int i = (relatives >> 1) + 1; i < relatives; ++i) {
			distance += street_numbers[i] - median;
		}
		printf("%d\n", distance);
	}
	return 0;
}
