#include <cstdio>
#include <cstring>

using namespace std;

#define MAX 10000

static int gcd_map[10000][10000];
static int input[200];
static int n_input;
static char line[5000];
static char delims[] = " \t\r\n";

static int gcd(int u, int v) {
	if (u < MAX && v < MAX && gcd_map[u][v]) {
		return gcd_map[u][v];
	}
	if (v == 0) {
		return u;
	}
	int result = gcd(v, u % v);
	if (u < MAX && v < MAX) {
		gcd_map[u][v] = result;
	}
	return result;
}

int main() {
	int i, j;
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		fgets(line, 5000, stdin);
		n_input = 0;
		int n;
		char *tok;
		tok = strtok(line, delims);
		while (tok != nullptr && sscanf(tok, "%d", &n) == 1) {
			input[n_input++] = n;
			tok = strtok(nullptr, delims);
		}

		int max = 0;
		for (i = 0; i < n_input; ++i) {
			for (j = i + 1; j < n_input; ++j) {
				int cur_gcd = gcd(input[i], input[j]);
				if (cur_gcd > max) {
					max = cur_gcd;
				}
			}
		}
		printf("%d\n", max);
	}
	return 0;
}
