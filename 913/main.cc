#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static long long calc(long long line) {
	line = (line + 1) >> 1;
	return (((line * line) << 1) - 1) * 3 - 6;
}

int main() {
	long long n;
	while (scanf("%lld\n", &n) == 1) {
		printf("%lld\n", calc(n));
	}
	return 0;
}
