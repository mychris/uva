#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int well_height;
static int climbing_distance;
static int slide_down;
static int fatigue_factor;

static double cur_height;
static double fatigue;
static double cur_climb;

static void climb(void) {
	cur_height += cur_climb;
	cur_climb -= fatigue;
	if (cur_climb < 0.0) {
		cur_climb = 0.0;
	}
}

static inline void slide(void) {
	cur_height -= slide_down;
}

int main() {
	while (scanf("%d %d %d %d",
	             &well_height,
	             &climbing_distance,
	             &slide_down,
	             &fatigue_factor) != EOF) {
		if (well_height <= 0 || climbing_distance <= 0 || slide_down <= 0 ||
		    fatigue_factor <= 0) {
			break;
		}
		int day = 0;
		cur_height = 0.0;
		fatigue = ((double) (climbing_distance * fatigue_factor)) / 100.0;
		cur_climb = climbing_distance;
		for (day = 1;; ++day) {
			climb();
			if (cur_height > well_height) {
				break;
			}
			slide();
			if (cur_height < 0.0) {
				break;
			}
		}

		if (cur_height <= 0.0) {
			printf("failure on day %d\n", day);
		} else {
			printf("success on day %d\n", day);
		}
	}
	return 0;
}
