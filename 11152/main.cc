#include <cmath>
#include <cstdio>

using namespace std;

const double PI = 3.141592653589793238463;

static inline double radius_excircle_triangle(const double side_a,
                                              const double side_b,
                                              const double side_c,
                                              const double area) {
	return (side_a * side_b * side_c) / (4.0 * area);
}

static inline double radius_incircle_triangle(const double side_a,
                                              const double side_b,
                                              const double side_c,
                                              const double area) {
	return (2 * area) / (side_a + side_b + side_c);
}

static inline double
area_triangle(const double side_a, const double side_b, const double side_c) {
	const double s = (side_a + side_b + side_c) / 2.0;
	return sqrt(s * (s - side_a) * (s - side_b) * (s - side_c));
}

int main() {
	double a, b, c;
	while (scanf("%lf %lf %lf\n", &a, &b, &c) == 3) {
		const double area = area_triangle(a, b, c);
		const double rad_ex = radius_excircle_triangle(a, b, c, area);
		const double rad_in = radius_incircle_triangle(a, b, c, area);
		double sunflowers = PI * rad_ex * rad_ex - area;
		double roses = PI * rad_in * rad_in;
		double violets = area - roses;
		printf("%.4lf %.4lf %.4lf\n", sunflowers, violets, roses);
	}
	return 0;
}
