#include <cstdio>
#include <cstring>

using namespace std;

int equ_no_ws(char *a, char *b) {
	while (*a && *b) {
		if (*a == ' ') {
			++a;
		} else if (*b == ' ') {
			++b;
		} else if (*a == *b) {
			++a;
			++b;
		} else {
			return 0;
		}
	}
	return *a == 0 && *b == 0;
}

int main() {
	int _C;
	char team[50], judge[50];
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		fgets(team, sizeof(team), stdin);
		fgets(judge, sizeof(judge), stdin);
		if (strcmp(team, judge) == 0) {
			printf("Case %d: Yes\n", the_case);
			continue;
		} else if (equ_no_ws(team, judge) == 1) {
			printf("Case %d: Output Format Error\n", the_case);
		} else {
			printf("Case %d: Wrong Answer\n", the_case);
		}
	}
	return 0;
}
