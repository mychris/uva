#include <cstdio>
#include <cstdlib>

using namespace std;

int main() {
	int t;
	scanf("%d\n", &t);
	while (t--) {
		long long n;
		scanf("%lld\n", &n);
		n *= 567;
		n /= 9;
		n += 7492;
		n *= 235;
		n /= 47;
		n -= 498;
		n = n / 10 % 10;
		printf("%lld\n", abs(n));
	}
	return 0;
}
