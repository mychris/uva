#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int T;
static char month[8];
static char day[8];

static int days_in_months[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

static inline int month_to_numeric(char *mon) {
	switch (mon[0]) {
	case 'J':
		switch (mon[1]) {
		case 'A':
			return 1;
		case 'U':
			switch (mon[2]) {
			case 'N':
				return 6;
			case 'L':
				return 7;
			}
		}
		return 0;
	case 'F':
		return 2;
	case 'M':
		switch (mon[1]) {
		case 'A':
			switch (mon[2]) {
			case 'R':
				return 3;
			case 'Y':
				return 5;
			}
		}
		return 0;
	case 'A':
		switch (mon[1]) {
		case 'P':
			return 4;
		case 'U':
			return 8;
		}
		return 0;
	case 'S':
		return 9;
	case 'O':
		return 10;
	case 'N':
		return 11;
	case 'D':
		return 12;
	}
	return 0;
}

static inline int day_to_numeric(char *d) {
	switch (d[0]) {
	case 'S':
		switch (d[1]) {
		case 'U':
			return 7;
		case 'A':
			return 6;
		}
		return 0;
	case 'M':
		return 1;
	case 'T':
		switch (d[1]) {
		case 'U':
			return 2;
		case 'H':
			return 4;
		}
		return 0;
	case 'W':
		return 3;
	case 'F':
		return 5;
	}
	return 0;
}

int main() {
	if (scanf("%d\n", &T) != 1) {
		return 1;
	}
	while (T--) {
		memset(&month[0], 0, sizeof(month));
		memset(&day[0], 0, sizeof(day));
		if (scanf("%3s %3s\n", &month[0], &day[0]) != 2) {
			return 1;
		}
		int numeric_month = month_to_numeric(month);
		int numeric_day = day_to_numeric(day);
		if (numeric_month == 0) {
			fprintf(stderr, "Unknown month: %s\n", month);
			return 1;
		}
		if (numeric_day == 0) {
			fprintf(stderr, "Unknown day: %s\n", day);
			return 1;
		}
		int dm = days_in_months[numeric_month];
		int friday = 5 - numeric_day + 1;
		if (friday <= 0) {
			friday += 7;
		}
		int saturday = 6 - numeric_day + 1;
		if (saturday <= 0) {
			saturday += 7;
		}
#ifndef ONLINE_JUDGE
		fprintf(stderr,
		        "%s(%d) %s(%d)  - FRI: %d SAT: %d\n",
		        month,
		        numeric_month,
		        day,
		        numeric_day,
		        friday,
		        saturday);
#endif
		// now start counting at 0 so we can use division
		--dm;
		--friday;
		--saturday;
		assert(dm >= 0 && dm < 31);
		assert(friday >= 0 && friday < 7);
		assert(saturday >= 0 && saturday < 7);
		int cnt = 0;
		cnt += (dm - friday) / 7 + 1;
		cnt += (dm - saturday) / 7 + 1;
		printf("%d\n", cnt);
	}
	return 0;
}
// m t w d f s s
