#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

#include <algorithm>

using namespace std;

static char buffer[4096];
static size_t pos = 0;
static size_t len = 0;

bool read_line(char *into) {
	size_t into_idx = 0;
	for (;;) {
		if (pos == len) {
			size_t read = fread(&buffer[0], sizeof(char), sizeof(buffer), stdin);
			if (read == 0) {
				return false;
			}
			len = (size_t) read;
			pos = 0;
		}
		while (pos < len && buffer[pos] != '\n') {
			into[into_idx] = buffer[pos];
			++pos;
			++into_idx;
		}
		if (pos < len && buffer[pos] == '\n') {
			++pos;
			into[into_idx] = '\0';
			return true;
		}
	}
}

static int cntr_a[256];
static int cntr_b[256];

static char a[1024];
static char b[1024];

static char out[2048];

int main() {
	while (read_line(&a[0]) && read_line(&b[0])) {
		memset(&cntr_a[0], 0, sizeof(cntr_a));
		memset(&cntr_b[0], 0, sizeof(cntr_b));
		for (char *ptr = &a[0]; *ptr; ++ptr) {
			cntr_a[(size_t) *ptr] += 1;
		}
		for (char *ptr = &b[0]; *ptr; ++ptr) {
			cntr_b[(size_t) *ptr] += 1;
		}
		char *ptr = &out[0];
		for (size_t i = (size_t) 'a'; i <= (size_t) 'z'; ++i) {
			size_t n = (size_t) min(cntr_a[i], cntr_b[i]);
			while (n--) {
				*ptr = (char) i;
				++ptr;
			}
		}
		*ptr = '\0';
		printf("%s\n", &out[0]);
	}
	return 0;
}
