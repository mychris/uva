#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

using namespace std;

static int *binary_search(int *start, size_t nelems, int target) {
	ssize_t l = 0;
	ssize_t r = (ssize_t) nelems - 1;
	while (l <= r) {
		ssize_t m = (l + r) / 2;
		if (start[m] < target) {
			l = m + 1;
		} else if (start[m] > target) {
			r = m - 1;
		} else {
			return start + m;
		}
	}
	return nullptr;
}

int main() {
	size_t n, m;
	while (scanf("%zu %zu\n", &n, &m) == 2 && (n > 0 || m > 0)) {
		int *data = (int *) malloc(sizeof(int) * n);
		if (!data) {
			return 27;
		}
		int *start = data;
		size_t search_cnt = (size_t) n;
		while (n--) {
			scanf("%d\n", data);
			++data;
		}
		data = start;
		size_t cnt = 0;

		while (m--) {
			int x;
			scanf("%d\n", &x);
			int *result = binary_search(start, search_cnt, x);
			if (result) {
				search_cnt -= (size_t) (result - start);
				cnt++;
				start = result;
			}
		}
		printf("%zu\n", cnt);
		free(data);
	}
	return 0;
}
