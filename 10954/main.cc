#include <cstdio>
#include <functional>
#include <queue>
#include <vector>

using namespace std;

int main() {
	int n;
	while (scanf("%d", &n) == 1 && n > 0) {
		priority_queue<int, vector<int>, greater<int>> list;
		while (n--) {
			int x;
			scanf("%d", &x);
			list.push(x);
		}
		int cost = 0;
		while (list.size() > 1) {
			int a, b;
			a = list.top();
			list.pop();
			b = list.top();
			list.pop();
			list.push(a + b);
			cost += a + b;
		}
		printf("%d\n", cost);
	}
	return 0;
}
