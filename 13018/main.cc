#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int sums[41];
static int n, m;

void doit(void) {
	assert(n >= 0 && n <= 20);
	assert(m >= 0 && m <= 20);
	memset(sums, 0, sizeof(sums));
	int max = 0;
	for (int a = 1; a <= n; ++a) {
		for (int b = 1; b <= m; ++b) {
			int const s = a + b;
			sums[s] += 1;
			if (sums[s] > max) {
				max = sums[s];
			}
		}
	}
	for (int i = 0; i < 41; ++i) {
		if (sums[i] == max) {
			printf("%d\n", i);
		}
	}
}

int main() {
	if (scanf("%d %d\n", &n, &m) != 2) {
		return 2;
	}
	for (;;) {
		doit();
		if (scanf("%d %d\n", &n, &m) == 2) {
			puts("");
		} else {
			break;
		}
	}
	return 0;
}
