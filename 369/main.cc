#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <climits>
#include <cstdio>

using namespace std;

/*
 * upper limit found by testing.
 */
#define MAX 2500
static unsigned long long triangle[MAX][MAX];

static void gen_triangle(void) {
	triangle[0][0] = 1;
	triangle[1][0] = 1;
	triangle[1][1] = 1;
	size_t x, y;
	for (x = 2; x < MAX; ++x) {
		triangle[x][0] = 1;
		triangle[x][x] = 1;
		for (y = 1; y < x; ++y) {
			triangle[x][y] = triangle[x - 1][y] + triangle[x - 1][y - 1];
		}
	}
}

static unsigned long long calc(size_t const n, size_t const m) {
	if (n == m) {
		return 1LLU;
	}
	if (m == 1 || m == n - 1) {
		return (unsigned long long) n;
	}
	if (m == 2 || m == n - 2) {
		return ((unsigned long long) n * ((unsigned long long) n - 1LLU)) / 2LLU;
	}
	return triangle[n][m];
}

int main() {
	gen_triangle();
	size_t n, m;
	while (scanf("%zu %zu", &n, &m) == 2 && n > 0 && m > 0) {
		if (m > n) {
			continue;
		}
		printf("%zu things taken %zu at a time is %llu exactly.\n", n, m, calc(n, m));
	}
	return 0;
}
