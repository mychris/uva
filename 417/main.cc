#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

#include <map>
#include <string>

using namespace std;

static map<string, int> cache;

static bool next(char *word, ssize_t len) {
#if !defined(ONLINE_JUDGE)
	assert(len >= 1);
	for (ssize_t i = 0; i < len; ++i) {
		assert(word[i] >= 'a' && word[i] <= 'z');
	}
#endif
	word[len - 1] += 1;
	for (ssize_t p = len - 1; p > 0; --p) {
		if (word[p] > 'z' - len + p + 1) {
			word[p - 1] += 1;
			word[p] = 'a' - 1;
		}
	}
	if (word[0] > 'z' - len + 1) {
		return false;
	}
	for (ssize_t p = 1; p < len; ++p) {
		if (word[p] <= word[p - 1]) {
			word[p] = word[p - 1] + 1;
		}
	}
#if !defined(ONLINE_JUDGE)
	for (ssize_t i = 0; i < len; ++i) {
		assert(word[i] >= 'a' && word[i] <= 'z');
	}
#endif
	return true;
}

template <ssize_t N> static void setup() {
	char word[(size_t) (N + 1)] = {};
	int cntr = 1;
	for (ssize_t len = 1; len <= N; ++len) {
		for (ssize_t i = 0; i < len; ++i) {
			word[i] = (char) ('a' + i);
		}
		for (;;) {
			cache.insert({string(&word[0]), cntr});
			++cntr;
			if (!next(&word[0], len)) {
				break;
			}
		}
	}
}

int main() {
	setup<5>();
	char input[16];
	while (scanf("%5s", &input[0]) == 1) {
		auto f = cache.find(string(&input[0]));
		if (f == cache.end()) {
#if !defined(ONLINE_JUDGE)
			bool ordered = true;
			for (char *ptr = &input[1]; *ptr; ++ptr) {
				if (*ptr <= *(ptr - 1)) {
					ordered = false;
					break;
				}
			}
			assert(!ordered);
#endif
			puts("0");
		} else {
			printf("%d\n", f->second);
		}
	}
}
