#include <cstdio>

using namespace std;

#define MAX (4294967296LLU)

static unsigned long long cache_f[5000];
static unsigned long long cache_m[5000];

static void init(void) {
	cache_f[0] = 1;
	unsigned long long *ptr_f = cache_f;
	unsigned long long *ptr_m = cache_m;
	while (*ptr_f < MAX && *ptr_m < MAX) {
		++ptr_f;
		++ptr_m;
		// the first female never dies!
		*ptr_f = 1 + *(ptr_m - 1);
		*ptr_m = *(ptr_m - 1) + *(ptr_f - 1);
	}
}

int main() {
	init();
	int year;
	while (scanf("%d\n", &year) == 1 && year >= 0) {
		printf("%llu %llu\n", cache_m[year], cache_f[year] + cache_m[year]);
	}
	return 0;
}
