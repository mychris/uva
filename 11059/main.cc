#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static long long numbers[1024];

static long long solve(int from, int to) {
	if (from + 1 == to) {
		return numbers[from];
	}
	long long up_to_first_neg = numbers[from];
	long long from_last_neg = numbers[from];
	long long mult = numbers[from];
	bool neg_seen = (numbers[from] < 0) ? true : false;
	for (int i = from + 1; i < to; ++i) {
		assert(numbers[i] != 0);
		if (!neg_seen) {
			up_to_first_neg *= numbers[i];
		}
		from_last_neg *= numbers[i];
		if (numbers[i] < 0) {
			neg_seen = true;
			from_last_neg = numbers[i];
		}
		mult *= numbers[i];
	}
	if (mult < 0) {
		assert(neg_seen);
		assert(up_to_first_neg < 0);
		assert(from_last_neg < 0);
		long long a = mult / up_to_first_neg;
		long long b = mult / from_last_neg;
		return (a > b) ? a : b;
	} else {
		return mult;
	}
}

static long long solve(int elements) {
	// split up by the zeros
	int last_end = 0;
	long long max = 0;
	for (int index = 0; index < elements; index++) {
		if (numbers[index] == 0) {
			long long this_max = solve(last_end, index);
			last_end = index + 1;
			if (this_max > max) {
				max = this_max;
			}
		}
	}
	return max;
}

int main() {
	int elements;
	int testCase = 0;
	while (scanf("%d", &elements) == 1 && elements > 0) {
		for (int i = 0; i < elements; ++i) {
			scanf("%lld", &numbers[i]);
		}
		numbers[elements] = 0;
		++elements;
		printf(
		    "Case #%d: The maximum product is %lld.\n\n", ++testCase, solve(elements));
	}
	return 0;
}
