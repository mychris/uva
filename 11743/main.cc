#include <cstdio>

using namespace std;

static void skip_to_newline(void) {
	int c;
	for (;;) {
		c = getchar();
		if (c == EOF || c == '\n') {
			break;
		}
	}
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		bool still_valid = true;
		int sum_doubled = 0;
		int sum = 0;
		for (int j = 0; still_valid && j < 4; ++j) {
			for (int i = 0; still_valid && i < 4; ++i) {
				int c = getchar();
				if (c < '0' || c > '9') {
					still_valid = false;
					ungetc(c, stdin);
					break;
				}
				if (i & 1) {
					sum += c - '0';
				} else {
					int doubled = (c - '0') * 2;
					while (doubled) {
						sum_doubled += doubled % 10;
						doubled /= 10;
					}
				}
			}
			if (j < 3) {
				int c = getchar();
				if (c != ' ') {
					still_valid = false;
					ungetc(c, stdin);
				}
			}
		}
		skip_to_newline();
		if (still_valid && (sum + sum_doubled) % 10 == 0) {
			puts("Valid");
		} else {
			puts("Invalid");
		}
	}
	return 0;
}
