#include <bitset>
#include <cmath>
#include <cstdio>

using namespace std;

#define UPPER_BOUND 1000001

static bitset<UPPER_BOUND + 1> h_primes;
static bitset<UPPER_BOUND + 1> semis;

static unsigned int cache[UPPER_BOUND + 1];

static void sieve(void) {
	unsigned int upper_bound_square_root = (unsigned int) sqrt(UPPER_BOUND);
	bitset<UPPER_BOUND + 1> is_composite;

	for (unsigned int m = 5; m <= upper_bound_square_root; m += 4) {
		if (!is_composite[m]) {
			for (unsigned int k = m * m; k <= UPPER_BOUND; k += m) {
				is_composite[k] = true;
			}
		}
	}
	for (unsigned int m = 5; m <= UPPER_BOUND; m += 4) {
		if (!is_composite[m]) {
			h_primes[m] = true;
		}
	}

	is_composite.reset();
	for (unsigned int m = 5; m <= UPPER_BOUND; m += 4) {
		if (h_primes[m]) {
			is_composite[m] = true;
		} else if (!is_composite[m]) {
			for (unsigned int k = m * 2; k <= UPPER_BOUND; k += m) {
				is_composite[k] = true;
			}
		}
	}
	for (unsigned int m = 5; m <= UPPER_BOUND; m += 4) {
		if (!is_composite[m]) {
			semis[m] = true;
		}
	}
}

static void init(void) {
	sieve();
	unsigned int counter = 0;
	for (unsigned int i = 1; i <= UPPER_BOUND; ++i) {
		if (semis[i]) {
			++counter;
		}
		cache[i] = counter;
	}
}

int main() {
	init();
	unsigned int n;
	while (scanf("%u\n", &n) == 1 && n > 0) {
		printf("%u %u\n", n, cache[n]);
	}
	return 0;
}
