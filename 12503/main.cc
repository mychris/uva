#include <cstdio>
#include <cstring>

using namespace std;

static int instruction_cache[200];
static char input[100];

int main() {
	int _C, i;
	scanf("%d\n", &_C);
	while (_C--) {
		memset(instruction_cache, 0, sizeof(instruction_cache));
		int position = 0;
		int instructions;
		scanf("%d\n", &instructions);
		for (i = 1; i <= instructions; ++i) {
			scanf("%s", input);
			if (strcmp(input, "LEFT") == 0) {
				instruction_cache[i] = -1;
				position -= 1;
			} else if (strcmp(input, "RIGHT") == 0) {
				instruction_cache[i] = 1;
				position += 1;
			} else {
				scanf("%s", input);
				int pos;
				scanf("%d\n", &pos);
				position += instruction_cache[pos];
				instruction_cache[i] = instruction_cache[pos];
			}
		}
		printf("%d\n", position);
	}
	return 0;
}
