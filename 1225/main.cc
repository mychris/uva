#include <cstdio>

using namespace std;

static int cache[10000][10];

static void init(void) {
	for (size_t i = 0; i < 10; ++i) {
		cache[0][i] = 0;
	}
	for (size_t i = 1; i < 10000; ++i) {
		for (size_t j = 0; j < 10; ++j) {
			cache[i][j] = cache[i - 1][j];
		}
		size_t n = i;
		while (n) {
			cache[i][n % 10]++;
			n /= 10;
		}
	}
}

int main() {
	init();
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		int N;
		scanf("%d\n", &N);
		for (size_t i = 0; i < 9; ++i) {
			printf("%d ", cache[N][i]);
		}
		printf("%d\n", cache[N][9]);
	}
	return 0;
}
