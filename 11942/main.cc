#include <cstdio>

using namespace std;

static int lumbs[10];

int main() {
	int _C;
	scanf("%d\n", &_C);
	puts("Lumberjacks:");
	while (_C--) {
		scanf("%d %d %d %d %d %d %d %d %d %d",
		      &lumbs[0],
		      &lumbs[1],
		      &lumbs[2],
		      &lumbs[3],
		      &lumbs[4],
		      &lumbs[5],
		      &lumbs[6],
		      &lumbs[7],
		      &lumbs[8],
		      &lumbs[9]);
		int asc = 1, desc = 1, i;
		for (i = 1; i < 10; ++i) {
			if (lumbs[i - 1] < lumbs[i]) {
				asc = 0;
			}
			if (lumbs[i - 1] > lumbs[i]) {
				desc = 0;
			}
		}
		if (asc || desc) {
			puts("Ordered");
		} else {
			puts("Unordered");
		}
	}
	return 0;
}
