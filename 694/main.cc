#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

long long next(long long n) {
	if (n & 1LL) {
		return 3LL * n + 1LL;
	}
	return n >> 1LL;
}

int main() {
	int the_case = 1;
	long long start, max;
	while (scanf("%lld %lld\n", &start, &max) == 2 && start > 0 && max > 0) {
		int cnt = 0;
		long long cur = start;
		while (cur <= max && cur > 1) {
			cur = next(cur);
			++cnt;
		}
		if (cur == 1) {
			++cnt;
		}
		printf("Case %d: A = %lld, limit = %lld, number of terms = %d\n",
		       the_case,
		       start,
		       max,
		       cnt);
		++the_case;
	}
	return 0;
}
