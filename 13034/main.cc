#include <iostream>

using namespace std;

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int set = 1; set <= _C; ++set) {
		bool ok = true;
		for (int i = 0; i < 13; ++i) {
			int x;
			scanf("%d", &x);
			if (x == 0) {
				ok = false;
			}
		}
		printf("Set #%d: %s\n", set, (ok) ? "Yes" : "No");
	}
	return 0;
}
