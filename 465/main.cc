#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cerrno>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

using namespace std;

static char n1[2048];
static char op[32];
static char n2[2048];

int main() {
	for (string line; getline(cin, line);) {
		if (line.empty()) {
			continue;
		}
		if (sscanf(line.c_str(), "%s %s %s", n1, op, n2) != 3) {
			continue;
		}
		puts(line.c_str());
		double i1 = atof(n1), i2 = atof(n2);
		if (i1 > INT_MAX) {
			puts("first number too big");
		}
		if (i2 > INT_MAX) {
			puts("second number too big");
		}
		switch (op[0]) {
		case '+':
			if (i1 + i2 > INT_MAX) {
				puts("result too big");
			}
			break;
		case '*':
			if (i1 * i2 > INT_MAX) {
				puts("result too big");
			}
			break;
		default:
			printf("invalid operator '%s'\n", op);
			return 1;
		}
	}
	return 0;
}
