#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int h, m;
	while (scanf("%d:%d\n", &h, &m) == 2) {
		if (h == 0 && m == 0) {
			break;
		}
		double hd, md;
		hd = 360.0 / 12.0 * h + 360 / 720.0 * m;
		while (hd >= 360.0) {
			hd -= 360.0;
		}
		md = 360.0 / 60.0 * m;
		while (md >= 360.0) {
			md -= 360.0;
		}
		double diff1, diff2;
		diff1 = hd - md;
		while (diff1 < 0.0) {
			diff1 += 360.0;
		}
		diff2 = md - hd;
		while (diff2 < 0.0) {
			diff2 += 360.0;
		}
		printf("%.3lf\n", (diff1 < diff2) ? diff1 : diff2);
	}
	return 0;
}
