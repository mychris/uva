#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>

using namespace std;

static uint64_t isqrtu64(uint64_t x) {
	uint64_t m, y;
	m = UINT64_C(0x4000000000000000);
	y = 0;
	while (m != 0) {
		uint64_t b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

static uint64_t diagonals(int n) {
	assert(n >= 4);
	uint64_t x = static_cast<uint64_t>(n);
	return x * (x - 3LLU) / 2LLU;
}

static int small_cache[100];

int main() {
	{
		int sides = 4;
		uint64_t diags = diagonals(sides);
		for (size_t index = 1; index < sizeof(small_cache) / sizeof(small_cache[0]);
		     ++index) {
			small_cache[index] = sides;
			if (static_cast<uint64_t>(index) == diags) {
				sides += 1;
				diags = diagonals(sides);
			}
		}
	}
	int testCase = 0;
	uint64_t diags;
	while (scanf("%" SCNu64, &diags) == 1 && diags > 0) {
		testCase++;
		if (diags < sizeof(small_cache) / sizeof(small_cache[0])) {
			printf("Case %d: %d\n", testCase, small_cache[diags]);
		} else {
			int start = static_cast<int>(isqrtu64(diags * 2));
			while (diagonals(start) < diags) {
				++start;
			}
			printf("Case %d: %d\n", testCase, start);
		}
	}
	return 0;
}
