#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int hbound = 11, lbound = -1;
static int result = 1;
static char buffer[2][20];
static int guess = 0;

int seen[11];

static inline int max(int a, int b) {
	return (a > b) ? a : b;
}
static inline int min(int a, int b) {
	return (a < b) ? a : b;
}

int main() {
	while (scanf("%d\n", &guess) == 1 && guess > 0) {
		scanf("%s %s\n", &buffer[0][0], &buffer[1][0]);
		if (strcmp("high", buffer[1]) == 0) {
			seen[guess] = 1;
			hbound = min(hbound, guess);
		} else if (strcmp("low", buffer[1]) == 0) {
			seen[guess] = 1;
			lbound = max(lbound, guess);
		} else {
			if (guess > hbound || guess < lbound || hbound < lbound || seen[guess]) {
				result = 0;
			}
			if (result) {
				puts("Stan may be honest");
			} else {
				puts("Stan is dishonest");
			}
			hbound = 11;
			lbound = -1;
			result = 1;
			memset(seen, 0, sizeof(seen));
		}
	}
	return 0;
}
