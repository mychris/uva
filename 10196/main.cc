#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>
#include <initializer_list>
#include <utility>

using namespace std;

enum result {
	NO,
	BLACK,
	WHITE,
	END,
};

char board[24][24] = {};

static enum result solve() {
	enum result result = END;
	for (int row = 8; row < 16; ++row) {
		for (int col = 8; col < 16; ++col) {
			if (board[row][col] != '.') {
				result = NO;
			}
			switch (board[row][col]) {
			case 'p':
				if (board[row + 1][col + 1] == 'K' || board[row + 1][col - 1] == 'K') {
					return WHITE;
				}
				break;
			case 'P':
				if (board[row - 1][col + 1] == 'k' || board[row - 1][col - 1] == 'k') {
					return BLACK;
				}
				break;
			case 'r':
			case 'R':
				for (auto dir : {pair<int, int>{0, -1}, {0, +1}, {-1, 0}, {+1, 0}}) {
					for (int i = 1; i < 8; ++i) {
						char pos = board[row + dir.first * i][col + dir.second * i];
						if (pos == 'K' && board[row][col] == 'r') {
							return WHITE;
						} else if (pos == 'k' && board[row][col] == 'R') {
							return BLACK;
						} else if (pos != '.') {
							break;
						}
					}
				}
				break;
			case 'b':
			case 'B':
				for (auto dir :
				     {pair<int, int>{-1, -1}, {+1, +1}, {-1, +1}, {+1, -1}}) {
					for (int i = 1; i < 8; ++i) {
						char pos = board[row + dir.first * i][col + dir.second * i];
						if (pos == 'K' && board[row][col] == 'b') {
							return WHITE;
						} else if (pos == 'k' && board[row][col] == 'B') {
							return BLACK;
						} else if (pos != '.') {
							break;
						}
					}
				}
				break;
			case 'q':
			case 'Q':
				for (auto dir : {pair<int, int>{-1, -1},
				                 {+1, +1},
				                 {-1, +1},
				                 {+1, -1},
				                 {0, -1},
				                 {0, +1},
				                 {-1, 0},
				                 {+1, 0}}) {
					for (int i = 1; i < 8; ++i) {
						char pos = board[row + dir.first * i][col + dir.second * i];
						if (pos == 'K' && board[row][col] == 'q') {
							return WHITE;
						} else if (pos == 'k' && board[row][col] == 'Q') {
							return BLACK;
						} else if (pos != '.') {
							break;
						}
					}
				}
				break;
			case 'k':
			case 'K':
				for (auto dir : {pair<int, int>{-1, -1},
				                 {+1, +1},
				                 {-1, +1},
				                 {+1, -1},
				                 {0, -1},
				                 {0, +1},
				                 {-1, 0},
				                 {+1, 0}}) {
					char pos = board[row + dir.first][col + dir.second];
					if (pos == 'K' && board[row][col] == 'k') {
						return WHITE;
					} else if (pos == 'k' && board[row][col] == 'K') {
						return BLACK;
					}
				}
				break;
			case 'n':
			case 'N':
				for (auto dir : {pair<int, int>{-2, +1},
				                 {-2, -1},
				                 {-1, +2},
				                 {-1, -2},
				                 {+1, -2},
				                 {+1, +2},
				                 {+2, -1},
				                 {+2, +1}}) {
					char pos = board[row + dir.first][col + dir.second];
					if (pos == 'K' && board[row][col] == 'n') {
						return WHITE;
					} else if (pos == 'k' && board[row][col] == 'N') {
						return BLACK;
					}
				}
				break;
			}
		}
	}
	return result;
}

int main() {
	memset(board, '\0', sizeof(board));
	for (int game = 1;; ++game) {
		for (int row = 8; row < 16; ++row) {
			scanf("%8s", board[row] + 8);
		}
		switch (solve()) {
		case END:
			return 0;
		case NO:
			printf("Game #%d: no king is in check.\n", game);
			break;
		case BLACK:
			printf("Game #%d: black king is in check.\n", game);
			break;
		case WHITE:
			printf("Game #%d: white king is in check.\n", game);
			break;
		}
	}
	return 0;
}
