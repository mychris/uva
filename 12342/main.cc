#include <algorithm>
#include <cmath>
#include <cstdio>

using namespace std;

static int calc_tax(int income) {
	double tax = 0;
	if ((income -= 180000) <= 0) {
		goto RET;
	}
	tax += min(income, 300000) * 0.1;
	if ((income -= 300000) <= 0) {
		goto RET;
	}
	tax += min(income, 400000) * 0.15;
	if ((income -= 400000) <= 0) {
		goto RET;
	}
	tax += min(income, 300000) * 0.2;
	if ((income -= 300000) <= 0) {
		goto RET;
	}
	tax += income * 0.25;
RET:
	if (tax > 0 && tax < 2000.0) {
		return 2000;
	}
	return (int) ceil(tax);
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int income;
		scanf("%d\n", &income);
		printf("Case %d: %d\n", the_case, calc_tax(income));
	}
	return 0;
}
