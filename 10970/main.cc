#include <cstdio>

using namespace std;

static inline int calc(int m, int n) {
	return (m - 1) + m * (n - 1);
}

int main() {
	int w, h;
	while (scanf("%d %d\n", &w, &h) == 2) {
		printf("%d\n", calc(w, h));
	}
}
