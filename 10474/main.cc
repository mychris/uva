#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

#define MAX_INPUT 10000

struct Marble {
	int position;
	int count;
};

static int N, Q;
static int CASE = 0;

static struct Marble vec[MAX_INPUT + 1];

int main() {
	for (;;) {
		++CASE;
		if (scanf("%d %d\n", &N, &Q) != 2) {
			return 1;
		}
		if (N == 0 && Q == 0) {
			return 0;
		}
		if (N < 0 || N > MAX_INPUT || Q < 0 || Q > MAX_INPUT) {
			return 1;
		}
		memset(&vec[0], 0, sizeof(vec));

		for (int i = 1; i <= N; ++i) {
			int x;
			scanf("%d\n", &x);
			vec[x].count += 1;
		}

		int current_position = 1;
		for (int i = 0; i <= MAX_INPUT; ++i) {
			if (vec[i].count != 0) {
				vec[i].position = current_position;
				current_position += vec[i].count;
			}
		}

		printf("CASE# %d:\n", CASE);
		for (int i = 1; i <= Q; ++i) {
			int x;
			scanf("%d\n", &x);
			if (vec[x].count != 0) {
				printf("%d found at %d\n", x, vec[x].position);
			} else {
				printf("%d not found\n", x);
			}
		}
	}
}
