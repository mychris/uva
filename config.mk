ONLINE_JUDGE ?= 1

SHELL := /bin/sh

DIFF := colordiff
DIFF_OPT := -u --suppress-common-lines

CXX := g++
CC := gcc
RM := rm -f

# Compilation locally
CFLAGS_LOCAL := -ansi -O0 -g -pipe
CXXFLAGS_LOCAL := -std=c++11 -O0 -g -pipe
WARNFLAGS_LOCAL := -Wall -Wextra
WARNFLAGS_LOCAL += -Wformat -Wreturn-type -Wstrict-aliasing
WARNFLAGS_LOCAL += -Wcast-qual -Wcast-align -Wconversion
WARNFLAGS_LOCAL += -Wwrite-strings -Wsign-conversion
WARNFLAGS_LOCAL += -Wshadow
WARNFLAGS_LOCAL += -Wno-error=unused-variable
WARNFLAGS_LOCAL += -pedantic -pedantic-errors
WARNFLAGS_LOCAL += -Werror
WARNFLAGS_LOCAL += -Wno-unused-parameter
INSTRUMENTATION_LOCAL := -fsanitize=address -fsanitize=pointer-compare -fsanitize=pointer-subtract
INSTRUMENTATION_LOCAL := -fsanitize=leak
INSTRUMENTATION_LOCAL := -fsanitize=undefined
INSTRUMENTATION_LOCAL := -fno-sanitize-recover=all
CFLAGS_LOCAL += $(WARNFLAGS_LOCAL) $(INSTRUMENTATION_LOCAL)
CXXFLAGS_LOCAL += $(WARNFLAGS_LOCAL) $(INSTRUMENTATION_LOCAL)
LDFLAGS_LOCAL := -lm -lcrypt

# Compilation like the online judge does.
# Take the flags from the website.
CFLAGS_ONLINE := -ansi -O2 -g0 -pipe -DONLINE_JUDGE
CXXFLAGS_ONLINE := -std=c++11 -O2 -g0 -pipe -DONLINE_JUDGE
LDFLAGS_ONLINE := -lm -lcrypt
