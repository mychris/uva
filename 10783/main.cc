#include <cstdio>

using namespace std;

static int sums[101];

static void init(void) {
	int a = 0;
	for (int b = 1; b <= 100; b += 2) {
		sums[b - 1] = a;
		a += b;
		sums[b] = a;
	}
	sums[100] = a;
}

int main() {
	init();
	int _C;
	scanf("%d", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int a, b;
		scanf("%d\n%d\n", &a, &b);
		printf("Case %d: %d\n", the_case, sums[b] - sums[a - 1]);
	}
	return 0;
}
