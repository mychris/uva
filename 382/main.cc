#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static int test(const int x) {
	int s = 0;
	for (int i = 1; i < x; i++) {
		if (x % i == 0) {
			s += i;
		}
	}
	return (s == x) ? 0 : ((s < x) ? -1 : 1);
}

int main() {
	int x, res;
	puts("PERFECTION OUTPUT");
	while (scanf("%d", &x) != EOF && x != 0) {
		printf("%5d  ", x);
		res = test(x);
		if (res == 0) {
			puts("PERFECT");
		} else if (res < 0) {
			puts("DEFICIENT");
		} else {
			puts("ABUNDANT");
		}
	}
	puts("END OF OUTPUT");
	return 0;
}
