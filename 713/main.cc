#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <iostream>

using namespace std;

static string add(string a, string b) {
	string result = "";
	auto a_idx = a.rbegin();
	auto b_idx = b.rbegin();
	int carry = 0;
	while (a_idx != a.rend() || b_idx != b.rend()) {
		result.insert(0, "0");
		if (a_idx != a.rend()) {
			result[0] = (char) (result[0] + (*a_idx - '0'));
			++a_idx;
		}
		if (b_idx != b.rend()) {
			result[0] = (char) (result[0] + (*b_idx - '0'));
			++b_idx;
		}
		result[0] = (char) (result[0] + carry);
		if (result[0] > '9') {
			carry = 1;
			result[0] = (char) (result[0] - 10);
		} else {
			carry = 0;
		}
	}

	if (carry) {
		result.insert(0, "0");
		result[0] = (char) (result[0] + carry);
	}

	return result;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		string i1, i2;
		cin >> i1 >> i2;
		reverse(i1.begin(), i1.end());
		reverse(i2.begin(), i2.end());

		string sum = add(i1, i2);
		reverse(sum.begin(), sum.end());
		while (sum[0] == '0') {
			sum.erase(0, 1);
		}

		cout << sum << endl;
	}
	return 0;
}
