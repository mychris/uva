#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

int main() {
	int c;
	int test = 1;
	while ((c = getchar()) != EOF) {
		if (c == '"') {
			if (test) {
				puts("``");
			} else {
				puts("''");
			}
			test = !test;
		} else {
			putchar(c);
		}
	}
	return 0;
}
