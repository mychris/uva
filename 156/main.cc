#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

static vector<string> words;
static vector<string> sorted;
static vector<string> result;

static bool check(size_t const idx) {
	string const &target = sorted[idx];
	size_t const len = sorted.size();
	for (size_t i = 0; i < len; ++i) {
		if (i == idx) {
			continue;
		}
		if (sorted[i] == target) {
			return false;
		}
	}
	return true;
}

int main() {
	string word;
	while (cin >> word) {
		if (word == "#") {
			break;
		}
		words.push_back(word);
		transform(word.begin(), word.end(), word.begin(), ::tolower);
		sort(word.begin(), word.end());
		sorted.push_back(word);
	}
	size_t const len = words.size();
	for (size_t i = 0; i < len; ++i) {
		if (check(i)) {
			result.push_back(words[i]);
		}
	}
	sort(result.begin(), result.end());
	for (size_t i = 0; i < result.size(); ++i) {
		cout << result[i] << endl;
	}
	return 0;
}
