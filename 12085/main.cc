#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static int numbers[100003];

int main() {
	size_t test_case = 1;
	int n;
	while (scanf("%d\n", &n) == 1 && n > 0) {
		for (int i = 0; i < n; ++i) {
			scanf("%d\n", &numbers[i]);
		}
		numbers[n] = 0;
		printf("Case %zu:\n", test_case++);
		for (int i = 0; i < n; ++i) {
			if (numbers[i] + 1 != numbers[i + 1]) {
				printf("0%d\n", numbers[i]);
			} else {
				printf("0%d-", numbers[i]);
				int next = i + 1;
				while (numbers[next] + 1 == numbers[next + 1]) {
					++next;
				}
				int mod = 10;
				while (numbers[i] - (numbers[i] % mod) !=
				       numbers[next] - (numbers[next] % mod)) {
					mod *= 10;
				}
				printf("%u\n", numbers[next] % mod);
				i = next;
			}
		}
		puts("");
	}
	return 0;
}
