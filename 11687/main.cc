#include <iostream>
#include <string>

using namespace std;

static inline size_t ndigits(size_t n) {
	size_t ret = 0;
	while (n > 0) {
		++ret;
		n /= 10;
	}
	return ret;
}

static size_t calc(string in) {
	size_t len = in.size();
	if (len == 1 && in[0] == '1') {
		return 1;
	}
	size_t working_len = len;
	size_t result = 1;
	while (working_len != 1) {
		working_len = ndigits(working_len);
		++result;
	}
	return result + 1;
}

int main() {
	for (string line; getline(cin, line);) {
		if (line == "END") {
			break;
		}
		printf("%zu\n", calc(line));
	}
	return 0;
}
