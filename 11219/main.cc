#include <cstdio>

using namespace std;

static int
calc(int cur_d, int cur_m, int cur_y, int birth_d, int birth_m, int birth_y) {
	if (birth_y > cur_y) {
		return -1;
	}
	if (birth_y == cur_y && birth_m > cur_m) {
		return -1;
	}
	if (birth_y == cur_y && birth_m == cur_m && birth_d > cur_d) {
		return -1;
	}
	int age = cur_y - birth_y;
	if (cur_m < birth_m || (cur_m == birth_m && cur_d < birth_d)) {
		--age;
	}
	return age;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int cur_d, cur_m, cur_y;
		int birth_d, birth_m, birth_y;
		scanf("%d/%d/%d", &cur_d, &cur_m, &cur_y);
		scanf("%d/%d/%d", &birth_d, &birth_m, &birth_y);

		int age = calc(cur_d, cur_m, cur_y, birth_d, birth_m, birth_y);
		if (age < 0) {
			printf("Case #%d: Invalid birth date\n", the_case);
		} else if (age > 130) {
			printf("Case #%d: Check birth date\n", the_case);
		} else {
			printf("Case #%d: %d\n", the_case, age);
		}
	}
	return 0;
}
