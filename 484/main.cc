#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

#include <map>
#include <vector>

using namespace std;

static map<int, int> cntr;
static vector<int> numbers;

int main() {
	int n;
	while (scanf("%d", &n) == 1) {
		auto N = cntr.find(n);
		if (N == cntr.end()) {
			numbers.push_back(n);
			cntr.insert({n, 1});
		} else {
			N->second += 1;
		}
	}
	for (auto it = numbers.begin(); it != numbers.end(); ++it) {
		printf("%d %d\n", *it, cntr[*it]);
	}
	return 0;
}
