#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

/* clang-format off */
static const char mirror_map[] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*',
    '+', ',', '-', '.', '/', '0',
    '1', 'S', 'E', '#', 'Z', '#', '#', '8', '#', /* 1 - 9 */
    ':', ';', '<', '=', '>', '?', '@',
    'A', '#', '#', '#', '3', '#', '#', 'H', 'I', 'L', '#', 'J', /* A - L */
    'M', '#', 'O', '#', '#', '#', '2', 'T', 'U', 'V', 'W', 'X', /* M - x */
    'Y', '5',
};
/* clang-format on */

static void trim(string &str) {
	string::size_type pos = str.find_last_not_of(' ');
	if (pos != string::npos) {
		str.erase(pos + 1);
		pos = str.find_first_not_of(' ');
		if (pos != string::npos) {
			str.erase(0, pos);
		}
	} else {
		str.erase(str.begin(), str.end());
	}
}

int main() {
	for (string line; getline(cin, line);) {
		trim(line);
		size_t const len = line.size();
		if (len == 0) {
			continue;
		}
		bool pal = true, mir = true;
		for (size_t i = 0; i < len / 2 + 1; ++i) {
			if (line[i] != line[len - i - 1]) {
				pal = false;
			}
			if (line[i] != mirror_map[(size_t) line[len - i - 1]]) {
				mir = false;
			}
			if (!pal && !mir) {
				break;
			}
		}
		if (pal && mir) {
			cout << line << " -- is a mirrored palindrome." << endl;
		} else if (pal) {
			cout << line << " -- is a regular palindrome." << endl;
		} else if (mir) {
			cout << line << " -- is a mirrored string." << endl;
		} else {
			cout << line << " -- is not a palindrome." << endl;
		}
		cout << endl;
	}
	return 0;
}
