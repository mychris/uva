#include <iostream>
#include <queue>
#include <utility>
#include <vector>

using namespace std;

struct node {
	int id;
	int value;
	vector<int> neighbours;
};

static node nodes[128];

pair<int, int> run(void) {
	const node *cur = &nodes[0];
	int sum = cur->value;
	while (!cur->neighbours.empty()) {
		const node *next = &nodes[cur->neighbours[0]];
		for (int neighbour : cur->neighbours) {
			if (nodes[neighbour].value > next->value) {
				next = &nodes[neighbour];
			}
		}
		sum += next->value;
		cur = next;
	}

	return pair<int, int>(sum, cur->id);
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int n_nodes, n_edges;
		scanf("%d %d\n", &n_nodes, &n_edges);
		for (int i = 0; i < n_nodes; ++i) {
			nodes[i].id = i;
			nodes[i].neighbours.clear();
			scanf("%d", &nodes[i].value);
		}
		for (int i = 0; i < n_edges; ++i) {
			int from, to;
			scanf("%d %d\n", &from, &to);
			nodes[from].neighbours.push_back(to);
		}

		pair<int, int> res = run();
		printf("Case %d: %d %d\n", the_case, res.first, res.second);
	}
	return 0;
}
