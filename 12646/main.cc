#include <cstdio>

using namespace std;

char results[9] = {
    '*', /* 0 0 0 */
    'C', /* 0 0 1 */
    'B', /* 0 1 0 */
    'A', /* 0 1 1 */
    'A', /* 1 0 0 */
    'B', /* 1 0 1 */
    'C', /* 1 1 0 */
    '*', /* 1 1 1 */
};

int main() {
	int A, B, C;
	while (scanf("%d %d %d\n", &A, &B, &C) == 3) {
		int x = ((A & 1) << 2) | ((B & 1) << 1) | (C & 1);
		if (x > 9 || x < 0) {
			continue;
		}
		putchar(results[x]);
		putchar('\n');
	}
	return 0;
}
