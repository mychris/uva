#include <cmath>
#include <cstdio>
#include <set>

using namespace std;

static long long n;
static long long k;
static long long biggest;

static set<long long> seen;

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		if (scanf("%lld %lld\n", &n, &k) != 2) {
			break;
		}
		seen.clear();
		n = (long long) pow(10, n);
		biggest = k;
		while (seen.find(k) == seen.end()) {
			seen.insert(k);
			k *= k;
			while (k >= n) {
				k /= 10;
			}
			if (k > biggest) {
				biggest = k;
			}
		}
		printf("%lld\n", biggest);
	}
	return 0;
}
