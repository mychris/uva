#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <cstring>

using namespace std;

static char best[200];
static char work[200];

int main() {
	int _C;
	scanf("%d\n", &_C);
	while (_C--) {
		if (scanf("%s\n", work) != 1) {
			break;
		}
		strcpy(best, work);
		size_t len = strlen(work);
		size_t i;
		for (i = 0; i < len; ++i) {
			char tmp = work[0];
			memmove(&work[0], &work[1], len - 1);
			work[len - 1] = tmp;
			if (strcmp(best, work) > 0) {
				strcpy(best, work);
			}
		}
		puts(best);
	}
}
