#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>

using namespace std;

static void print_line(int times) {
	int i = times;
	while (i--) {
		printf("%d", times);
	}
	putchar('\n');
}

void print_wave(int amp) {
	for (int i = 1; i <= amp; i++) {
		print_line(i);
	}
	for (int i = amp - 1; i > 0; i--) {
		print_line(i);
	}
}

int main() {
	int _C;
	scanf("%d", &_C);
	while (_C--) {
		int amp, freq;
		scanf("%d", &amp);
		scanf("%d", &freq);
		for (int i = 1; i <= freq; i++) {
			print_wave(amp);
			if (i != freq) {
				putchar('\n');
			}
		}
		if (_C > 0) {
			putchar('\n');
		}
	}

	return 0;
}
