#include <cstdio>
#include <cstring>

using namespace std;

static char input[64];

static int test_left(size_t pos) {
	if (input[pos + 1] != '-' && input[pos + 1] != '=') {
		return 1;
	}
	int cnt = 2;
	char type = input[++pos];
	while (input[++pos] == type) {
		++cnt;
	}
	return cnt;
}

static int test_right(size_t pos) {
	if (input[pos - 1] != '-' && input[pos - 1] != '=') {
		return 1;
	}
	int cnt = 2;
	char type = input[--pos];
	while (input[--pos] == type) {
		++cnt;
	}
	return cnt;
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		if (fgets(&input[1], sizeof(input) - 1, stdin) == nullptr) {
			break;
		}
		int best = -1;
		int cnt = -1;
		for (size_t i = 1; input[i]; ++i) {
			switch (input[i]) {
			case '<':
				cnt = test_left(i);
				break;
			case '>':
				cnt = test_right(i);
				break;
			}
			if (cnt > best) {
				best = cnt;
			}
		}
		printf("Case %d: %d\n", the_case, best);
	}
	return 0;
}
