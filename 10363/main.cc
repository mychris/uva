#include <cstdio>

#define NO      \
	puts("no"); \
	continue
#define YES      \
	puts("yes"); \
	continue

#define B100000000 0x100
#define B111000000 0x1C0
#define B000111000 0x038
#define B000000111 0x007
#define B100100100 0x124
#define B010010010 0x092
#define B001001001 0x049
#define B100010001 0x111
#define B001010100 0x054

using namespace std;

bool won_chars(char *in, char c) {
	return (in[0] == c && in[1] == c && in[2] == c) ||
	       (in[3] == c && in[4] == c && in[5] == c) ||
	       (in[6] == c && in[7] == c && in[8] == c) ||
	       (in[0] == c && in[3] == c && in[6] == c) ||
	       (in[1] == c && in[4] == c && in[7] == c) ||
	       (in[2] == c && in[5] == c && in[8] == c) ||
	       (in[0] == c && in[4] == c && in[8] == c) ||
	       (in[2] == c && in[4] == c && in[6] == c);
}

bool won_bitmask(size_t in) {
	return (in & B111000000) == B111000000 || (in & B000111000) == B000111000 ||
	       (in & B000000111) == B000000111 || (in & B100100100) == B100100100 ||
	       (in & B010010010) == B010010010 || (in & B001001001) == B001001001 ||
	       (in & B100010001) == B100010001 || (in & B001010100) == B001010100;
}

int main() {
	int _N;
	scanf("%d\n", &_N);
	while (_N--) {
		char l[10];
		char *ptr = &l[0];
		if (scanf("%3s\n%3s\n%3s\n", &l[0], &l[3], &l[6]) != 3) {
			return 1;
		}
		size_t xs = 0, os = 0, x_bm = 0, o_bm = 0, m = B100000000;
		while (*ptr) {
			switch (*ptr) {
			case 'X':
				++xs;
				x_bm |= m;
				break;
			case 'O':
				++os;
				o_bm |= m;
				break;
			case '.':
				break;
			default:
				fprintf(stderr, "invalid input '%c'\n", *ptr);
				return 2;
			}
			++ptr;
			m >>= 1;
		}

		if (xs > 5 || os > 4 || !(xs == os || xs == os + 1)) {
			NO;
		}
		if ((won_bitmask(x_bm) && xs != os + 1) || (won_bitmask(o_bm) && xs != os)) {
			NO;
		}
		YES;
	}
	return 0;
}
