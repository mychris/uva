#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <cassert>
#include <cstdio>
#include <functional>
#include <limits>
#include <queue>
#include <vector>

using namespace std;

class CoefRange {
  public:
	double from, to, coef;
	CoefRange(double _from, double _to, double _coef)
	    : from(_from), to(_to), coef(_coef) {
	}
};

class Elem {
  public:
	double cord, coef;
	Elem(double _cord, double _coef) : cord(_cord), coef(_coef) {
	}
};

bool cmp(const Elem &lhs, const Elem &rhs) {
	return lhs.cord > rhs.cord;
}

int main() {
	int _N;
	scanf("%d\n", &_N);
	while (_N--) {
		int lines;
		scanf("%d\n", &lines);
		double x1, y1, x2, y2, c;
		priority_queue<Elem, vector<Elem>, function<bool(Elem, Elem)>> queue(cmp);
		vector<CoefRange> result;

		// read the lines, and sort per X values. Y values are ignored
		while (lines--) {
			scanf("%lf %lf %lf %lf %lf\n", &x1, &y1, &x2, &y2, &c);
			queue.push(Elem(min(x1, x2), c));
			queue.push(Elem(max(x1, x2), 1.0 / c));
		}

		double current = 1.0;
		if (queue.top().cord != -numeric_limits<double>::infinity()) {
			result.push_back(CoefRange(
			    -numeric_limits<double>::infinity(), queue.top().cord, current));
		}
		while (!queue.empty()) {
			double first_x = queue.top().cord;
			current *= queue.top().coef;
			queue.pop();
			double second_x =
			    (queue.empty()) ? numeric_limits<double>::infinity() : queue.top().cord;
			result.push_back(CoefRange(first_x, second_x, current));
		}
		printf("%zu\n", result.size());
		for (const CoefRange &r : result) {
			if (r.to == numeric_limits<double>::infinity()) {
				printf("%.3lf %+.3lf %.3lf\n", r.from, r.to, r.coef);
			} else {
				printf("%.3lf %.3lf %.3lf\n", r.from, r.to, r.coef);
			}
		}
		if (_N) {
			puts("");
		}
	}
	return 0;
}
