#include <cstdio>
#include <cstdlib>

using namespace std;

static const int pog = 40 * 7;

static inline bool is_leap_year(const int year) {
	return ((year & 3) == 0 && ((year % 25) != 0 || (year & 15) == 0));
}

static int days_in_month(int month, int year) {
	switch (month) {
	case 1:
		return 31;
	case 2:
		return (is_leap_year(year)) ? 29 : 28;
	case 3:
		return 31;
	case 4:
		return 30;
	case 5:
		return 31;
	case 6:
		return 30;
	case 7:
		return 31;
	case 8:
		return 31;
	case 9:
		return 30;
	case 10:
		return 31;
	case 11:
		return 30;
	case 12:
		return 31;
	default:
		fprintf(stderr, "invalid month %d\n", month);
		exit(127);
	}
}

/*
 * Sign			Begin				End
 * Aquarius		January, 21			February, 19
 * Pisces		February, 20		March, 20
 * Aries		March, 21			April, 20
 * Taurus		April, 21			May, 21
 * Gemini		May, 22				June, 21
 * Cancer		June, 22			July, 22
 * Leo			July, 23			August, 21
 * Virgo		August, 22			September, 23
 * Libra		September, 24		October, 23
 * Scorpio		October, 24			November, 22
 * Sagittarius	November, 23		December, 22
 * Capricorn	December, 23		January, 20
 */
static const char *get_zodiac_sign(const int day, const int month, const int year) {
	if ((month == 1 && day >= 21) || (month == 2 && day <= 19)) {
		return "aquarius";
	}
	if ((month == 2 && day >= 20) || (month == 3 && day <= 20)) {
		return "pisces";
	}
	if ((month == 3 && day >= 21) || (month == 4 && day <= 20)) {
		return "aries";
	}
	if ((month == 4 && day >= 21) || (month == 5 && day <= 21)) {
		return "taurus";
	}
	if ((month == 5 && day >= 22) || (month == 6 && day <= 21)) {
		return "gemini";
	}
	if ((month == 6 && day >= 22) || (month == 7 && day <= 22)) {
		return "cancer";
	}
	if ((month == 7 && day >= 23) || (month == 8 && day <= 21)) {
		return "leo";
	}
	if ((month == 8 && day >= 22) || (month == 9 && day <= 23)) {
		return "virgo";
	}
	if ((month == 9 && day >= 24) || (month == 10 && day <= 23)) {
		return "libra";
	}
	if ((month == 10 && day >= 24) || (month == 11 && day <= 22)) {
		return "scorpio";
	}
	if ((month == 11 && day >= 23) || (month == 12 && day <= 22)) {
		return "sagittarius";
	}
	if ((month == 12 && day >= 23) || (month == 1 && day <= 20)) {
		return "capricorn";
	}
	fprintf(stderr, "invalid date for zodiac sign: %d %d %d\n", day, month, year);
	exit(128);
}

int main() {
	int _C;
	scanf("%d\n", &_C);
	for (int the_case = 1; the_case <= _C; ++the_case) {
		int mmddyyyy;
		scanf("%d", &mmddyyyy);
		int day = mmddyyyy / 10000 % 100;
		int month = mmddyyyy / 1000000 % 100;
		int year = mmddyyyy % 10000;
		day += pog;
		while (day > days_in_month(month, year)) {
			day -= days_in_month(month, year);
			month += 1;
			if (month > 12) {
				month = 1;
				++year;
			}
		}
		printf("%d %02d/%02d/%02d %s\n",
		       the_case,
		       month,
		       day,
		       year,
		       get_zodiac_sign(day, month, year));
	}
	return 0;
}
