#if defined(ONLINE_JUDGE)
#define NDEBUG
#endif
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

static inline string &trim(string &s) {
	// ltrim
	s.erase(s.begin(),
	        find_if(s.begin(), s.end(), [](int x) { return !std::isspace(x); }));
	// ritrim
	s.erase(
	    find_if(s.rbegin(), s.rend(), [](int x) { return !std::isspace(x); }).base(),
	    s.end());
	return s;
}

static bool isempty(string str) {
	return trim(str).empty();
}

int main() {
	int _C;
	string line;
	scanf("%d\n", &_C);
	while (_C--) {
		if (cin.eof()) {
			break;
		}
		vector<string> input;
		vector<string> sorted_input;
		for (;;) {
			getline(cin, line);
			if (isempty(line)) {
				break;
			}
			input.push_back(line);
		}
		sort(input.begin(), input.end());
		for (string str : input) {
			str.erase(
			    remove_if(str.begin(), str.end(), [](char x) { return isspace(x); }),
			    str.end());
			sort(str.begin(), str.end());
			sorted_input.push_back(str);
		}

		for (size_t i = 0; i < input.size(); ++i) {
			for (size_t j = i + 1; j < input.size(); ++j) {
				if (sorted_input[i] == sorted_input[j]) {
					cout << input[i] << " = " << input[j] << endl;
				}
			}
		}
		if (_C) {
			cout << endl;
		}
	}
	return 0;
}
